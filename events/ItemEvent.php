<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\events;

/**
 * ModelEvent class.
 *
 * ModelEvent represents the parameter needed by model events.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class ItemEvent extends \yii\base\Event
{

    // модель
    public $item = false;

}
