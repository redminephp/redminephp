<?php
use yii\helpers\Html;
use app\helpers\Url;
use app\helpers\Block;
use app\helpers\Content;

use app\widgets\ActiveForm;
use app\models\Role;

/**
 * @var yii\web\View $this
 * @var yii\widgets\ActiveForm $form
 * @var app\models\LoginForm $model
 */
$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;

$checkboxListParams = [
    'itemOptions' => [
        'container'     => '',
        'labelOptions'  => ['class' => 'floating'],
    ],
    'name' => 'role[permissions][]',
    'tag' =>  'span'
];
$role_segments = $model->id ? $model->id : 'new';
?>

<?=\app\widgets\Breadcrumbs::widget([
    'items'         => [
        ['title' => 'Roles', 'url' => Url::roles()],
        ['title' => $model->name],
    ],
])?>

<?php $form = ActiveForm::begin(['action' => '', 'class' => 'edit_role']); ?>

<?=Block::flexnotifications (['error' => $model->getClearErrorList()])?>

<div class="box tabular">

    <?= $form->field($model, 'name')->textInput(['id' => 'role_name', 'name' => 'role[name]', 'size' => 30])->required(); ?>

    <?= $form->field($model, 'assignable')->checkbox(['id' => 'role_assignable', 'name' => 'role[assignable]'], false); ?>

    <?= $form->field($model, 'issues_visibility')->dropDownList($model->getListValues('issues_visibility'), ['id' => 'role_issues_visibility', 'name' => 'role[issues_visibility]']); ?>

</div>

<h3>Permissions</h3>
<div class="box tabular" id="permissions">

<?php foreach (Role::$permissionTitles as $key => $title): ?>
<?=\app\widgets\Fieldset::widget([
    'title'         => $title,
    'content'       =>
        $form->field($model, 'permissions', ['options' => ['tag' => 'span']])
            ->checkboxList($model->getPermissionsByGroup($key), $checkboxListParams)
            ->label(''),
])?>
<?php endforeach; ?>

<br /><a href="#" onclick="checkAll(&#x27;permissions&#x27;, true); return false;">Check all</a> | <a href="#" onclick="checkAll(&#x27;permissions&#x27;, false); return false;">Uncheck all</a>

</div>

<input name="commit" type="submit" value="Save" />

<?php ActiveForm::end(); ?>