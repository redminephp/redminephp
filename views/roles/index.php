<?php
    use app\helpers\Url;
    use app\helpers\Path;

    use app\Model\Role;
?>

<div class="contextual">
    <a class="icon icon-add" href="<?=Url::rolesNew()?>">New role</a>
    <a class="icon icon-summary" href="/roles/permissions" style="display: none">Permissions report</a>
</div>

<?=\app\widgets\Breadcrumbs::widget([
    'items'         => [
        ['title' => 'Roles'],
    ],
])?>

<?=\app\widgets\ListSort::widget([
    'type'              => 'role',
    'urlEdit'           => function ($item) { return Url::rolesEdit($item['id']); },
    'urlView'           => function ($item) { return Url::rolesHandler($item['id']); },
    'urlCopy'           => function ($item) { return Url::rolesCopy($item['id']); },
    'columnList'        => [
        ['title' => 'Role'],
        ['title' => 'Sort'],
        ['title' => ''],
    ],
    'items'             => $list,
    'permanentItems'    => $builtin_list,
])?>

<p class="pagination"><span class="items">(1-5/5)</span> </p>