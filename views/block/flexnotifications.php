<?php if ($list['error']): ?>

    <?php if ($type == 'single'): ?>
        <div class="flash error" id="flash_error"><?=$list['error'][0]?></div>
    <?php else: ?>
        <div id="errorExplanation">
            <ul>
                <?php foreach ($list['error'] as $item): ?>
                    <li><?=$item?></li>
                <?php endforeach; ?>
            </ul>
        </div>
    <?php endif; ?>

<?php elseif ($list['text']): ?>
    <div id="flash_notice" class="flash notice"><?=$list['text'][0]?></div>
<?php endif; ?>