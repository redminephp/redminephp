<?php
    use app\models\User;
    use app\helpers\Url;

    use yii\helpers\Html;
?>
<div id="principals">
    <?php foreach($list as $userInfo): ?>
        <label><input type="checkbox" value="<?=$userInfo[User::Id]?>" name="user_ids[]"><?=$userInfo[User::FirstName]?> <?=$userInfo[User::LastName]?></label>
    <?php endforeach; ?>
</div>

<?=\app\widgets\Pagination::widget([
    'totalCount'        => $total,
    'perPage'           => $limit,
    'currentPage'       => $page,
    'url'               => Url::groupsAutocompleteUser($model->id),
    'fnRenderItemLink'  => function ($page, $url, $title, $options) {
        $options['onclick'] = '$.get(\''.$url.'\', function( data ) {$(\'#users\').html(data)}); return false;';
        return Html::a ($title, $url, $options);
    },
])?>