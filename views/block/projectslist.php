<?php
    use app\models\Project;
    use app\helpers\Url;
    use app\helpers\Block;
?>
<ul class='projects <?=$config['class_main']?>'>

    <?php foreach ($list as $key => $item):
        $subitems = isset($item['subItems']) ? $item['subItems'] : false;
    ?>
        <li class='<?=$config['class_inner']?>'><div class='<?=$config['class_inner']?>'><a href="<?=Url::projects($item[Project::Identifier])?>" class="project <?=$config['class_inner']?> <?=($subitems)?'parent ':'leaf'?>"><?=$item[Project::Name]?></a></div>

            <?php if ($subitems): ?>
                <?=Block::projectslist($subitems, [
                    'class_main' => '', 'class_inner' => 'child',
                ])?>
            <?php endif; ?>

        </li>
    <?php endforeach; ?>

</ul>