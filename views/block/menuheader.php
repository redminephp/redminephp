<?php
use app\helpers\Url;
?>

<div id="main-menu">
    <ul>
        <?php foreach ($list as $key => $item): ?>
            <li><a class="overview <?=($key==$active_key)?'selected':''?>" <?=(isset($item['accesskey']))?'accesskey="'.$item['accesskey'].'"':''?> href="<?=$item['url']?>"><?=$item['title']?></a></li>
        <?php endforeach; ?>
    </ul>
</div>