<?php
    use app\models\Role;
    use app\models\Project;

    use app\helpers\ArrayHelper;
    use app\helpers\Url;
    use app\helpers\Block;
    use yii\helpers\Html;
?>

<div class="splitcontentleft">
    <table class="list memberships">
        <thead><tr>
            <th>Project</th>
            <th>Roles</th>
            <th style="width:15%"></th>
        </tr></thead>
        <tbody>

        <?php foreach ($model->members as $member): ?>
            <tr class="odd class" id="member-<?=$member->id?>">
                <td class="project"><a href="<?=Url::projects($member->project->identifier)?>"><?=$memeberInfo[Project::Name]?></a></td>
                <td class="roles">
                    <span id="member-<?=$member->id?>-roles"><? implode(', ', ArrayHelper::ArrayValues($memeberInfo['roles'], Role::Name)) ?></span>

                    <form style="display:none;" method="post" data-method="post" id="member-<?=$member->id?>-roles-form" data-remote="true" action="<?=Url::groupsMembershipEdit($model->id, $member->id)?>" accept-charset="UTF-8">

                        <p><?=Html::checkboxList ('membership[role_ids]', ArrayHelper::ArrayValues($memeberInfo['roles'], Role::Id), $roles)?></p>

                        <p>
                            <input type="submit" value="Change" name="commit" />
                            <a onclick="$('#member-<?=$member->id?>-roles').show(); $('#member-<?=$member->id?>-roles-form').hide(); return false;" href="#">Cancel</a>
                        </p>
                    </form>

                </td>
                <td class="buttons">
                    <a onclick="$('#member-<?=$member->id?>-roles').hide(); $('#member-<?=$member->id?>-roles-form').show(); return false;" href="#" class="icon icon-edit">Edit</a>
                    <a rel="nofollow" data-remote="true" data-method="post" data-confirm="Are you sure?" class="icon icon-del" href="<?=Url::groupsMembershipDestroy($model->id, $member->id)?>">Delete</a>
                </td>
            </tr>
        <?php endforeach; ?>

        </tbody>
    </table>
</div>

<div class="splitcontentright">
    <fieldset><legend>New project</legend>
        <form method="post" data-remote="true" data-method="post" action="<?=Url::groupsMembershipEdit($model->id)?>" accept-charset="UTF-8">

            <label for="membership_project_id" class="hidden-for-sighted">Projects</label>
            <?= \app\widgets\FieldCascadeSelect::widget([
                'cascadeItems'  => $projects,
                'items'         => ArrayHelper::ArrayValues($model->members, Project::Id),
                'id'            => 'membership_project_id',
                'name'          => 'membership[project_id]',
                'fieldId'       => Project::Id,
                'fieldParentId' => Project::ParentId,
                'fieldLabel'    => Project::Name,
            ])?>

            <p>Roles:
                <?=Html::checkboxList ('membership[role_ids]',[], $roles, [
                    'tag'           => 'span',
                    'itemOptions'   => [
                        'tag' => 'span'
                    ]
                ])?>
            </p>
            <p><input type="submit" value="Add" name="commit" /></p>
        </form></fieldset>
</div>

<script type="application/javascript">

    $('#tab-content-memberships')
        .find('[data-remote]')
        .bind('remoteSuccess', function(event, data) {
            $('#tab-content-memberships').html(data).dataRemote();
        });

</script>