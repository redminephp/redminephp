<?php
    use app\helpers\Url;
?>
<div id="admin-menu">
    <ul>
        <?php foreach ($list as $key => $value): ?>
            <li><a href="<?=$value["url"]?>" class="<?=$key?>"><?=$value["title"]?></a></li>
        <?php endforeach; ?>
    </ul>
</div>