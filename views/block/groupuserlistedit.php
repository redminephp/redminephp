<?php
    use app\helpers\Url;
    use app\helpers\Block;
?>

<div class="splitcontentleft">

    <?php if (!$model->users): ?>
        <p class="nodata">No data to display</p>
    <?php else: ?>
        <?=\app\widgets\ListSort::widget([
            'type'              => 'groupuser',
            'urlEdit'           => function ($item) { return Url::usersView($item['id']); },
            'urlView'           => function ($item) { return Url::groupsUser($item['group_id'], $item['id']); },
            'columnList'        => [
                ['title'    => 'User'],
                ['title'    => ''],
            ],
            'itemTemplate'  => '<td class="name"><span><a href="{url}">{name}</a></span></td>
            <td class="buttons">{buttons}</td>',
            'items'             => $model->users,
            'buttonsTemplate'   => '{buttonDelete}',
            'dataRemote'        => true,
        ])?>
    <?php endif; ?>

</div>

<div class="splitcontentright">
    <form method="post" id="edit_group_4" data-remote="true" data-method="post" class="edit_group" action="<?=Url::groupsUser($model->id)?>" accept-charset="UTF-8">
        <fieldset><legend>New user</legend>

            <p><label for="user_search">Search for user:</label><input type="text" name="user_search" id="user_search" class="autocomplete" data-value-was=""></p>
            <script type="text/javascript">//<![CDATA[
                observeSearchfield('user_search', 'users', '<?=Url::groupsAutocompleteUser($model->id)?>')
            //]]></script>

            <div id="users">
                <?=Block::groupsuserautocompletelist($model)?>
            </div>

            <p><input type="submit" value="Add" name="commit" /></p>
        </fieldset>
    </form>
</div>

<script type="application/javascript">

    $('#tab-content-users')
        .find('[data-remote]')
        .add('.edit_group[data-remote]')
            .bind('remoteSuccess', function(event, data) {
                $('#tab-content-users').html(data).dataRemote();
            });

</script>
