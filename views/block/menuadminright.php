<?php
use app\helpers\Url;
?>

<h3>Administration</h3>
<div id="admin-menu">
    <ul>
        <?php foreach ($list as $key => $value): ?>
            <li><a href="<?=$value["url"]?>" class="<?=$key?> <?=($key==$active_key)?'selected':''?>"><?=$value["title"]?></a></li>
        <?php endforeach; ?>
    </ul>
</div>