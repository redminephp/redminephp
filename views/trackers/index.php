<?php
    use app\helpers\Url;
?>

<div class="contextual">
    <a class="icon icon-add" href="<?=Url::trackersNew()?>">New tracker</a>
    <a class="icon icon-summary" href="/trackers/fields" style="display: none">Summary</a>
</div>

<?=\app\widgets\Breadcrumbs::widget([
    'items'         => [
        ['title' => 'Trackers'],
    ],
])?>

<?=\app\widgets\ListSort::widget([
    'type'              => 'tracker',
    'urlEdit'           => function ($item) { return Url::trackersEdit($item['id']); },
    'urlView'           => function ($item) { return Url::trackersView($item['id']); },
    'columnList'        => [
        ['title'    => 'Tracker'],
        ['title'    => 'Sort'],
        ['title'    => ''],
    ],
    'items'             => $list,
    'buttonsTemplate'   => '{buttonDelete}',
])?>

<?=\app\widgets\Pagination::widget([
    'totalCount'    => $count,
    'perPage'       => 25,
    'currentPage'   => 1,
    'url'           => Url::trackers(),
])?>