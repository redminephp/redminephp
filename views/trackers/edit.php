<?php
    use app\widgets\ActiveForm;
    use app\helpers\Url;
    use yii\helpers\Html;
    use app\helpers\Block;

    use app\models\Project;
?>

<?=\app\widgets\Breadcrumbs::widget([
    'items'         => [
        ['title' => 'Trackers', 'url' => Url::trackers()],
        ['title' => $model->name],
    ],
])?>

<?php $form = ActiveForm::begin(['action' => '', 'class' => 'edit_tracker']); ?>

    <?=Block::flexnotifications (['error' => $model->getClearErrorList()])?>

    <div class="splitcontentleft">
        <div class="box tabular">

            <?= $form->field($model, 'name')->textInput(['id' => 'tracker_name', 'name' => 'tracker[name]', 'size' => 30])->required(); ?>

            <?= $form->field($model, 'is_in_roadmap')->checkbox(['id' => 'tracker_is_in_roadmap', 'name' => 'tracker[is_in_roadmap]'], false); ?>

            <?= $form->field($model, 'fields_bits')->checkboxList($model->getListValues('fields_bits'), [
                'name' => 'tracker[fields_bits][]', // core_fields
                'tag' =>  'span',
                'itemOptions' => [
                    'container' => false,
                    'labelOptions' => ['class' => 'block'],
                ],
            ])?>

        </div>
        <input type="submit" value="Save" name="commit">
    </div>

    <div class="splitcontentright">
        <?=\app\widgets\Fieldset::widget([
            'title'         => $model->getAttributeLabel('projects'),
            'options'       => ['class' => 'box', 'id' => 'tracker_project_ids'],

            'content'       => \app\widgets\FieldCascade::widget([
                'cascadeItems'  => $model->getListValues('projectsCascade'),
                'items'         => $model->projects,

                'name'          => 'tracker[projects][]',
                'fieldId'       => Project::Id,
                'fieldParentId' => Project::ParentId,
                'fieldLabel'    => Project::Name,

                'renderItem'    => function ($object, $item, $params) {
                    $class      = $params['class'];
                    $itemId    = $item[$object->fieldId];
                    $checked    = in_array($itemId, $object->items);
                    $content    = Html::label(Html::checkbox ($object->name, $checked, ['value' => $itemId]).' '.$item[$object->fieldLabel]);
                    return Html::tag('div', $content, ['class' => $class]);
                },

                'renderItems'     => function ($object, $items, $params = ['class' => 'root']) {
                    $class = $params['class'];

                    // идем по всем элементам
                    $content = '';
                    foreach ($items as $itemId => $item) {
                        $content .= $object->fn ('renderItem', [$object, $item, $params]);
                        if (isset($item['subItems']) ) {
                            $content .= $object->fn('renderItems', [$object, $item['subItems'], ['class' => 'child']]);
                        }
                        $content = Html::tag('li', $content, ['class' => $class]);
                    }
                    return Html::tag('ul', $content, ['class' => 'projects '.$class]);
                },
            ]),
        ])?>

    </div>

<?php ActiveForm::end(); ?>
