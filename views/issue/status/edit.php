<?php
    use app\widgets\ActiveForm;
    use app\helpers\Url;
    use app\helpers\Block;
?>

<?=\app\widgets\Breadcrumbs::widget([
    'items'         => [
        ['title' => 'Issue statuses', 'url' => Url::issueStatus()],
        ['title' => $model->name],
    ],
])?>

<?php $form = ActiveForm::begin(['action' => '', 'class' => 'edit_issue_status']); ?>

    <?=Block::flexnotifications (['error' => $model->getClearErrorList()])?>

    <div class="box tabular">

        <?= $form->field($model, 'name')->textInput(['id' => 'issue_status_name', 'name' => 'issue_status[name]', 'size' => 30])->required(); ?>

        <?= $form->field($model, 'is_closed')->checkbox(['id' => 'issue_status_is_closed', 'name' => 'issue_status[is_closed]'], false); ?>

        <?= $form->field($model, 'is_default')->checkbox(['id' => 'issue_status_is_default', 'name' => 'issue_status[is_default]'], false); ?>

    </div>

    <input type="submit" value="Save" name="commit">

<?php ActiveForm::end(); ?>