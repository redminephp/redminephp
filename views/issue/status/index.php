<?php
    use app\helpers\Url;
?>

<div class="contextual">
    <a class="icon icon-add" href="<?=Url::issueStatusNew()?>">New status</a>
</div>

<?=\app\widgets\Breadcrumbs::widget([
    'items'         => [
        ['title' => 'Issue statuses'],
    ],
])?>

<?=\app\widgets\ListSort::widget([
    'type'              => 'issue_status',
    'itemTemplate'      => '<td class="name"><a href="{url}">{name}</a></td>
        <td>{defaultValue}</td>
        <td>{issueClosed}</td>
        <td class="reorder">{sortItems}</td>
        <td class="buttons">{buttons}</td>',
    'buttonCopyTemplate'=> '',
    'urlEdit'           => function ($item) { return Url::issueStatusEdit($item['id']); },
    'urlView'           => function ($item) { return Url::issueStatusHandler($item['id']); },
    'columnList'        => [
        ['title' => 'Status'],
        ['title' => 'Default value'],
        ['title' => 'Issue closed'],
        ['title' => 'Sort'],
        ['title' => ''],
    ],
    'items'             => $list,
    'fnRenderItem'      => function ($widget, $item, $template) {
        return strtr($template, [
            '{name}'            => $item['name'],
            '{url}'             => Url::issueStatusEdit($item['id']),
            '{defaultValue}'    => $widget->renderToggleCheck ($item['is_default']),
            '{issueClosed}'     => $widget->renderToggleCheck ($item['is_closed']),
        ]);
    },
])?>


<?=\app\widgets\Pagination::widget([
    'totalCount'    => $count,
    'perPage'       => 25,
    'currentPage'   => 1,
    'url'           => Url::issueStatus(),
])?>
