<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var yii\widgets\ActiveForm $form
 * @var app\models\LoginForm $model
 */
$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>

<div id="login-form">

        <?php $form = ActiveForm::begin([
            'fieldConfig' => [
                'template' =>  '<td align="right">{label}</td>
                                <td align="left">{input}</td>',
                'options' => [
                    'tag' => 'tr'
                ],
            ],
        ]); ?>

        <table>

            <?= $form->field($model, 'username')->textInput(['id' => 'username', 'name' => 'username']); ?>

            <?= $form->field($model, 'password')->passwordInput(['id' => 'password', 'name' => 'password']) ?>

            <tr>
                <td></td>
                <td align="left"></td>
            </tr>
            <tr>
                <td align="left"></td>
                <td align="right">
                    <?= Html::submitButton('Login &#187;', ['class' => 'btn btn-primary', 'name' => 'login']) ?>
                </td>
            </tr>

        </table>

    <?php ActiveForm::end(); ?>

</div>

<script type="text/javascript">
    //<![CDATA[
    $('#username').focus();
    //]]>
</script>


