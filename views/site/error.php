<?php

use yii\helpers\Html;
use app\helpers\Block;

/**
 * @var yii\web\View $this
 * @var string $name
 * @var string $message
 * @var Exception $exception
 */

$this->title = $code;

$messages = [
    '403'   => 'You are not authorized to access this page.',
    '404'   => 'The page you were trying to access doesn&#x27;t exist or has been removed.',
];
if (isset($messages[$code]) ) {
    $message = $messages[$code];
}
?>

<h2><?=$code?></h2>

<?=Block::flexnotifications (['error' => [$message]], 'single')?>

<p><a href="javascript:history.back()">Back</a></p>