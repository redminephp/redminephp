<?php
    use app\helpers\Url;
?>

<div class="contextual">
    <a class="icon icon-add" href="/projects/new?parent_id=yertyert">New subproject</a>
    <a rel="nofollow" data-method="post" data-confirm="Are you sure?" class="icon icon-lock" href="/projects/yertyert/close">Close</a>
</div>


<?=\app\widgets\Breadcrumbs::widget([
    'items'         => [
        ['title' => 'Overview'],
    ],
])?>


<div class="splitcontentleft">
    <ul>
    </ul>

    <div class="issues box">
        <h3>Issue tracking</h3>
        <ul>
            <li><a href="/projects/yertyert/issues?set_filter=1&amp;tracker_id=1">Bug</a>:
                0 open / 0
            </li>
            <li><a href="/projects/yertyert/issues?set_filter=1&amp;tracker_id=2">Feature</a>:
                0 open / 0
            </li>
            <li><a href="/projects/yertyert/issues?set_filter=1&amp;tracker_id=3">Support</a>:
                0 open / 0
            </li>
        </ul>
        <p>
            <a href="/projects/yertyert/issues?set_filter=1">View all issues</a>
            | <a href="/projects/yertyert/issues/calendar">Calendar</a>
            | <a href="/projects/yertyert/issues/gantt">Gantt</a>
        </p>
    </div>

</div>

<div class="splitcontentright">



</div>