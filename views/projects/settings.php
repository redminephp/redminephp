<?php
use yii\helpers\Html;
use app\helpers\Url;
use app\helpers\Content;

use app\widgets\ActiveForm;
use app\models\Project;

/**
 * @var yii\web\View $this
 * @var yii\widgets\ActiveForm $form
 * @var app\models\LoginForm $model
 */
$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;

$tabsContent = Content::create();

$checkboxListParams = [
    'itemOptions' => [
        'container'     => '',
        'labelOptions'  => ['class' => 'floating'],
    ],
    'tag' =>  'span'
];
?>


<script src="/web/js/jstoolbar/jstoolbar-textile.min.js?1370076409" type="text/javascript"></script>
<script src="/web/js/jstoolbar/jstoolbar-en.js?1321098518" type="text/javascript"></script>
<link href="/web/css/jstoolbar.css?1370076409" media="screen" rel="stylesheet" type="text/css" />
<script src="/web/js/select_list_move.js?1393764265" type="text/javascript"></script>

<h2>Settings</h2>

    <?php
        $tabsContent->begin();
        $form = ActiveForm::begin(['action' => Url::projectsSettings($project->identifier, 'info')]);
    ?>

    <div class="box tabular">
        <?= $form->field($project, 'name')->textInput(['id' => 'project_name', 'name' => 'project[name]', 'size' => 60])->required(); ?>

        <?= $form->field($project, 'description')->htmlEditor(['id' => 'project_description', 'name' => 'project[description]', 'cols' => 40, 'rows' => 8]); ?>

        <?= $form->field($project, 'identifier')->textInput(['id' => 'project_identifier', 'name' => 'project[identifier]', 'size' => 60, 'maxlength' => 100, 'disabled' => 'disabled'])->required(); ?>

        <?= $form->field($project, 'homepage')->textInput(['id' => 'project_homepage', 'name' => 'project[homepage]', 'size' => 60]); ?>

        <?= $form->field($project, 'is_public')->checkbox(['id' => 'project_is_public', 'name' => 'project[is_public]'], false); ?>

        <?= $form->field($project, 'parent_id')->dropDownList($project->getListValues('parent_id'), ['id' => 'project_parent_id', 'name' => 'project[parent_id]']); ?>

        <?= $form->field($project, 'inherit_members')->checkbox(['id' => 'project_inherit_members', 'name' => 'project[inherit_members]'], false); ?>
    </div>

    <?=\app\widgets\Fieldset::widget([
        'title'         => 'Trackers',
        'content'       =>
            $form->field($project, 'tracker_ids', ['options' => ['tag' => 'span',]])
                ->checkboxList($project->getListValues('tracker_ids'), array_merge($checkboxListParams, ['name' => 'project[tracker_ids]']))
                ->label(''),
    ])?>

    <!--[eoform:project]-->

    <input name="commit" type="submit" value="Save" />

<?php ActiveForm::end(); $tabsContent->end('info'); ?>



<?php
    $tabsContent->begin();
    $form = ActiveForm::begin(['action' => Url::projectsSettings($project->identifier, 'modules')]);
?>

    <?=\app\widgets\Fieldset::widget([
        'title'         => 'Select modules to enable for this project:',
        'options'       => ['class' => 'box'],
        'content'       =>
            $form->field($project, 'enabled_module_names')
                ->checkboxList($project->getListValues('enabled_module_names'), [
                    'name' => 'enabled_module_names[]',
                    'itemOptions' => [
                        'tag' => 'p',
                        'container' => [
                            'class' => 'test',
                        ]
                    ],
                ])
                ->label(''),
    ])?>

   
    <p><a href="#" onclick="checkAll(&#x27;modules-form&#x27;, true); return false;">Check all</a> | <a href="#" onclick="checkAll(&#x27;modules-form&#x27;, false); return false;">Uncheck all</a></p>

    <p><input name="commit" type="submit" value="Save" /></p>

<?php ActiveForm::end(); $tabsContent->end('modules'); ?>


<?php
    $tabsContent->begin();
    $form = ActiveForm::begin(['action' => Url::projectsSettings($project->identifier, 'members')]);
?>

    <div class="splitcontentleft">
        <p class="nodata">No data to display</p>
    </div>

    <div class="splitcontentright">
        <form accept-charset="UTF-8" action="/projects/sfgsfghgh/memberships" class="new_membership" data-remote="true" id="new_membership" method="post"><div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="&#x2713;" /><input name="authenticity_token" type="hidden" value="/Y80DTd7uZdee9fm1EoV57uRjJLYPLwjyES6btIiLaM=" /></div>
            <fieldset>
                <legend>New member</legend>
                <p>
                    <label for="principal_search">Search for user or group:</label>
                    <input id="principal_search" name="principal_search" type="text" />
                </p>
                <script type="text/javascript">
                    //<![CDATA[
                    observeSearchfield('principal_search', null, '/projects/sfgsfghgh/memberships/autocomplete.js')
                    //]]>
                </script>
                <div id="principals_for_new_member">
                    <div id="principals"><label><input name="membership[user_ids][]" type="checkbox" value="2" /> Redmine Admin</label>
                        <label><input name="membership[user_ids][]" type="checkbox" value="4" /> group 1</label>
                        <label><input name="membership[user_ids][]" type="checkbox" value="5" /> group 2</label>
                        <label><input name="membership[user_ids][]" type="checkbox" value="6" /> group 3</label>
                    </div><p class="pagination"><span class="items">(1-4/4)</span> </p>
                </div>
                <p>
                    Roles:
                    <label><input name="membership[role_ids][]" type="checkbox" value="5" /> Reporter</label>
                    <label><input name="membership[role_ids][]" type="checkbox" value="3" /> Manager</label>
                    <label><input name="membership[role_ids][]" type="checkbox" value="4" /> Developer</label>
                </p>
                <p><input id="member-add-submit" name="commit" type="submit" value="Add" /></p>
            </fieldset>
        </form>
    </div>

<?php ActiveForm::end(); $tabsContent->end('members'); ?>


<?php
    $tabsContent->begin();
    $form = ActiveForm::begin(['action' => Url::projectsSettings($project->identifier, 'versions')]);
?>

    <p class="nodata">No data to display</p>

    <div class="contextual">
    </div>

    <p><a href="/projects/sfgsfghgh/versions/new?back_url=" class="icon icon-add">New version</a></p>

<?php ActiveForm::end(); $tabsContent->end('versions'); ?>


<?php
    $tabsContent->begin();
    $form = ActiveForm::begin(['action' => Url::projectsSettings($project->identifier, 'categories')]);
?>
    <p class="nodata">No data to display</p>

    <p><a href="/projects/sfgsfghgh/issue_categories/new" class="icon icon-add">New category</a></p>

<?php ActiveForm::end(); $tabsContent->end('categories'); ?>


<?php
    $tabsContent->begin();
    $form = ActiveForm::begin(['action' => Url::projectsSettings($project->identifier, 'wiki')]);
?>

    <div class="box tabular">
        <p><label for="wiki_start_page">Start page<span class="required"> *</span></label><input id="wiki_start_page" name="wiki[start_page]" size="60" type="text" value="Wiki" />
            <em class="info">Unallowed characters: , . / ? ; : |</em></p>
    </div>

    <div class="contextual">
        <a href="/projects/sfgsfghgh/wiki/destroy" class="icon icon-del">Delete</a>
    </div>

    <input name="commit" type="submit" value="Save" />

<?php ActiveForm::end(); $tabsContent->end('wiki'); ?>



<?php
    $tabsContent->begin();
    $form = ActiveForm::begin(['action' => Url::projectsSettings($project->identifier, 'repositories')]);
?>

    <p class="nodata">No data to display</p>

    <p><a href="/projects/sfgsfghgh/repositories/new" class="icon icon-add">New repository</a></p>

<?php ActiveForm::end(); $tabsContent->end('repositories'); ?>



<?php
    $tabsContent->begin();
    $form = ActiveForm::begin(['action' => Url::projectsSettings($project->identifier, 'boards')]);
?>

    <p class="nodata">No data to display</p>

    <p><a href="/projects/sfgsfghgh/boards/new" class="icon icon-add">New forum</a></p>

<?php ActiveForm::end(); $tabsContent->end('boards'); ?>



<?php
    $tabsContent->begin();
    $form = ActiveForm::begin(['action' => Url::projectsSettings($project->identifier, 'activities')]);
?>

    <table class="list">
        <thead><tr>
            <th>Name</th>
            <th>System Activity</th>
            <th>Active</th>
        </tr></thead>


        <tr class="odd">
            <td class="name">
                <input id="enumerations_8_parent_id" name="enumerations[8][parent_id]" type="hidden" value="8" />
                Design
            </td>
            <td class="tick"><img alt="Toggle_check" src="/images/toggle_check.png?1321098518" /></td>
            <td>
                <input name="enumerations[8][active]" type="hidden" value="0" /><input checked="checked" id="enumerations_8_active" name="enumerations[8][active]" type="checkbox" value="1" />
            </td>
        </tr>

        <tr class="even">
            <td class="name">
                <input id="enumerations_9_parent_id" name="enumerations[9][parent_id]" type="hidden" value="9" />
                Development
            </td>
            <td class="tick"><img alt="Toggle_check" src="/images/toggle_check.png?1321098518" /></td>
            <td>
                <input name="enumerations[9][active]" type="hidden" value="0" /><input checked="checked" id="enumerations_9_active" name="enumerations[9][active]" type="checkbox" value="1" />
            </td>
        </tr>
    </table>

    <div class="contextual">
        <a href="/projects/sfgsfghgh/enumerations" class="icon icon-del" data-confirm="Are you sure?" data-method="delete" rel="nofollow">Reset</a>
    </div>

    <input name="commit" type="submit" value="Save" />

<?php ActiveForm::end(); $tabsContent->end('activities'); ?>


<?php
$items = [];
foreach (Project::$tabs as $key => $info) {
    $items[] = ['key' => $key, 'label' => $info['title']];
}
?>
<?=\app\widgets\Tab::widget([
    'items'         => $items,
    'activeKey'     => $project->tab,
    'contentTabs'   => $tabsContent,
    'urlParams'     => ['projectsSettings', [$project->identifier, '{key}']],
])?>