<?php

use app\helpers\Url;
use app\widgets\ActiveForm;

use app\helpers\Block;

$checkboxListParams = [
    'itemOptions' => [
        'container'     => '',
        'labelOptions'  => ['class' => 'floating'],
    ],
    'tag' =>  'span'
];

?>


<script src="/web/js/jstoolbar/jstoolbar-textile.min.js?1370076409" type="text/javascript"></script>
<script src="/web/js/jstoolbar/jstoolbar-en.js?1321098518" type="text/javascript"></script>
<link href="/web/css/jstoolbar.css?1370076409" media="screen" rel="stylesheet" type="text/css" />
<script src="/web/js/select_list_move.js?1393764265" type="text/javascript"></script>


<h2>New project</h2>

<?php $form = ActiveForm::begin([
    'action'    => Url::projectsNew(),
    'options'   => [
        'class'             => 'new_project',
        'accept-charset'    => 'UTF-8',
    ],
    'id'        => 'new_project',
]); ?>

    <?=Block::flexnotifications (['error' => $model->getClearErrorList()])?>

    <div class="box tabular">
        <!--[form:project]-->

        <?= $form->field($model, 'name')->textInput(['id' => 'project_name', 'name' => 'project[name]', 'size' => 60])->required(); ?>

        <?= $form->field($model, 'description')->htmlEditor(['id' => 'project_description', 'name' => 'project[description]', 'cols' => 40, 'rows' => 8]); ?>

        <?= $form->field($model, 'identifier')->textInput(['id' => 'project_identifier', 'name' => 'project[identifier]', 'size' => 60, 'maxlength' => 100])->required()->hintDown('Length between 1 and 100 characters. Only lower case letters (a-z), numbers, dashes and underscores are allowed, must start with a lower case letter.<br />Once saved, the identifier cannot be changed'); ?>

        <?= $form->field($model, 'homepage')->textInput(['id' => 'project_homepage', 'name' => 'project[homepage]', 'size' => 60]); ?>

        <?= $form->field($model, 'is_public')->checkbox(['id' => 'project_is_public', 'name' => 'project[is_public]'], false); ?>

        <?= $form->field($model, 'parent_id')->dropDownList($model->getListValues('parent_id'), ['id' => 'project_parent_id', 'name' => 'project[parent_id]']); ?>

        <?= $form->field($model, 'inherit_members')->checkbox(['id' => 'project_inherit_members', 'name' => 'project[inherit_members]'], false); ?>

    </div>

    <fieldset id="project_modules" class="box tabular"><legend><?=$model->getAttributeLabel('enabled_module_names')?></legend>

        <?= $form->field($model, 'enabled_module_names', ['options' => [
            'tag' => 'span',
        ]])->checkboxList($model->getListValues('enabled_module_names'), array_merge($checkboxListParams, ['name' => 'project[enabled_module_names]']))->label(''); ?>

    </fieldset>

    <fieldset class="box tabular" id="project_trackers"><legend><?=$model->getAttributeLabel('tracker_ids')?></legend>

        <?= $form->field($model, 'tracker_ids', ['options' => [
            'tag' => 'span',
        ]])->checkboxList($model->getListValues('tracker_ids'), array_merge($checkboxListParams, ['name' => 'project[tracker_ids]']))->label(''); ?>

    </fieldset>

    <!--[eoform:project]-->


    <script type="text/javascript">
        //<![CDATA[

        $(document).ready(function() {
            $('#project_modules').find('input[value=issue_tracking]').on('change', function(){
                if ($(this).prop("checked") ){
                    $('#project_trackers, #project_issue_custom_fields').show();
                } else {
                    $('#project_trackers, #project_issue_custom_fields').hide();
                }
            }).trigger('change');
        });

        //]]>
    </script>
    <input name="commit" type="submit" value="Create" />
    <input name="continue" type="submit" value="Create and continue" />

<?php ActiveForm::end(); ?>