<?php
use yii\helpers\Html;
use app\helpers\Url;
use app\helpers\Block;
use app\helpers\Content;
?>

<div class="contextual">
    <a href="<?=Url::projectsNew()?>" class="icon icon-add">New project</a> |
    <a href="/issues">View all issues</a> |
    <a href="/time_entries">Overall spent time</a> |
    <a href="/activity">Overall activity</a>
</div>

<h2>Projects</h2>

<div id="projects-index">
    <?=Block::projectslist($list)?>
</div>

<p style="text-align:right;">
    <span class="my-project">My projects</span>
</p>

<p class="other-formats">Also available in:  <span><a href="/projects.atom?key=dc061625c73e7c8719c1cb8b8b0fdf1f721e817c" class="atom" rel="nofollow">Atom</a></span>
</p>