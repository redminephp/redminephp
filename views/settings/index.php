<?php
use yii\helpers\Html;
use app\helpers\Url;
use app\helpers\Content;

use app\widgets\ActiveForm;
use app\models\Settings;

/**
 * @var yii\web\View $this
 * @var yii\widgets\ActiveForm $form
 * @var app\models\LoginForm $model
 */
$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
$checkboxListParams = [
    'itemOptions' => [
        'container'     => '',
        'labelOptions'  => ['class' => 'block'],
    ],
    'tag' =>  'span'
];

$tabsContent = Content::create();
?>


<script src="/web/js/jstoolbar/jstoolbar-textile.min.js?1370076409" type="text/javascript"></script>
<script src="/web/js/jstoolbar/jstoolbar-en.js?1321098518" type="text/javascript"></script>
<link href="/web/css/jstoolbar.css?1370076409" media="screen" rel="stylesheet" type="text/css" />
<script src="/web/js/select_list_move.js?1393764265" type="text/javascript"></script>


<h2>Settings</h2>


<?php
    $tabsContent->begin();
    $form = ActiveForm::begin(['action' => Url::settings('edit', 'general')]);
?>

    <div class="box tabular settings">

        <?= $form->field($model, 'app_title')->textInput(['id' => 'settings_app_title', 'name' => 'settings[app_title]', 'size' => 30]); ?>

        <?= $form->field($model, 'welcome_text')->htmlEditor(['id' => 'settings_welcome_text', 'name' => 'settings[welcome_text]', 'cols' => 60, 'rows' => 5]); ?>

        <?= $form->field($model, 'attachment_max_size')->textInput(['id' => 'settings_attachment_max_size', 'name' => 'settings[attachment_max_size]', 'size' => 6])->hintRight("KB"); ?>

        <?= $form->field($model, 'per_page_options')->textInput(['id' => 'settings_per_page_options', 'name' => 'settings[per_page_options]', 'size' => 20])->hintDown("Multiple values allowed (comma separated)."); ?>

        <?= $form->field($model, 'activity_days_default')->textInput(['id' => 'settings_activity_days_default', 'name' => 'settings[activity_days_default]', 'size' => 6])->hintRight("days"); ?>

        <?= $form->field($model, 'host_name')->textInput(['id' => 'settings_host_name', 'name' => 'settings[host_name]', 'size' => 60])->hintDown("Example: trickylabs.m.redmine.org"); ?>

        <?= $form->field($model, 'protocol')->dropDownList($model->getListValues('protocol'), ['id' => 'settings_protocol', 'name' => 'settings[protocol]']); ?>

        <?= $form->field($model, 'text_formatting')->dropDownList($model->getListValues('text_formatting'), ['id' => 'settings_text_formatting', 'name' => 'settings[text_formatting]']); ?>

        <?= $form->field($model, 'cache_formatted_text')->checkbox(['id' => 'settings_cache_formatted_text', 'name' => 'settings[cache_formatted_text]'], false); ?>

        <?= $form->field($model, 'wiki_compression')->dropDownList($model->getListValues('wiki_compression'), ['id' => 'settings_wiki_compression', 'name' => 'settings[wiki_compression]']); ?>

        <?= $form->field($model, 'feeds_limit')->textInput(['id' => 'settings_feeds_limit', 'name' => 'settings[feeds_limit]', 'size' => 6]); ?>

        <?= $form->field($model, 'file_max_size_displayed')->textInput(['id' => 'settings_file_max_size_displayed', 'name' => 'settings[file_max_size_displayed]', 'size' => 6])->hintRight("KB"); ?>

        <?= $form->field($model, 'diff_max_lines_displayed')->textInput(['id' => 'settings_diff_max_lines_displayed', 'name' => 'settings[diff_max_lines_displayed]', 'size' => 6]); ?>

        <?= $form->field($model, 'repositories_encodings')->textInput(['id' => 'settings_repositories_encodings', 'name' => 'settings[repositories_encodings]', 'size' => 60])->hintDown("Multiple values allowed (comma separated)."); ?>

    </div>

    <input name="commit" type="submit" value="Save" />

<?php ActiveForm::end(); $tabsContent->end('general'); ?>



<?php
$tabsContent->begin();
$form = ActiveForm::begin(['action' => Url::settings('edit', 'display')]);
?>

    <div class="box tabular settings">

        <?= $form->field($model, 'ui_theme')->dropDownList($model->getListValues('ui_theme'), ['id' => 'settings_ui_theme', 'name' => 'settings[ui_theme]']); ?>

        <?= $form->field($model, 'default_language')->dropDownList($model->getListValues('default_language'), ['id' => 'settings_default_language', 'name' => 'settings[default_language]']); ?>

        <?= $form->field($model, 'force_default_language_for_anonymous')->checkbox(['id' => 'settings_force_default_language_for_anonymous', 'name' => 'settings[force_default_language_for_anonymous]'], false); ?>

        <?= $form->field($model, 'force_default_language_for_loggedin')->checkbox(['id' => 'settings_force_default_language_for_loggedin', 'name' => 'settings[force_default_language_for_loggedin]'], false); ?>

        <?= $form->field($model, 'start_of_week')->dropDownList($model->getListValues('start_of_week'), ['id' => 'settings_start_of_week', 'name' => 'settings[start_of_week]']); ?>

        <?= $form->field($model, 'date_format')->dropDownList($model->getListValues('date_format'), ['id' => 'settings_date_format', 'name' => 'settings[date_format]']); ?>

        <?= $form->field($model, 'time_format')->dropDownList($model->getListValues('time_format'), ['id' => 'settings_time_format', 'name' => 'settings[time_format]']); ?>

        <?= $form->field($model, 'user_format')->dropDownList($model->getListValues('user_format'), ['id' => 'settings_user_format', 'name' => 'settings[user_format]']); ?>

        <?= $form->field($model, 'gravatar_enabled')->checkbox(['id' => 'settings_gravatar_enabled', 'name' => 'settings[gravatar_enabled]'], false); ?>

        <?= $form->field($model, 'gravatar_default')->dropDownList($model->getListValues('gravatar_default'), ['id' => 'settings_gravatar_default', 'name' => 'settings[gravatar_default]']); ?>

        <?= $form->field($model, 'thumbnails_enabled')->checkbox(['id' => 'settings_thumbnails_enabled', 'name' => 'settings[thumbnails_enabled]'], false); ?>

        <?= $form->field($model, 'thumbnails_size')->textInput(['id' => 'settings_thumbnails_size', 'name' => 'settings[thumbnails_size]', 'size' => 6]); ?>

    </div>

    <input name="commit" type="submit" value="Save" />

<?php ActiveForm::end(); $tabsContent->end('display'); ?>



<?php
    $tabsContent->begin();
    $form = ActiveForm::begin(['action' => Url::settings('edit', 'authentication')]);
?>

    <div class="box tabular settings">

        <?= $form->field($model, 'login_required')->checkbox(['id' => 'settings_login_required', 'name' => 'settings[login_required]'], false); ?>

        <?= $form->field($model, 'autologin')->dropDownList($model->getListValues('autologin'), ['id' => 'settings_autologin', 'name' => 'settings[autologin]']); ?>

        <?= $form->field($model, 'self_registration')->dropDownList($model->getListValues('autologin'), ['id' => 'settings_self_registration', 'name' => 'settings[self_registration]']); ?>

        <?= $form->field($model, 'unsubscribe')->checkbox(['id' => 'settings_unsubscribe', 'name' => 'settings[unsubscribe]'], false); ?>

        <?= $form->field($model, 'password_min_length')->textInput(['id' => 'settings_password_min_length', 'name' => 'settings[password_min_length]', 'size' => 6]); ?>

        <?= $form->field($model, 'lost_password')->checkbox(['id' => 'settings_lost_password', 'name' => 'settings[lost_password]'], false); ?>

        <?= $form->field($model, 'openid')->checkbox(['id' => 'settings_openid', 'name' => 'settings[openid]'], false); ?>

        <?= $form->field($model, 'rest_api_enabled')->checkbox(['id' => 'settings_rest_api_enabled', 'name' => 'settings[rest_api_enabled]'], false); ?>

        <?= $form->field($model, 'jsonp_enabled')->checkbox(['id' => 'settings_jsonp_enabled', 'name' => 'settings[jsonp_enabled]'], false); ?>

    </div>

    <fieldset class="box">
        <legend>Session expiration</legend>

        <div class="tabular settings">

            <?= $form->field($model, 'session_lifetime')->dropDownList($model->getListValues('session_lifetime'), ['id' => 'settings_session_lifetime', 'name' => 'settings[session_lifetime]']); ?>

            <?= $form->field($model, 'session_timeout')->dropDownList($model->getListValues('session_timeout'), ['id' => 'settings_session_timeout', 'name' => 'settings[session_timeout]']); ?>

        </div>

        <p><em class="info">Warning: changing these settings may expire the current sessions including yours.</em></p>
    </fieldset>

    <input name="commit" type="submit" value="Save" />

<?php ActiveForm::end(); $tabsContent->end('authentication'); ?>



<?php
    $tabsContent->begin();
    $form = ActiveForm::begin(['action' => Url::settings('edit', 'projects')]);
?>
    <div class="box tabular settings">

        <?= $form->field($model, 'default_projects_public')->checkbox(['id' => 'settings_default_projects_public', 'name' => 'settings[default_projects_public]'], false); ?>

        <?= $form->field($model, 'default_projects_modules')->checkboxList($model->getListValues('default_projects_modules'), array_merge($checkboxListParams, ['name' => 'settings[default_projects_modules]'])); ?>

        <?= $form->field($model, 'default_projects_tracker_ids')->checkboxList($model->getListValues('default_projects_tracker_ids'), array_merge($checkboxListParams, ['name' => 'settings[default_projects_tracker_ids]'])); ?>

        <?= $form->field($model, 'sequential_project_identifiers')->checkbox(['id' => 'settings_sequential_project_identifiers', 'name' => 'settings[sequential_project_identifiers]'], false); ?>

        <?= $form->field($model, 'new_project_user_role_id')->dropDownList($model->getListValues('new_project_user_role_id'), ['id' => 'settings_new_project_user_role_id', 'name' => 'settings[new_project_user_role_id]']); ?>

    </div>

    <input name="commit" type="submit" value="Save" />

<?php ActiveForm::end(); $tabsContent->end('projects'); ?>



<?php
$tabsContent->begin();

$form = ActiveForm::begin([
    'options'   => ['onsubmit' => 'return selectAllOptions("selected_columns");'],
    'action'    => Url::settings('edit', 'issues'),
]);
?>
    <div class="box tabular settings">

        <?= $form->field($model, 'cross_project_issue_relations')->checkbox(['id' => 'settings_cross_project_issue_relations', 'name' => 'settings[cross_project_issue_relations]'], false); ?>

        <?= $form->field($model, 'cross_project_subtasks')->dropDownList($model->getListValues('cross_project_subtasks'), ['id' => 'settings_cross_project_subtasks', 'name' => 'settings[cross_project_subtasks]']); ?>

        <?= $form->field($model, 'issue_group_assignment')->checkbox(['id' => 'settings_issue_group_assignment', 'name' => 'settings[issue_group_assignment]'], false); ?>

        <?= $form->field($model, 'default_issue_start_date_to_creation_date')->checkbox(['id' => 'settings_default_issue_start_date_to_creation_date', 'name' => 'settings[default_issue_start_date_to_creation_date]'], false); ?>

        <?= $form->field($model, 'display_subprojects_issues')->checkbox(['id' => 'settings_display_subprojects_issues', 'name' => 'settings[display_subprojects_issues]'], false); ?>

        <?= $form->field($model, 'issue_done_ratio')->dropDownList($model->getListValues('issue_done_ratio'), ['id' => 'settings_issue_done_ratio', 'name' => 'settings[issue_done_ratio]']); ?>

        <?= $form->field($model, 'non_working_week_days')->checkboxList($model->getListValues('non_working_week_days'), [
            'name' => 'settings[non_working_week_days]',
            'itemOptions' => [
                'container'     => '',
                'labelOptions'  => ['class' => 'inline', 'separator' => ''],
            ],
            'separator'     => '',
            'tag' =>  'span'
        ]); ?>

        <?= $form->field($model, 'issues_export_limit')->textInput(['id' => 'settings_issues_export_limit', 'name' => 'settings[issues_export_limit]', 'size' => 6]); ?>

        <?= $form->field($model, 'gantt_items_limit')->textInput(['id' => 'settings_gantt_items_limit', 'name' => 'settings[gantt_items_limit]', 'size' => 6]); ?>

    </div>

    <?php // сделать в activeField соответствующую ф-цию на основе виджета! а нужно ли?.. ?>
    <?= \app\widgets\ListBoxSwitch::widget([
        'model'         => $model,
        'form'          => $form,
        'attribute'     => 'issue_list_default_columns',
        'allItems'      => $model->getListValues('issue_list_default_columns'),
        'activeKeys'    => $model->issue_list_default_columns,
        'fieldName'     => 'settings[issue_list_default_columns][]',
    ])?>

    <input name="commit" type="submit" value="Save" />

<?php ActiveForm::end(); $tabsContent->end('issues'); ?>


<?php
    $tabsContent->begin();
    $form = ActiveForm::begin(['action' => Url::settings('edit', 'notifications')]);
?>

    <div class="box tabular settings">

        <?= $form->field($model, 'mail_from')->textInput(['id' => 'settings_mail_from', 'name' => 'settings[mail_from]', 'size' => 60]); ?>

        <?= $form->field($model, 'bcc_recipients')->checkbox(['id' => 'settings_bcc_recipients', 'name' => 'settings[bcc_recipients]'], false); ?>

        <?= $form->field($model, 'plain_text_mail')->checkbox(['id' => 'settings_plain_text_mail', 'name' => 'settings[plain_text_mail]'], false); ?>

        <?= $form->field($model, 'default_notification_option')->dropDownList($model->getListValues('default_notification_option'), ['id' => 'settings_default_notification_option', 'name' => 'settings[default_notification_option]']); ?>

    </div>


    <fieldset class="box" id="notified_events"><legend><?=$model->getAttributeLabel('notified_events')?></legend>
        <input id="settings_notified_events_" name="settings[notified_events][]" type="hidden" value="" />

        <?php // @todo преобразовать нормально чтобы гибко было(+ может вынести отдельно в виджет или ф-цию)! ?>
        <?php $childList = $model->getListValues('notified_events_parent'); ?>
        <?=
            $form->field($model, 'notified_events', ['options' => [
                'tag' => 'span',
            ]])->checkboxList($model->getListValues('notified_events'), array_merge($checkboxListParams, [
                'name'  => 'settings[notified_events]',
                'item'  => function ($index, $label, $name, $checked, $value) use ($childList) {
                    $checkboxOptions = [
                        'value'                     => $value,
                        'label'                     => $label,
                    ];
                    if ($value == 'issue_updated') {
                        $checkboxOptions['data-disables'] = 'input[data-parent-notifiable=issue_updated]';
                    }
                    if (in_array($value, $childList) ) {
                        $checkboxOptions = array_merge ($checkboxOptions, [
                            'labelOptions'              => ['class' => 'parent'],
                            'data-parent-notifiable'    => 'issue_updated',
                        ]);
                    }
                    return Html::checkbox($name, $checked, $checkboxOptions);
                }
            ]))->label('');?>
    </fieldset>


    <?php // @todo вынести в отдельную ф-цию! ?>
    <fieldset class="box"><legend><?=$model->getAttributeLabel('emails_header')?></legend>

        <?= $form->field($model, 'emails_header', ['options' => [
            'tag' => 'span',
        ]])->textarea(['id' => 'settings_emails_header', 'name' => 'settings[emails_header]', 'rows' => 5, 'class' => 'wiki-edit'])->label(''); ?>

    </fieldset>

    <fieldset class="box"><legend><?=$model->getAttributeLabel('emails_footer')?></legend>

        <?= $form->field($model, 'emails_footer', ['options' => [
            'tag' => 'span',
        ]])->textarea(['id' => 'settings_emails_footer', 'name' => 'settings[emails_footer]', 'rows' => 5, 'class' => 'wiki-edit'])->label(''); ?>

    </fieldset>

    <div style="float:right;">
        <a href="/redmine/admin/test_email">Send a test email</a>
    </div>

    <input name="commit" type="submit" value="Save" />

<?php ActiveForm::end(); $tabsContent->end('notifications'); ?>


<?php $tabsContent->begin(); ?>

    <div class="flash error">
        This feature is disabled in this demo environment.
    </div>

<?php $tabsContent->end('mail_handler'); ?>


<?php $tabsContent->begin(); ?>

    <div class="flash error">
        This feature is disabled in this demo environment.
    </div>

<?php $tabsContent->end('repositories'); ?>


<?php
    $items = [];
    foreach (Settings::$tabs as $key => $info) {
        $items[] = ['key' => $key, 'label' => $info['title']];
    }
?>
<?=\app\widgets\Tab::widget([
    'items'         => $items,
    'activeKey'     => $model->tab,
    'contentTabs'   => $tabsContent,
    'urlParams'     => ['settings', ['', '{key}']],
])?>