<?php
use app\helpers\Url;
use app\helpers\Block;
use app\models\Project;
?>

<div class="contextual">
    <a href="<?=Url::projectsNew()?>" class="icon icon-add">New project</a>
</div>

<h2>Projects</h2>

<?=\app\widgets\FilterStatus::widget([
    'title'         => 'Project',
    'statusItems'   => Project::$statuses,
    'activeItem'    => $activeStatus,
    'keyword'       => $keyword,
    'baseUrl'       => Url::admin('projects'),
]);?>&nbsp;

<div class="autoscroll">
    <?=\app\widgets\ListCascade::widget([
        'rowClass'          => 'project',
        'itemTemplate'      => '<td class="name"><span><a href="{url}">{name}</a></span></td>
                                <td>{isPublic}</td>
                                <td>{createdOn}</td>
                                <td class="buttons">{buttons}</td>',
        'buttonsTemplate'   => /*'{buttonSwitchArchive}*/
                                '{buttonDelete}',
        'urlEdit'           => function ($item) { return Url::projectsSettings($item['huu']); },
        'urlView'           => function ($item) { return Url::projectsHandler($item['huu']); },
        'urlCopy'           => function ($item) { return Url::projectsCopy($item['huu']); },
        'columnList'        => [
            ['title' => 'Project'],
            ['title' => 'Public'],
            ['title' => 'Created'],
            ['title' => ''],
        ],
        'items'             => $list,
        'fnIsArchive'       => function ($item) {
            return ($item['status'] == Project::StatusArchived);
        },
        'fnRenderRow'       => function ($widget, $item, $template) {

            // archive class
            if ($widget->isArchive($item) ) {
                $template = strtr ($template, [
                    '{class}' => '{class} archived'
                ]);
            }

            // closed class
            if ($item['status'] == Project::StatusClosed) {
                $template = strtr ($template, [
                    '{class}' => '{class} closed'
                ]);
            }

            return $template;
        },
        'fnRenderItem'      => function ($widget, $item, $template) {
            return strtr($template, [
                '{name}'            => $item['name'],
                '{url}'             => Url::projectsSettings($item['huu']),
                '{isPublic}'        => $widget->renderToggleCheck ($item['is_public']),
                '{createdOn}'       => date('m/d/Y', strtotime($item['created_on'])),
                '{buttons}'         => $widget->renderButtons($item),
            ]);
        },
    ])?>
</div>