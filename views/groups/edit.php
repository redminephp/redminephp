<?php
    use app\widgets\ActiveForm;
    use app\helpers\Url;
    use yii\helpers\Html;
    use app\helpers\Block;
    use app\models\Group;
    use app\models\User;
    use app\helpers\Content;

    $tabsContent = Content::create();
?>

<?=\app\widgets\Breadcrumbs::widget([
    'items'         => [
        ['title' => 'Groups', 'url' => Url::groups()],
        ['title' => $model->lastname],
    ],
])?>

    <script src="/web/js/jstoolbar/jstoolbar-textile.min.js?1370076409" type="text/javascript"></script>
    <script src="/web/js/jstoolbar/jstoolbar-en.js?1321098518" type="text/javascript"></script>
    <link href="/web/css/jstoolbar.css?1370076409" media="screen" rel="stylesheet" type="text/css" />
    <script src="/web/js/select_list_move.js?1393764265" type="text/javascript"></script>

<?php
    $tabsContent->begin();
    $form = ActiveForm::begin(['action' => '']);
?>
    <div class="box tabular">

        <?= $form->field($model, 'name')->textInput(['id' => 'group_name', 'name' => 'group[name]', 'size' => 60])->required(); ?>

    </div>

    <input type="submit" value="Save" name="commit">

<?php ActiveForm::end(); $tabsContent->end('general'); ?>


<?php $tabsContent->begin(); ?>

    <?=Block::groupuserlistedit($model)?>

<?php $tabsContent->end('users'); ?>


<?php $tabsContent->begin(); ?>

    <?=Block::groupmembershipslistedit($model)?>

<?php $tabsContent->end('memberships'); ?>


<?php
$items = [];
foreach (Group::$tabs as $key => $info) {
    $items[] = ['key' => $key, 'label' => $info['title']];
}
?>
<?=\app\widgets\Tab::widget([
    'items'         => $items,
    'activeKey'     => $model->tab,
    'contentTabs'   => $tabsContent,
    'urlParams'     => ['groupsEdit', [$model->id, '{key}']],
])?>