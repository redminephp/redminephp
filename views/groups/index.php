<?php
    use app\helpers\Url;
?>

<div class="contextual">
    <a class="icon icon-add" href="<?=Url::groupsNew()?>">New group</a>
</div>

<h2>Groups</h2>

<?=\app\widgets\ListSort::widget([
    'type'              => 'group',
    'itemTemplate'      => '<td class="name"><a href="{url}">{name}</a></td>
                            <td>{userCount}</td>
                            <td class="buttons">{buttons}</td>',
    'urlEdit'           => function ($item) { return Url::groupsEdit($item['id']); },
    'urlView'           => function ($item) { return Url::groupsHandler($item['id']); },
    'columnList'        => [
        ['title'    => 'Group'],
        ['title'    => 'Users'],
        ['title'    => ''],
    ],
    'items'             => $list,
    'buttonsTemplate'   => '{buttonDelete}',
    'fnRenderItem'      => function ($widget, $item, $template) {
        return strtr($template, [
            '{name}'            => $item['name'],
            '{url}'             => Url::groupsEdit($item['id']),
            '{userCount}'       => 0,
        ]);
    },
])?>