<?php
    use app\widgets\ActiveForm;
    use app\helpers\Url;
    use yii\helpers\Html;
    use app\helpers\Block;
?>


<?=\app\widgets\Breadcrumbs::widget([
    'items'         => [
        ['title' => 'Groups', 'url' => Url::groups()],
        ['title' => 'New group'],
    ],
])?>

<?php $form = ActiveForm::begin(['action' => '', 'id' => 'new_group', 'class' => 'new_group']); ?>

    <?=Block::flexnotifications (['error' => $model->getClearErrorList()])?>

    <div class="box tabular">

        <?= $form->field($model, 'name')->textInput(['id' => 'group_name', 'name' => 'group[name]', 'size' => 60])->required(); ?>

    </div>

    <p>
        <input type="submit" value="Create" name="commit">
        <input type="submit" value="Create and continue" name="continue">
    </p>

<?php ActiveForm::end(); ?>