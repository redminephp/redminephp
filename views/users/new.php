<h2><a href="/users">Users</a> » New user</h2>

<form method="post" id="new_user" class="new_user" action="/users" accept-charset="UTF-8"><div style="margin:0;padding:0;display:inline"><input type="hidden" value="✓" name="utf8"><input type="hidden" value="CXZ+Uq7iOQjajhc+Q7eA+XPGV1d4HQGVTYzbFRzFudY=" name="authenticity_token"></div>


<div id="user_form">
<!--[form:user]-->
<div class="splitcontentleft">
    <fieldset class="box tabular">
        <legend>Information</legend>
        <p><label for="user_login">Login<span class="required"> *</span></label><input type="text" value="" size="25" name="user[login]" id="user_login"></p>
        <p><label for="user_firstname">First name<span class="required"> *</span></label><input type="text" value="" size="30" name="user[firstname]" id="user_firstname"></p>
        <p><label for="user_lastname">Last name<span class="required"> *</span></label><input type="text" value="" size="30" name="user[lastname]" id="user_lastname"></p>
        <p><label for="user_mail">Email<span class="required"> *</span></label><input type="text" value="" size="30" name="user[mail]" id="user_mail"></p>
        <p><label for="user_language">Language</label><select name="user[language]" id="user_language"><option value="">(auto)</option>
                <option value="sq">Albanian (Shqip)</option>
                <option value="ar">Arabic (عربي)</option>
                <option value="az">Azerbaijanian (Azeri)</option>
                <option value="bs">Bosanski</option>
                <option value="bg">Bulgarian (Български)</option>
                <option value="ca">Català</option>
                <option value="cs">Czech (Čeština)</option>
                <option value="da">Danish (Dansk)</option>
                <option value="de">Deutsch</option>
                <option value="et">Eesti</option>
                <option selected="selected" value="en">English</option>
                <option value="en-GB">English (British)</option>
                <option value="es">Español</option>
                <option value="eu">Euskara</option>
                <option value="fi">Finnish (Suomi)</option>
                <option value="fr">Français</option>
                <option value="gl">Galego</option>
                <option value="el">Greek (Ελληνικά)</option>
                <option value="he">Hebrew (עברית)</option>
                <option value="hr">Hrvatski</option>
                <option value="id">Indonesia</option>
                <option value="it">Italiano</option>
                <option value="ja">Japanese (日本語)</option>
                <option value="ko">Korean (한국어)</option>
                <option value="lv">Latvian (Latviešu)</option>
                <option value="lt">Lithuanian (lietuvių)</option>
                <option value="mk">Macedonian (Македонски)</option>
                <option value="hu">Magyar</option>
                <option value="mn">Mongolian (Монгол)</option>
                <option value="nl">Nederlands</option>
                <option value="no">Norwegian (Norsk bokmål)</option>
                <option value="fa">Persian (پارسی)</option>
                <option value="pl">Polski</option>
                <option value="pt">Português</option>
                <option value="pt-BR">Português(Brasil)</option>
                <option value="ro">Română</option>
                <option value="ru">Russian (Русский)</option>
                <option value="sr-YU">Serbian (Srpski)</option>
                <option value="sr">Serbian Cyrillic (Српски)</option>
                <option value="zh">Simplified Chinese (简体中文)</option>
                <option value="sk">Slovenčina</option>
                <option value="sl">Slovenščina</option>
                <option value="sv">Svenska</option>
                <option value="th">Thai (ไทย)</option>
                <option value="vi">Tiếng Việt</option>
                <option value="zh-TW">Traditional Chinese (繁體中文)</option>
                <option value="tr">Türkçe</option>
                <option value="uk">Ukrainian (Українська)</option></select></p>


        <p><label for="user_admin">Administrator</label><input type="hidden" value="0" name="user[admin]"><input type="checkbox" value="1" name="user[admin]" id="user_admin"></p>

    </fieldset>

    <fieldset class="box tabular">
        <legend>Authentication</legend>
        <div style="" id="password_fields">
            <p><label for="user_password">Password<span class="required"> *</span></label><input type="password" size="25" name="user[password]" id="user_password">
                <em class="info">Must be at least -2 characters long.</em></p>
            <p><label for="user_password_confirmation">Confirmation<span class="required"> *</span></label><input type="password" size="25" name="user[password_confirmation]" id="user_password_confirmation"></p>
            <p><label for="user_generate_password">Generate password</label><input type="hidden" value="0" name="user[generate_password]"><input type="checkbox" value="1" name="user[generate_password]" id="user_generate_password"></p>
            <p><label for="user_must_change_passwd">Must change password at next logon</label><input type="hidden" value="0" name="user[must_change_passwd]"><input type="checkbox" value="1" name="user[must_change_passwd]" id="user_must_change_passwd"></p>
        </div>
    </fieldset>
</div>

<div class="splitcontentright">
    <fieldset class="box">
        <legend>Email notifications</legend>
        <p>
            <label for="user_mail_notification" class="hidden-for-sighted">Mail notification settings</label>
            <select onchange="if (this.value == &quot;selected&quot;) {$(&quot;#notified-projects&quot;).show();} else {$(&quot;#notified-projects&quot;).hide();}" name="user[mail_notification]" id="user_mail_notification"><option value="all">For any event on all my projects</option>
                <option selected="selected" value="only_my_events">Only for things I watch or I'm involved in</option>
                <option value="only_assigned">Only for things I am assigned to</option>
                <option value="only_owner">Only for things I am the owner of</option>
                <option value="none">No events</option></select>
        </p>
        <div style="display:none;" id="notified-projects">

            <input type="hidden" value="" name="user[notified_project_ids][]" id="user_notified_project_ids_">
            <p><em class="info">For unselected projects, you will only receive notifications about things you watch or you're involved in (eg. issues you're the author or assignee).</em></p>
        </div>

        <p>
            <input type="hidden" value="0" name="pref[no_self_notified]"><input type="checkbox" value="1" name="pref[no_self_notified]" id="pref_no_self_notified">
            <label for="pref_no_self_notified">I don't want to be notified of changes that I make myself</label>
        </p>

    </fieldset>

    <fieldset class="box tabular">
        <legend>Preferences</legend>

        <p><label for="pref_hide_mail">Hide my email address</label><input type="hidden" value="0" name="pref[hide_mail]"><input type="checkbox" value="1" name="pref[hide_mail]" id="pref_hide_mail"></p>
        <p><label for="pref_time_zone">Time zone</label><select name="pref[time_zone]" id="pref_time_zone"><option value="">&nbsp;</option>
                <option value="American Samoa">(GMT-11:00) American Samoa</option>
                <option value="International Date Line West">(GMT-11:00) International Date Line West</option>
                <option value="Midway Island">(GMT-11:00) Midway Island</option>
                <option value="Hawaii">(GMT-10:00) Hawaii</option>
                <option value="Alaska">(GMT-09:00) Alaska</option>
                <option value="Pacific Time (US &amp; Canada)">(GMT-08:00) Pacific Time (US &amp; Canada)</option>
                <option value="Tijuana">(GMT-08:00) Tijuana</option>
                <option value="Arizona">(GMT-07:00) Arizona</option>
                <option value="Chihuahua">(GMT-07:00) Chihuahua</option>
                <option value="Mazatlan">(GMT-07:00) Mazatlan</option>
                <option value="Mountain Time (US &amp; Canada)">(GMT-07:00) Mountain Time (US &amp; Canada)</option>
                <option value="Central America">(GMT-06:00) Central America</option>
                <option value="Central Time (US &amp; Canada)">(GMT-06:00) Central Time (US &amp; Canada)</option>
                <option value="Guadalajara">(GMT-06:00) Guadalajara</option>
                <option value="Mexico City">(GMT-06:00) Mexico City</option>
                <option value="Monterrey">(GMT-06:00) Monterrey</option>
                <option value="Saskatchewan">(GMT-06:00) Saskatchewan</option>
                <option value="Bogota">(GMT-05:00) Bogota</option>
                <option value="Eastern Time (US &amp; Canada)">(GMT-05:00) Eastern Time (US &amp; Canada)</option>
                <option value="Indiana (East)">(GMT-05:00) Indiana (East)</option>
                <option value="Lima">(GMT-05:00) Lima</option>
                <option value="Quito">(GMT-05:00) Quito</option>
                <option value="Caracas">(GMT-04:30) Caracas</option>
                <option value="Atlantic Time (Canada)">(GMT-04:00) Atlantic Time (Canada)</option>
                <option value="Georgetown">(GMT-04:00) Georgetown</option>
                <option value="La Paz">(GMT-04:00) La Paz</option>
                <option value="Santiago">(GMT-04:00) Santiago</option>
                <option value="Newfoundland">(GMT-03:30) Newfoundland</option>
                <option value="Brasilia">(GMT-03:00) Brasilia</option>
                <option value="Buenos Aires">(GMT-03:00) Buenos Aires</option>
                <option value="Greenland">(GMT-03:00) Greenland</option>
                <option value="Mid-Atlantic">(GMT-02:00) Mid-Atlantic</option>
                <option value="Azores">(GMT-01:00) Azores</option>
                <option value="Cape Verde Is.">(GMT-01:00) Cape Verde Is.</option>
                <option value="Casablanca">(GMT+00:00) Casablanca</option>
                <option value="Dublin">(GMT+00:00) Dublin</option>
                <option value="Edinburgh">(GMT+00:00) Edinburgh</option>
                <option value="Lisbon">(GMT+00:00) Lisbon</option>
                <option value="London">(GMT+00:00) London</option>
                <option value="Monrovia">(GMT+00:00) Monrovia</option>
                <option value="UTC">(GMT+00:00) UTC</option>
                <option value="Amsterdam">(GMT+01:00) Amsterdam</option>
                <option value="Belgrade">(GMT+01:00) Belgrade</option>
                <option value="Berlin">(GMT+01:00) Berlin</option>
                <option value="Bern">(GMT+01:00) Bern</option>
                <option value="Bratislava">(GMT+01:00) Bratislava</option>
                <option value="Brussels">(GMT+01:00) Brussels</option>
                <option value="Budapest">(GMT+01:00) Budapest</option>
                <option value="Copenhagen">(GMT+01:00) Copenhagen</option>
                <option value="Ljubljana">(GMT+01:00) Ljubljana</option>
                <option value="Madrid">(GMT+01:00) Madrid</option>
                <option value="Paris">(GMT+01:00) Paris</option>
                <option value="Prague">(GMT+01:00) Prague</option>
                <option value="Rome">(GMT+01:00) Rome</option>
                <option value="Sarajevo">(GMT+01:00) Sarajevo</option>
                <option value="Skopje">(GMT+01:00) Skopje</option>
                <option value="Stockholm">(GMT+01:00) Stockholm</option>
                <option value="Vienna">(GMT+01:00) Vienna</option>
                <option value="Warsaw">(GMT+01:00) Warsaw</option>
                <option value="West Central Africa">(GMT+01:00) West Central Africa</option>
                <option value="Zagreb">(GMT+01:00) Zagreb</option>
                <option value="Athens">(GMT+02:00) Athens</option>
                <option value="Bucharest">(GMT+02:00) Bucharest</option>
                <option value="Cairo">(GMT+02:00) Cairo</option>
                <option value="Harare">(GMT+02:00) Harare</option>
                <option value="Helsinki">(GMT+02:00) Helsinki</option>
                <option value="Istanbul">(GMT+02:00) Istanbul</option>
                <option value="Jerusalem">(GMT+02:00) Jerusalem</option>
                <option value="Kyiv">(GMT+02:00) Kyiv</option>
                <option value="Pretoria">(GMT+02:00) Pretoria</option>
                <option value="Riga">(GMT+02:00) Riga</option>
                <option value="Sofia">(GMT+02:00) Sofia</option>
                <option value="Tallinn">(GMT+02:00) Tallinn</option>
                <option value="Vilnius">(GMT+02:00) Vilnius</option>
                <option value="Baghdad">(GMT+03:00) Baghdad</option>
                <option value="Kuwait">(GMT+03:00) Kuwait</option>
                <option value="Minsk">(GMT+03:00) Minsk</option>
                <option value="Nairobi">(GMT+03:00) Nairobi</option>
                <option value="Riyadh">(GMT+03:00) Riyadh</option>
                <option value="Tehran">(GMT+03:30) Tehran</option>
                <option value="Abu Dhabi">(GMT+04:00) Abu Dhabi</option>
                <option value="Baku">(GMT+04:00) Baku</option>
                <option value="Moscow">(GMT+04:00) Moscow</option>
                <option value="Muscat">(GMT+04:00) Muscat</option>
                <option value="St. Petersburg">(GMT+04:00) St. Petersburg</option>
                <option value="Tbilisi">(GMT+04:00) Tbilisi</option>
                <option value="Volgograd">(GMT+04:00) Volgograd</option>
                <option value="Yerevan">(GMT+04:00) Yerevan</option>
                <option value="Kabul">(GMT+04:30) Kabul</option>
                <option value="Islamabad">(GMT+05:00) Islamabad</option>
                <option value="Karachi">(GMT+05:00) Karachi</option>
                <option value="Tashkent">(GMT+05:00) Tashkent</option>
                <option value="Chennai">(GMT+05:30) Chennai</option>
                <option value="Kolkata">(GMT+05:30) Kolkata</option>
                <option value="Mumbai">(GMT+05:30) Mumbai</option>
                <option value="New Delhi">(GMT+05:30) New Delhi</option>
                <option value="Sri Jayawardenepura">(GMT+05:30) Sri Jayawardenepura</option>
                <option value="Kathmandu">(GMT+05:45) Kathmandu</option>
                <option value="Almaty">(GMT+06:00) Almaty</option>
                <option value="Astana">(GMT+06:00) Astana</option>
                <option value="Dhaka">(GMT+06:00) Dhaka</option>
                <option value="Ekaterinburg">(GMT+06:00) Ekaterinburg</option>
                <option value="Rangoon">(GMT+06:30) Rangoon</option>
                <option value="Bangkok">(GMT+07:00) Bangkok</option>
                <option value="Hanoi">(GMT+07:00) Hanoi</option>
                <option value="Jakarta">(GMT+07:00) Jakarta</option>
                <option value="Novosibirsk">(GMT+07:00) Novosibirsk</option>
                <option value="Beijing">(GMT+08:00) Beijing</option>
                <option value="Chongqing">(GMT+08:00) Chongqing</option>
                <option value="Hong Kong">(GMT+08:00) Hong Kong</option>
                <option value="Krasnoyarsk">(GMT+08:00) Krasnoyarsk</option>
                <option value="Kuala Lumpur">(GMT+08:00) Kuala Lumpur</option>
                <option value="Perth">(GMT+08:00) Perth</option>
                <option value="Singapore">(GMT+08:00) Singapore</option>
                <option value="Taipei">(GMT+08:00) Taipei</option>
                <option value="Ulaan Bataar">(GMT+08:00) Ulaan Bataar</option>
                <option value="Urumqi">(GMT+08:00) Urumqi</option>
                <option value="Irkutsk">(GMT+09:00) Irkutsk</option>
                <option value="Osaka">(GMT+09:00) Osaka</option>
                <option value="Sapporo">(GMT+09:00) Sapporo</option>
                <option value="Seoul">(GMT+09:00) Seoul</option>
                <option value="Tokyo">(GMT+09:00) Tokyo</option>
                <option value="Adelaide">(GMT+09:30) Adelaide</option>
                <option value="Darwin">(GMT+09:30) Darwin</option>
                <option value="Brisbane">(GMT+10:00) Brisbane</option>
                <option value="Canberra">(GMT+10:00) Canberra</option>
                <option value="Guam">(GMT+10:00) Guam</option>
                <option value="Hobart">(GMT+10:00) Hobart</option>
                <option value="Melbourne">(GMT+10:00) Melbourne</option>
                <option value="Port Moresby">(GMT+10:00) Port Moresby</option>
                <option value="Sydney">(GMT+10:00) Sydney</option>
                <option value="Yakutsk">(GMT+10:00) Yakutsk</option>
                <option value="New Caledonia">(GMT+11:00) New Caledonia</option>
                <option value="Vladivostok">(GMT+11:00) Vladivostok</option>
                <option value="Auckland">(GMT+12:00) Auckland</option>
                <option value="Fiji">(GMT+12:00) Fiji</option>
                <option value="Kamchatka">(GMT+12:00) Kamchatka</option>
                <option value="Magadan">(GMT+12:00) Magadan</option>
                <option value="Marshall Is.">(GMT+12:00) Marshall Is.</option>
                <option value="Solomon Is.">(GMT+12:00) Solomon Is.</option>
                <option value="Wellington">(GMT+12:00) Wellington</option>
                <option value="Nuku'alofa">(GMT+13:00) Nuku'alofa</option>
                <option value="Samoa">(GMT+13:00) Samoa</option>
                <option value="Tokelau Is.">(GMT+13:00) Tokelau Is.</option></select></p>
        <p><label for="pref_comments_sorting">Display comments</label><select name="pref[comments_sorting]" id="pref_comments_sorting"><option value="asc">In chronological order</option>
                <option value="desc">In reverse chronological order</option></select></p>
        <p><label for="pref_warn_on_leaving_unsaved">Warn me when leaving a page with unsaved text</label><input type="hidden" value="0" name="pref[warn_on_leaving_unsaved]"><input type="checkbox" value="1" name="pref[warn_on_leaving_unsaved]" id="pref_warn_on_leaving_unsaved" checked="checked"></p>


    </fieldset>
</div>
</div>
<div style="clear:left;"></div>
<!--[eoform:user]-->

<script type="text/javascript">
    //&lt;![CDATA[

    $(document).ready(function(){
        $('#user_generate_password').change(function(){
            var passwd = $('#user_password, #user_password_confirmation');
            if ($(this).is(':checked')){
                passwd.val('').attr('disabled', true);
            }else{
                passwd.removeAttr('disabled');
            }
        }).trigger('change');
    });

    //]]&gt;
</script>
<p>
    <input type="submit" value="Create" name="commit">
    <input type="submit" value="Create and continue" name="continue">
</p>
</form>