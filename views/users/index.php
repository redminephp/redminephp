<?php
    use app\helpers\Url;
?>

<div class="contextual">
    <a class="icon icon-add" href="<?=Url::usersNew()?>">New user</a>
</div>

<h2>Users</h2>

<form method="get" action="/users" accept-charset="UTF-8"><div style="margin:0;padding:0;display:inline"><input type="hidden" value="✓" name="utf8"></div>
    <fieldset><legend>Filters</legend>
        <label for="status">Status:</label>
        <select onchange="this.form.submit(); return false;" name="status" id="status" class="small"><option value="">all</option>
            <option selected="selected" value="1">active (2)</option>
            <option value="2">registered (0)</option>
            <option value="3">locked (0)</option></select>

        <label for="group_id">Group:</label>
        <select onchange="this.form.submit(); return false;" name="group_id" id="group_id"><option></option><option value="4">group 1</option>
            <option value="5">group 2</option>
            <option value="6">group 3</option></select>

        <label for="name">User:</label>
        <input type="text" size="30" name="name" id="name">
        <input type="submit" value="Apply" class="small">
        <a class="icon icon-reload" href="/users">Clear</a>
    </fieldset>
</form>&nbsp;

<div class="autoscroll">
    <table class="list">
        <thead><tr>
            <th title="Sort by &quot;Login&quot;"><a class="sort asc" href="/users?sort=login%3Adesc">Login</a></th>
            <th title="Sort by &quot;First name&quot;"><a href="/users?sort=firstname%2Clogin">First name</a></th>
            <th title="Sort by &quot;Last name&quot;"><a href="/users?sort=lastname%2Clogin">Last name</a></th>
            <th title="Sort by &quot;Email&quot;"><a href="/users?sort=mail%2Clogin">Email</a></th>
            <th title="Sort by &quot;Administrator&quot;"><a href="/users?sort=admin%3Adesc%2Clogin">Administrator</a></th>
            <th title="Sort by &quot;Created&quot;"><a href="/users?sort=created_on%3Adesc%2Clogin">Created</a></th>
            <th title="Sort by &quot;Last connection&quot;"><a href="/users?sort=last_login_on%3Adesc%2Clogin">Last connection</a></th>
            <th></th>
        </tr></thead>
        <tbody>
        <tr class="user active odd">
            <td class="username"><a href="/users/2/edit">trickylabs</a></td>
            <td class="firstname">Redmine</td>
            <td class="lastname">Admin</td>
            <td class="email"><a href="mailto:demo@example.net">demo@example.net</a></td>
            <td class="tick"><img src="/images/toggle_check.png?1321098518" alt="Toggle_check"></td>
            <td class="created_on">01/19/2014 01:21 PM</td>
            <td class="last_login_on">03/02/2015 01:40 PM</td>
            <td class="buttons">


            </td>
        </tr>
        <tr class="user active even">
            <td class="username"><a href="/users/7/edit">wertwert</a></td>
            <td class="firstname">wertwert</td>
            <td class="lastname">wertwert</td>
            <td class="email"><a href="mailto:trickylabs@ertr.ty">trickylabs@ertr.ty</a></td>
            <td class="tick"></td>
            <td class="created_on">01/27/2015 10:51 AM</td>
            <td class="last_login_on"></td>
            <td class="buttons">
                <a rel="nofollow" data-method="put" class="icon icon-lock" href="/users/7?user%5Bstatus%5D=3">Lock</a>
                <a rel="nofollow" data-method="delete" data-confirm="Are you sure?" class="icon icon-del" href="/users/7?back_url=%2Fusers">Delete</a>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<p class="pagination"><span class="items">(1-2/2)</span></p>