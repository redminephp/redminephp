<?php
use app\helpers\Block;
use app\helpers\Url;
use app\helpers\Notify;

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

use app\models\User;
use app\models\user\UserAuth;

/**
 * @var \yii\web\View $this
 * @var string $content
 */
AppAsset::register($this);

//$this->registerCssFile('/test.css', [], ['media' => 'print']);

$sitebarModule  = Yii::$app->controller->sitebarModule;
?>

<?php $this->beginPage() ?>

<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">

<head>
	<meta charset="<?= Yii::$app->charset ?>"/>
	<title><?= Html::encode($this->title) ?></title>
	<?php $this->head() ?>
</head>

<body class="controller-welcome action-index">
<?php $this->beginBody() ?>

<div id="wrapper">
<div id="wrapper2">
<div id="wrapper3">
<div id="top-menu">

	<?php if (!Yii::$app->user->IsGuest): ?>
		<div id="account">
			<ul>
				<li><a class="my-account" href="/my/account">My account</a></li>
				<li><a rel="nofollow" data-method="post" class="logout" href="<?=Url::logout()?>">Sign out</a></li>
			</ul>
		</div>
		<div id="loggedas">Logged in as <a class="user active" href="<?=Url::usersView(UserAuth::info('id'))?>"><?=UserAuth::info('login')?></a></div>
	<?php else: ?>
		<div id="account">
			<ul>
				<li><a href="<?=Url::login()?>" class="login">Sign in</a></li>
				<li><a href="/" class="register">Register</a></li>
			</ul>
		</div>
	<?php endif; ?>
    
    <ul>
		<li><a href="/" class="home">Home</a></li>
		<?php if (!Yii::$app->user->IsGuest): ?>
			<li><a href="/my/page" class="my-page">My page</a></li>
			<li><a href="<?=Url::projects()?>" class="projects">Projects</a></li>
			<li><a href="<?=Url::admin()?>" class="administration">Administration</a></li>
		<?php else: ?>
			<li><a href="<?=Url::projects()?>" class="projects">Projects</a></li>
		<?php endif ?>
		<li><a href="/" class="help">Help</a></li>
	</ul>
</div>

<div id="header">
    <div id="quick-search">
        <form accept-charset="UTF-8" action="/" method="get"><div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="&#x2713;" /></div>
        
        <label for='q'>
          <a href="/" accesskey="4">Search</a>:
        </label>
        <input accesskey="f" class="small" id="q" name="q" size="20" type="text" />
</form>        
    </div>

    <h1>RedminePHP</h1>

    <?php if (Yii::$app->controller->menuHeader): ?>
        <?=Block::menuheader(Yii::$app->controller->menuHeader)?>
    <?php endif; ?>

</div>

        <div id="main" class="<?=(!$sitebarModule)?'nosidebar':''?>">
            <div id="sidebar">

                <?php if ($sitebarModule): ?>
                    <?=Block::$sitebarModule()?>
                <?php endif; ?>

            </div>

            <div id="content">

                <?=Block::flexnotifications (Yii::$app->controller->notifications, 'single')?>

                <?=$content?>

                <div style="clear:both;"></div>
            </div>
        </div>
    </div>
    
    <div id="ajax-indicator" style="display:none;"><span>Загрузка...</span></div>
    <div id="ajax-modal" style="display:none;"></div>
  

    <div id="footer">
      <div class="bgl"><div class="bgr">
        Powered by <a href="/">RedminePHP</a> from original <a href="http://www.redmine.org/" rel="nofollow">Redmine</a> &copy; 2014
      </div></div>
    </div>

</div>
</div>


<?php if (false): ?>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-47287766-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<?php endif; ?>


<script type="text/javascript">//<![CDATA[
    $(window).load(function(){warnLeavingUnsaved('The current page contains unsaved text that will be lost if you leave this page.');});
//]]></script>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>