<?php

$params = require(__DIR__ . '/params.php');
$db = require(__DIR__ . '/db.php');

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'extensions' => require(__DIR__ . '/../vendor/yiisoft/extensions.php'),
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\user\UserAuth',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'class'=>'yii\web\UrlManager',
            'enablePrettyUrl'	=> true,
            'showScriptName'	=> false,
            'ruleConfig' => ['class' => 'yii\web\UrlRule'],
            'rules'=> [
                ''		=> 'site/index',
                '<action:(login|logout|register)>' => 'site/<action>',

                // projects
                'projects/new'                                              => 'projects/new',
                'projects/<projectHuu:[a-z0-9\_\-]{1,}>'                    => 'projects/handler',
                'projects/<projectHuu:[a-z0-9\_\-]{1,}><tab>'               => 'projects/view',
                'projects/<projectHuu:[a-z0-9\_\-]{1,}>/settings<tab>'      => 'projects/settings',
                'projects/<projectHuu:[a-z0-9\_\-]{1,}>/<action>'           => 'projects/<action>',
                '<action:(projects)>' => 'projects',

                // groups
                'groups/<itemId:[0-9]{1,}>/edit'                                => 'groups/edit',
                'groups/<itemId:new>'                                           => 'groups/new',
                'groups/<itemId:[0-9]{1,}>'                                     => 'groups/view',
                'groups/<itemId:[0-9]{1,}>/users'                               => 'groups/users',
                'DELETE groups/<itemId:[0-9]{1,}>/users/<userId:[0-9]{1,}>'     => 'groups/users',
                'groups/<itemId:[0-9]{1,}>/autocomplete_for_user'               => 'groups/autocompleteuser',
                'groups/edit_membership/<itemId:[0-9]{1,}>'                     => 'groups/editmembership',
                'groups/destroy_membership/<itemId:[0-9]{1,}>'                  => 'groups/destroymembership',

                // http://trickylabs.m.redmine.org/groups/edit_membership/4?membership_id=73

                // issue statuses
                'issue_statuses'                           => 'issue/status',
                'issue_statuses/<itemId:[0-9]{1,}>/edit'   => 'issue/status/edit',
                'issue_statuses/<itemId:new>'              => 'issue/status/edit',
                'issue_statuses/<itemId:[0-9]{1,}>'        => 'issue/status/handler',

                // roles
                'roles/<itemId:[0-9]{1,}>/edit'    => 'roles/edit',
                'roles/<itemId:new>'               => 'roles/edit',
                'roles/<itemId:[0-9]{1,}>'         => 'roles/handler',

                // trackers
                'trackers/<itemId:[0-9]{1,}>/edit'     => 'trackers/edit',
                'trackers/<itemId:new>'                => 'trackers/edit',
                'trackers/<itemId:[0-9]{1,}>'          => 'trackers/handler',

                '<action:(admin)>' => 'admin',

                //'<action:(settings)>' => 'settings',
                //'<action:(settings)>' => 'settings/<action>',
            ],
            //'baseUrl' => '/web/',
        ],
        'mail' => [
            'class' => 'yii\swiftmailer\Mailer',
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = 'yii\debug\Module';
    $config['modules']['gii'] = 'yii\gii\Module';
}

return $config;
