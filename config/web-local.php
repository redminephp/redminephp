<?php

$config = require(__DIR__ . '/web.php');

$config['components']['db'] = [
	'class'		=> 'yii\db\Connection',
	'dsn'		=> 'mysql:host=localhost;dbname=redmineyii',
	'username'	=> 'root', 
	'password'	=> '',
];

return $config;
