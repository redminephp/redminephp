<?php

namespace app\controllers;

use Yii;
use yii\web\AccessControl;
use yii\web\Controller;
use yii\web\VerbFilter;

use app\helpers\Url;
use app\models\Role;

use app\helpers\ArrayHelper;


/**
 * Class RolesController
 * @package app\controllers
 */
class RolesController extends BaseItem
{

    // класс модели
    public $modelClass = '\app\models\Role';

    // название типа
    public $typeName = 'role';


    /**
     * Инициализация
     */
    public function init ()
    {
        parent::init ();

        $this->sitebarModule    = "menuadminright";
        $this->menuAdminRight   = "roles";
    }


    /**
     * Url списка
     *
     * @return string
     */
    public function urlList () {
        return Url::roles();
    }


    /**
     * Список проектов
     */
    public function actionIndex()
    {
        $data = [];

        // общий список
        $list = Role::getList (['fetchNewKeys' => [
            Role::Id    => 'id',
            Role::Name  => 'name',
        ]]);

        $data['list']           = $list;
        $data['builtin_list']   = Role::getBuiltinList($list);

        return $this->render('index', $data);
    }


    /**
     * Получаем модель
     *
     * @return array|object
     */
    protected function getModel ($itemId) {

        $is_copy = false;
        if (isset($_GET['copy']) ) {
            $itemId = intval ($_GET['copy']);
            $is_copy = true;
        }

        $model = parent::getModel ($itemId);

        if ($itemId !== 'new' && $is_copy) {
            $model->name = '';
        }

        return $model;
    }

}