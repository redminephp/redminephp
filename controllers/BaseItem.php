<?php

namespace app\controllers;

use yii\web\Controller;

use app\helpers\Url;


/**
 * Class BaseItem
 * @package app\controllers
 */
class BaseItem extends Base
{


    /**
     * Url списка
     *
     * @return string
     */
    public function urlList () {
        return Url::base();
    }


    /**
     * Удаление
     *
     * @param int
     */
    public function actionHandler ($itemId) {

        // класс модели
        $modelClass = $this->modelClass;

        // where
        $where = $modelClass::positionListWhere ();

        // по методу выполняем действия
        $method = \Yii::$app->request->getMethod();
        switch ($method) {

            // удаление
            case 'DELETE':
                $modelClass::deleteById ($itemId);
                break;

            // сортировка
            case 'PUT':
                $list = $modelClass::getList (['fetchValue' => $modelClass::Id, 'where' => $where]);

                $key = array_search($itemId, $list);

                $sort_to = \Yii::$app->request->get ($this->typeName);
                $sort_to = isset($sort_to['move_to']) ? $sort_to['move_to'] : false;
                switch ($sort_to) {
                    case 'highest':
                        unset ($list[$key]);
                        array_unshift ($list, $itemId);
                        break;
                    case 'higher':
                        if ($key > 0) {
                            $list[$key]     = $list[$key-1];
                            $list[$key-1]   = $itemId;
                        }
                        break;
                    case 'lower':
                        if ($key < count($list)-1) {
                            $list[$key]     = $list[$key+1];
                            $list[$key+1]   = $itemId;
                        }
                        break;
                    case 'lowest':
                        unset ($list[$key]);
                        $list[] = $itemId;
                        break;
                }

                // добавление sql-where
                $sql_where = '';
                if ($where) {
                    $sql_where = 'where '.$where;
                }

                // выполняем
                \Yii::$app->db->createCommand("set @i := 0")->execute();
                $sql = \Yii::$app->db->createCommand("update ". $modelClass::Table ." set ". $modelClass::Position ." = (@i := @i + 1) $sql_where order by field (". $modelClass::Id .", ". implode(',', $list) .")")->execute();

                break;
        }

        $this->redirect($this->urlList());
    }


    /**
     * Редактирование
     *
     * @param int
     */
    public function actionEdit ($itemId) {

        // инициализация модели
        $model = $this->edit ($itemId);

        $data = [];

        $data['model'] = $model;

        return $this->render('edit', $data);
    }


    /**
     * Edit
     *
     * @param $itemId
     * @return array|object
     */
    protected function edit ($itemId) {

        // инициализация объекта
        $model = $this->getModel ($itemId);

        if (isset($_POST[$this->typeName]) ) {

            // загружаем post-данные
            $model->load($_POST[$this->typeName], '');

            // атрибуты
            $attributes = $model->getAttributesInner();

            // проверяем на валидность
            if ($model->validate($attributes) ) {

                // уведомление
                $notify_message = 'Successful update.';
                if (!$model->id) {
                    $notify_message = 'Successful creation.';
                }
                $this->setTextNotify($notify_message);

                // добавляем проект
                $model->save ();

                // редиректим
                $this->redirect($this->urlList());
            }
            else {
                // декодируем атрибуты обратно после валидации
                $model->decodeAttributes(true);
            }

        }

        return $model;
    }

}