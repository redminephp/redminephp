<?php

namespace app\controllers\Issue;

use Yii;
use yii\web\AccessControl;
use yii\web\Controller;
use yii\web\VerbFilter;

use app\helpers\Url;
use app\models\Issue\Status;

use app\helpers\ArrayHelper;


/**
 * Class StatusController
 * @package app\controllers\Issue
 */
class StatusController extends \app\controllers\BaseItem
{


    // класс модели
    public $modelClass = '\app\models\Issue\Status';

    // название типа
    public $typeName = 'issue_status';


    /**
     * Инициализация
     */
    public function init ()
    {
        $this->sitebarModule    = "menuadminright";
        $this->menuAdminRight   = "issue_statuses";
    }


    /**
     * Url списка
     *
     * @return string
     */
    public function urlList () {
        return Url::issueStatus();
    }


    /**
     * Список проектов
     */
    public function actionIndex()
    {
        $data = [];

        $data['list']   = Status::getList (['fetchNewKeys' => [
            Status::Id          => 'id',
            Status::Name        => 'name',
            Status::IsClosed    => 'is_closed',
            Status::IsDefault   => 'is_default',
        ]]);
        $data['count']  = Status::getList (['onlyCount' => true]);

        return $this->render('index', $data);
    }


}