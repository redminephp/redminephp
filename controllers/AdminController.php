<?php

namespace app\controllers;

use app\helpers\ArrayHelper;
use Yii;
use yii\web\AccessControl;
use yii\web\Controller;
use yii\web\VerbFilter;

use app\models\User;
use app\models\user\UserAuth;
use app\models\user\UserToken;

use app\models\Settings;
use app\models\Project;

use app\helpers\Url;

class AdminController extends Base
{


    public function actionIndex()
    {
        return $this->render('index');
    }


    /**
     * Список проектов
     */
    public function actionProjects ()
    {
        $this->sitebarModule    = "menuadminright";
        $this->menuAdminRight   = "projects";

        $where = [
            'fetchNewKeys'  => [
                Project::Id         => 'id',
                Project::ParentId   => 'parent_id',
                Project::Name       => 'name',
                Project::Identifier => 'huu',
                Project::Status     => 'status',
                Project::IsPublic   => 'is_public',
                Project::CreatedOn  => 'created_on',
            ],
            'fetchCascade'  => [
                'fieldId'       => 'id',
                'fieldParentId' => 'parent_id',
                'parentId'      => 0,
            ]
        ];

        // загружаем post-данные
        $project = Project::object();
        $project->load($_GET, '');

        // активный статус
        $activeStatus = Project::StatusActive; $keyword = '';
        if ($_GET && $project->validate(['status']) ) {
            $activeStatus = $project->status;

            // фильтр по словам
            if ($project->name) {
                $where['filter'] = [
                    ['like', Project::Name, $project->name]
                ];
                $keyword = $project->name;

                // если фильтр по словам активен - убираем каскадность
                unset ($where['fetchCascade']);
            }
        }

        // если есть активный статус
        if ($activeStatus) {
            $where[Project::Status] = $activeStatus;
        }


        $data = [];

        $data['keyword']        = $keyword;
        $data['activeStatus']   = $activeStatus;

        $data['list'] = Project::getList ($where);

        return $this->render('projects', $data);
    }


    /**
     * Страница настроек
     */
    public function actionSettings ()
    {
        $settings_model = new Settings ();
        $settings_model->initAttributes ();

        $this->sitebarModule    = "menuadminright";
        $this->menuAdminRight   = "settings";

        // tab
        $settings_model->load($_GET, '');
        $result = $settings_model->validate(["tab"]);
        if (!$result || !$settings_model->tab) {
            $settings_model->tab = "general";
        }

        // если форма сабмичена - проверяем на валидность и обновляем
        if (isset($_POST["settings"]) ) {
            $settings_model->load($_POST["settings"], '');

            // поля с которыми идет работа
            $attributes = Settings::$allKeys[$settings_model->tab];

            // проверяем на валидность
            $settings_model->validate($attributes);

            // восстаналиваем default'ные значения для полей с ошибками
            $settings_model->restoreDefaultValues ();

            // обновляем данные
            foreach ($attributes as $attribute) {
                $settings_model->saveItem ($attribute, $settings_model->$attribute);
            }

            // уведомление об успешном сохранении
            $this->setTextNotify("Successful update.");
        }

        $data = [];
        $data['model'] = $settings_model;
        return $this->render('settings', $data);
    }



}