<?php

namespace app\controllers;

use app\models\Member;
use Yii;
use yii\web\AccessControl;
use yii\web\Controller;
use yii\web\VerbFilter;

use app\helpers\Url;
use app\models\User;
use app\models\Group;
use app\models\Role;
use app\models\Group\User as GroupUser;
use app\models\Member\Role as MemberRole;

use app\helpers\ArrayHelper;
use app\helpers\Block;


/**
 * Class RolesController
 * @package app\controllers
 */
class GroupsController extends BaseItem
{

    // класс модели
    public $modelClass = '\app\models\Group';

    // название типа
    public $typeName = 'group';


    /**
     * Инициализация
     */
    public function init ()
    {
        parent::init ();

        $this->sitebarModule    = "menuadminright";
        $this->menuAdminRight   = "groups";
    }


    /**
     * Url списка
     *
     * @return string
     */
    public function urlList () {
        return Url::groups();
    }


    /**
     * Список проектов
     */
    public function actionIndex()
    {
        $data = [];

        // общий список
        $list = Group::getList (['fetchNewKeys' => [
            Group::Id       => 'id',
            Group::LastName => 'name',
        ]]);

        $data['list']           = $list;

        return $this->render('index', $data);
    }


    /**
     * Просмотр
     */
    public function actionView () {

    }


    /**
     * Пользователи группы
     *
     * @param int
     * @param int
     */
    public function actionUsers ($itemId, $userId = false) {

        $model = $this->getModel($itemId);

        switch (Yii::$app->request->getMethod() ) {

            // Удаление
            case 'DELETE':
                $model->scenario = 'delete';

                // загружаем post-данные
                $model->load(['users' => [$userId]], '');

                // проверяем на валидность
                if ($model->validate(['users']) ) {

                    // Удаляем запись из БД
                    GroupUser::deleteItem ($itemId, $userId);
                }
                break;


            // По умолчанию - добавление
            default:
                $model->scenario = 'add';

                if (Yii::$app->request->post('user_ids') ) {

                    // загружаем post-данные
                    $model->load(['users' => Yii::$app->request->post('user_ids')], '');

                    // проверяем на валидность
                    if ($model->validate(['users']) ) {

                        // добавляем записи в БД
                        foreach ($model->users as $userId) {
                            GroupUser::addItem($itemId, $userId);
                        }

                    }
                }
                break;
        }

        $model = $this->getModel($itemId);
        die (Block::groupuserlistedit($model));
    }


    /**
     * Редактирование
     *
     * @param int
     */
    public function actionNew ($itemId) {

        // инициализация модели
        $model = $this->edit ($itemId);

        $data = [];

        $data['model'] = $model;

        return $this->render('new', $data);
    }


    /**
     * Ф-ция автозаполнения
     *
     * @param int
     * @param string
     * @param int
     */
    public function actionAutocompleteuser ($itemId, $q = '', $page = 1) {
        $model = $this->getModel($itemId);
        die (\app\helpers\Block::groupsuserautocompletelist($model, $q, $page));
    }


    /**
     * Редактирование связей group с project
     *
     * @param $groupId
     * @param int
     */
    public function actionEditmembership ($itemId, $membership_id = false) {
        $model = $this->getModel($itemId);

        // Validate data
        $validateData = ['roles' => ArrayHelper::getValue($_POST['membership'], 'role_ids', [])];

        // Create membership item
        if ($membership_id) {
            $member = Member::getById($membership_id);
        }
        else {
            $member = new Member;

            // Add project ID to validate data
            $validateData['project_id'] = ArrayHelper::getValue($_POST['membership'], 'project_id', false);
        }

        // Load post data to model
        $member->load($validateData, '');

        // проверяем на валидность
        if ($member->validate(['project_id', 'roles']) ) {

            // Set user/group ID
            $member->user_id    = $model->id;
            $member->created_on = date('Y-m-d H:i:s');

            // Save member
            $member->save ();

            // Change relation items
            $member->saveMany2Many (Role::className(), 'roles');
        }

        // Update model var
        $model = Group::getById($model->id);

        // Return updated html
        die (Block::groupmembershipslistedit($model));
    }


    /**
     * Удаление связей group с project
     *
     * @param $groupId
     * @param string $membership_id
     */
    public function actionDestroymembership ($itemId, $membership_id = '') {
        $model = $this->getModel($itemId);
        //print_v([$groupId, $membership_id]);
    }


}