<?php

namespace app\controllers;

use Yii;
use yii\web\AccessControl;
use yii\web\Controller;
use yii\web\VerbFilter;

use app\helpers\Url;
use app\models\Tracker;

use app\helpers\ArrayHelper;

use app\models\Project\Tracker as ProjectTracker;


/**
 * Class TrackersController
 * @package app\controllers
 */
class TrackersController extends BaseItem
{

    // класс модели
    public $modelClass = '\app\models\Tracker';

    // название типа
    public $typeName = 'tracker';



    /**
     * Инициализация
     */
    public function init ()
    {
        parent::init ();

        $this->sitebarModule    = "menuadminright";
        $this->menuAdminRight   = "trackers";
    }


    /**
     * Url списка
     *
     * @return string
     */
    public function urlList () {
        return Url::trackers();
    }


    /**
     * Список проектов
     */
    public function actionIndex()
    {
        $data = [];

        $data['list']   = Tracker::getList (['fetchNewKeys' => [
            Tracker::Id     => 'id',
            Tracker::Name   => 'name',
        ]]);
        $data['count']  = Tracker::getList (['onlyCount' => true]);

        return $this->render('index', $data);
    }



}