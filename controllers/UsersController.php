<?php

namespace app\controllers;

use Yii;
use yii\web\AccessControl;
use yii\web\Controller;
use yii\web\VerbFilter;

use app\helpers\Url;
use app\models\Tracker;

use app\helpers\ArrayHelper;

use app\models\Project\Tracker as ProjectTracker;


/**
 * Class UsersController
 * @package app\controllers
 */
class UsersController extends Base
{


    /**
     * Инициализация
     */
    public function init ()
    {
        parent::init ();

        $this->sitebarModule    = "menuadminright";
        $this->menuAdminRight   = "users";
    }


    /**
     * Список проектов
     */
    public function actionIndex()
    {
        $data = [];

        /*$data['list']   = Tracker::getList (['fetchNewKeys' => [
            Tracker::Id     => 'id',
            Tracker::Name   => 'name',
        ]]);
        $data['count']  = Tracker::getList (['onlyCount' => true]);
        */

        return $this->render('index', $data);
    }


    /**
     * Новый пользователь
     */
    public function actionNew () {
        $data = [];

        /*$data['list']   = Tracker::getList (['fetchNewKeys' => [
            Tracker::Id     => 'id',
            Tracker::Name   => 'name',
        ]]);
        $data['count']  = Tracker::getList (['onlyCount' => true]);
        */

        return $this->render('new', $data);
    }


}