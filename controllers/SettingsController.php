<?php

namespace app\controllers;

use Yii;
use yii\web\AccessControl;
use yii\web\Controller;
use yii\web\VerbFilter;

use app\models\User;
use app\models\user\UserAuth;
use app\models\user\UserToken;

use app\models\Settings;

use app\helpers\Url;

class SettingsController extends Base
{

    /**
     * Главная страница
     */
    public function actionIndex()
    {
        $settings_model = $this->getModel ();

        $this->sitebarModule    = "menuadminright";
        $this->menuAdminRight   = "settings";

        $data = [];
        $data['model'] = $settings_model;
        return $this->render('index', $data);
    }


    /**
     * Список проектов
     */
    public function actionEdit ()
    {
        $settings_model = $this->getModel ();

        // если форма сабмичена - проверяем на валидность и обновляем
        if (isset($_POST["settings"]) ) {
            $settings_model->load($_POST["settings"], '');

            // поля с которыми идет работа
            $attributes = Settings::$allKeys[$settings_model->tab];

            // проверяем на валидность
            $settings_model->validate($attributes);

            // восстаналиваем default'ные значения для полей с ошибками
            $settings_model->restoreAttributes4Errors ();

            // обновляем данные
            foreach ($attributes as $attribute) {
                $settings_model->saveItem ($attribute, $settings_model->$attribute);
            }

            // уведомление об успешном сохранении
            $this->setTextNotify("Successful update.");
        }

        $this->redirect (Url::settings('', $settings_model->tab));
    }


    /**
     * Получаем активную вкладку
     *
     * @param link
     */
    protected function initTab (&$settings_model) {
        $settings_model->load($_GET, '');
        $result = $settings_model->validate(["tab"]);
        if (!$result || !$settings_model->tab) {
            $settings_model->tab = "general";
        }
    }


    /**
     * Получаем подготовленную модель
     */
    protected function getModel () {
        $settings_model = Settings::object();

        // инициализируем атриубы - значения
        $settings_model->initAttributes ();

        // инициализируем вкладку
        $this->initTab ($settings_model);

        return $settings_model;
    }



}