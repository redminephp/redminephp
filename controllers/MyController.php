<?php

namespace app\controllers;

use Yii;
use yii\web\AccessControl;
use yii\web\Controller;
use yii\web\VerbFilter;

use app\models\User;
use app\models\user\UserAuth;
use app\models\user\UserToken;

use app\helpers\Url;

class MyController extends Base
{


    public function actionIndex()
    {
        return $this->render('index');
    }



}