<?php

namespace app\controllers;

use app\models\Project;
use Yii;
use yii\web\AccessControl;
use yii\web\Controller;
use yii\web\VerbFilter;

use app\models\User;
use app\models\user\UserAuth;
use app\models\user\UserToken;

use app\helpers\Url;

class ProjectsController extends Base
{


    /**
     * Список проектов
     */
    public function actionIndex()
    {
        $this->sitebarModule    = "projectsfilter";

        $data = [];

        $data['list'] = Project::getList (['fetchCascade' => true]);

        return $this->render('index', $data);
    }


    /**
     * Новый проект
     */
    public function actionNew () {
        $data = [];

        $project = Project::object();

        if (isset($_POST['project']) ) {

            // загружаем post-данные
            $project->load($_POST['project'], '');

            // проверяем на валидность
            if ($project->validate(['name', 'description', 'identifier', 'homepage', 'is_public', 'parent_id', 'inherit_members', 'enabled_module_names', 'tracker_ids']) ) {

                // если идет создание проекта
                if (!$project->id) {
                    $this->setTextNotify('Successful creation.');
                }

                // добавляем проект
                $project->save ();

                // обычное добавление
                if (isset($_POST['commit']) ) {
                    $this->redirect(Url::projectsSettings($project->identifier));
                }

                // create & continue
                else {
                    $this->redirect(Url::projectsNew());
                }
            }
        }

        $data['model'] = $project;

        return $this->render('new', $data);
    }


    /**
     * Удаление
     *
     * @param int
     */
    public function actionHandler ($projectHuu) {

        // по методу выполняем действия
        $method = \Yii::$app->request->getMethod();
        switch ($method) {

            // удаление
            case 'DELETE':
                Project::deleteAll ([Project::Identifier => $projectHuu]);
                break;
        }

        $this->redirect(Url::admin('projects'));
    }


    /**
     * Просмотр проекта
     */
    public function actionView ($projectHuu = '') {

        $this->menuHeader  = "overview";

        $data = [];

        $project = Project::getByIdentifier($projectHuu);

        $data['project'] = $project;

        return $this->render('view', $data);
    }


    /**
     * Настройки проекта
     */
    public function actionSettings ($projectHuu = '', $tab = 'info') {

        $this->menuHeader  = "settings";

        /*if (!$projectHuu || $projectHuu == 'new' || !$project->getByIdentifier($projectHuu) ) {
            $this->errorNotFound ();
        }*/

        $project = Project::getByIdentifier($projectHuu);

        $data = [];

        $data['project'] = $project;
        $project->tab = $tab;

        return $this->render('settings', $data);
    }


}