<?php

namespace app\controllers;

use yii\web\Controller;
use yii\web\Session;

use app\helpers\Notify;

class Base extends Controller
{

    // right block key
    public $sitebarModule       = "";

    // admin menu right active key
    public $menuAdminRight   = "";

    // menu header
    public $menuHeader = [];

    // уведомления
    public $notifications = [];



    /**
     * Инициализация
     */
    public function init ()
    {
        $this->setNotifications (true);
    }


    /**
     * Добавляем ошибку в уведомления
     *
     * @param string|array
     */
    public function setErrorNotify ($text) {
        Notify::set ('error', $text);
        $this->setNotifications ();
    }


    /**
     * Добавляем текст в уведомления
     *
     * @param string|array
     */
    function setTextNotify ($text) {
        Notify::set ('text', $text);
        $this->setNotifications ();
    }


    /**
     * Установка списка уведомлений
     */
    protected function setNotifications ($clear = false) {
        $this->notifications = Notify::getList($clear);
    }


    /**
     * Ошибка не найдено
     */
    protected function errorNotFound () {
        throw new \yii\web\NotFoundHttpException ('The page you were trying to access doesn&#x27;t exist or has been removed.', 404);
    }


    /**
     * Получаем модель
     *
     * @return array|object
     */
    protected function getModel ($itemId) {

        // устанавливается только у child-классов, соответственно exception ловится на автомате
        $modelClass = $this->modelClass;

        // инициализация объекта
        if ($itemId !== 'new') {
            $model = $modelClass::getById ($itemId);
            if (!$model) {
                $this->errorNotFound ();
            }
        }

        // дефолтная инициализация
        else {
            $model = $modelClass::object();

            // декодируем атрибуты
            $model->decodeAttributes ();
        }

        return $model;
    }

}