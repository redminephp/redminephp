<?php

namespace app\widgets;


/**
 * Class ActiveForm
 * @package yii\widgets
 */
class ActiveField extends \yii\widgets\ActiveField
{
    // html для обязательного поля (звездочка и т.д.)
    public $requiredHtml = '<span class="required"> *</span>';

    // обязательное поле
    public $isRequired = false;


    /**
     * Установка обязательности поля
     */
    public function required ($html = "") {
        $this->isRequired = true;
        if ($html) {
            $this->requiredHtml = $html;
        }
        $this->labelOptions['requiredHtml'] = $this->requiredHtml;
        return $this;
    }

}