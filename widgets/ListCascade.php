<?php


namespace app\widgets;

use app\helpers\Path;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class ListCascade extends ListButtons
{

    // ф-ции генерации url
    public $urlEdit;
    public $urlView;
    public $urlCopy;

    // отрисовка элемента
    public $fnRenderItem = false;

    // шаблон элемента
    public $itemTemplate = '<td class="name"><span><a href="{url}">{name}</a></span></td>
        <td class="buttons">{buttons}</td>';

    // шаблон ряда
    public $rowTemplate    = '<tr class="{class} {oddevenClass} {cascadeClass}">{content}</tr>';


    /**
     * Отрисовываем body
     *
     * @return string
     */
    protected function renderBody () {
        $result = ""; $index = 0;
        foreach ($this->items as $key => $item) {
            $result .= $this->renderRowCascade($index, $item);
        }
        return $result;
    }


    /**
     * Отрисовка ряда
     *
     * @param $item
     * @return string
     */
    protected function renderRowCascade (&$index, $item, $level = 0) {

        $result = $this->renderRow ($index, $item);

        // cascade class
        $cascadeClass = '';
        if ($level > 0) {
            $cascadeClass = 'idnt idnt-'.$level;
        }
        $result = strtr($result, ['{cascadeClass}' => $cascadeClass]);

        if (isset($item['subItems']) ) {
            foreach ($item['subItems'] as $subitem) {
                $result .= $this->renderRowCascade($index, $subitem, $level+1);
            }
        }

        return $result;
    }


    /**
     * Отрисовка элемента
     *
     * @param unknown
     * @return string
     */
    protected function renderItem ($item) {
        $template   = $this->itemTemplate;
        $url        = $this->fn ('urlEdit', [$item]);

        // если есть пользовательская отрисовка элемента
        if ($this->fnRenderItem) {
            $template = $this->fn ('fnRenderItem', [$this, $item, $template]);
        }

        // дефолтные элементы
        else {
            $template = strtr($template, [
                '{url}'         => $url,
                '{name}'        => $item['name'],
            ]);
        }

        return strtr($template, [
            '{buttons}'     => $this->renderButtons ($item),
        ]);
    }


}
