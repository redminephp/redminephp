<?php


namespace app\widgets;

use yii\helpers\Html;
use app\helpers\ArrayHelper;


/**
 * Class FieldCascadeSelect
 * @package app\widgets
 */
class FieldCascadeSelect extends Base
{

    // All cascade items
    public $cascadeItems;

    // Selected items (will be disabled)
    public $items;

    // Id & name attributes
    public $id;
    public $name;

    // Field keys
    public $fieldId;
    public $fieldParentId;
    public $fieldLabel;

    // Render callback functions
    public $renderItem;
    public $renderItems;

    // Delimiter & prefix for cascade displaing
    public $iterationDelimiter      = '» ';
    public $paddingIterationPrefix  = '&nbsp;&nbsp;';

    // Empty item
    public $emptyItem = '--- Please select ---';



    /**
     * Init
     */
    public function init () {

        // Render item
        $this->renderItem = function ($object, $item, $params) {
            $paddingPrefix  = ArrayHelper::remove($params, 'paddingPrefix', '');
            $options        = ArrayHelper::getValue($params, 'itemOptions', []);

            // Set options value
            $options['value'] = $item[$this->fieldId];

            // Check for disabling item
            if (in_array($options['value'], $this->items) ) {
                $options['disabled'] = true;
            }

            return Html::tag('option', $paddingPrefix.$item[$this->fieldLabel], $options);
        };


        // Render items
        $this->renderItems = function ($object, $items, $params = ['class' => 'root', 'padding' => 0]) {

            $padding = ArrayHelper::remove($params, 'padding', 0);

            // Init padding prefix
            $paddingPrefix = '';
            for ($i=0; $i<$padding; $i++) {
                $paddingPrefix .= $this->paddingIterationPrefix;
            }
            if ($paddingPrefix) {
                $paddingPrefix = $paddingPrefix . $this->iterationDelimiter;
            }
            $params['paddingPrefix'] = $paddingPrefix;


            // Render all items
            $content = '';
            foreach ($items as $itemId => $item) {
                $content .= $object->fn ('renderItem', [$object, $item, $params]);
                if (isset($item['subItems']) ) {
                    $params['padding'] = $padding+1;
                    $content .= $object->fn('renderItems', [$object, $item['subItems'], $params]);
                }
            }
            return $content;
        };

    }

    /**
     * Ends recording a block.
     * This method stops output buffering and saves the rendering result as a named block in the view.
     */
    public function run() {
        echo Html::tag('select',
            ($this->emptyItem ? Html::tag('option', $this->emptyItem, ['value' => '']) : '').
            $this->fn ('renderItems', [$this, $this->cascadeItems]),
            ['id' => $this->id, 'name' => $this->name]
        );
    }




}