<?php


namespace app\widgets;


/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class Breadcrumbs extends Base
{

    // элементы
    public $items;

    // шаблон шапки
    public $mainTemplate = '<h2>{content}</h2>';

    // шаблоны элемента
    public $itemTemplateLink    = '<a href="{url}">{title}</a>';
    public $itemTemplateText    = '{title}';
    public $separatorTemplate   = ' » ';


    /**
     * Главный метод
     *
     * @return string
     */
    public function run() {
        $content = [];
        foreach ($this->items as $item) {
            $content[] = $this->renderItem ($item);
        }
        echo strtr($this->mainTemplate, [
            '{content}' => implode($this->separatorTemplate, $content),
        ]);
    }


    /**
     * Отрисовка элемента
     *
     * @param array
     * @return string
     */
    public function renderItem ($item, $is_last = false) {
        if (isset($item['url']) ) {
            return strtr($this->itemTemplateLink, [
                '{url}'     => $item['url'],
                '{title}'   => $item['title'],
            ]);
        }
        else {
            return strtr($this->itemTemplateText, [
                '{title}'   => $item['title'],
            ]);
        }
    }
}
