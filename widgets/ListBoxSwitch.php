<?php

namespace app\widgets;

use app\helpers\Url;
use yii\helpers\Html;


/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class ListBoxSwitch extends Base
{

    // все элементы
    public $allItems = [];

    // элементы
    public $activeKeys = [];

    // имя поля
    public $fieldName   = '';

    // модель
    public $model;

    // атрибут
    public $attribute;

    // форма
    public $form;


    // шаблон шапки
    public $mainTemplate = '<fieldset class="box">
        <legend>{title}</legend>
        <table class="query-columns">
            <tr>
                <td style="padding-left:0">
                    <label for="available_columns">Available Columns</label><br />
                    {all-items}
                </td>
                <td class="buttons">{buttons-switch}</td>
                <td>
                    <label for="selected_columns">Selected Columns</label><br />
                    {active-items}
                </td>
                <td class="buttons">{buttons-order}</td>
            </tr>
        </table>
    </fieldset>';

    // шаблон кнопок переключения элементов
    public $templateButtonsSwitch = '<input type="button" value="&#8594;" onclick="moveOptions(this.form.available_columns, this.form.selected_columns);" /><br /><input type="button" value="&#8592;" onclick="moveOptions(this.form.selected_columns, this.form.available_columns);" />';

    // шаблон кнопок сортировки элементов
    public $templateButtonsOrder = '<input type="button" value="&#8593;" onclick="moveOptionUp(this.form.selected_columns);" /><br /><input type="button" value="&#8595;" onclick="moveOptionDown(this.form.selected_columns);" />';

    // св-ва поля
    public $fieldOptions = [
        'multiple'      => 'multiple',
        'ondblclick'    => 'moveOptions(this.form.available_columns, this.form.selected_columns);',
        'size'          => 10,
        'style'         => 'width:150px',
    ];


    /**
     * Ends recording a block.
     * This method stops output buffering and saves the rendering result as a named block in the view.
     */
    public function run() {

        // формируем массив активных элементов
        $activeItems = [];
        foreach ($this->activeKeys as $key) {
            if (!isset($this->allItems[$key]) ) continue;
            $activeItems[$key] = $this->allItems[$key];
            unset ($this->allItems[$key]);
        }

        // html поля
        $fieldHtml = $this->form->field($this->model, $this->attribute)
            ->listBox($activeItems, array_merge($this->fieldOptions, [
                'name'  => $this->fieldName,
            ]))->render(
                $this->renderField($activeItems, 'selected_columns', $this->fieldName)
            );

        // рендерим
        return strtr($this->mainTemplate, [
            '{title}'           => $this->model->getAttributeLabel($this->attribute),
            '{buttons-switch}'  => $this->templateButtonsSwitch,
            '{buttons-order}'   => $this->templateButtonsOrder,
            '{all-items}'       => $this->renderField($this->allItems, 'available_columns', 'available_columns[]'),
            '{active-items}'    => $fieldHtml,
        ]);
    }


    /**
     * Отрисовывам
     *
     * @param array
     * @param string
     * @param string
     * @return string
     */
    public function renderField ($items, $id, $name) {

        // св-ва поля
        $options = $this->fieldOptions;
        $options['id']      = $id;
        $options['name']    = $name;

        return Html::dropDownList($name, null, $items, $options);
    }

}