<?php


namespace app\widgets;

use yii\helpers\Html;


/**
 * Class FieldCascade
 * @package app\widgets
 */
class FieldCascade extends Base
{

    // весь список каскадных элементов
    public $cascadeItems;

    // активные элементы
    public $items;

    // имя
    public $name;

    // данные по ключам
    public $fieldId;
    public $fieldParentId;
    public $fieldLabel;

    // ф-ции отрисовки
    public $renderItem;
    public $renderItems;

    /**
     * Ends recording a block.
     * This method stops output buffering and saves the rendering result as a named block in the view.
     */
    public function run() {
        echo $this->fn ('renderItems', [$this, $this->cascadeItems]);
    }

}
