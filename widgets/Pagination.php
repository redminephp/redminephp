<?php


namespace app\widgets;


use yii\helpers\Html;


/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class Pagination extends Base
{
    // все количество элементов
    public $totalCount;

    // кол-во на странице
    public $perPage;

    // текущая страница
    public $currentPage;

    // базовый url
    public $url;

    // шаблон шапки
    public $mainTemplate = '<p class="pagination">{previous} {pages} {next} {counts}</p>';

    // шаблона кол-ва
    public $countTemplates = '<span class="items">({countFrom}-{countTo}/{countAll})</span>';

    // callback-ф-ция отрисовки ссылки
    public $fnRenderItemLink;

    // шаблоны элемента
    public $itemCurrentTemplate = '<span class="current page">{page}</span>';

    // кол-во страниц
    protected $pageCount;


    /**
     * Главный метод
     *
     * @return string
     */
    public function run() {
        $this->pageCount = ceil ($this->totalCount/$this->perPage);
        return strtr($this->mainTemplate, [
            '{previous}'    => $this->renderPreviousLink(),
            '{pages}'       => $this->renderItems(),
            '{next}'        => $this->renderNextLink(),
            '{counts}'      => $this->renderCounts(),
        ]);
    }


    /**
     * Отрисовка предыдущей ссылки
     *
     * @return string
     */
    public function renderPreviousLink () {
        $template = '';
        if ($this->currentPage > 1) {
            $page = $this->currentPage-1;
            return $this->renderItemLink ($page, $this->getUrl($page), '« Previous', ['class' => 'previous']);
        }
        return '';
    }


    /**
     * Отрисовка следующей ссылки
     *
     * @return string
     */
    public function renderNextLink () {
        $template = '';
        if ($this->pageCount > 1 && $this->currentPage < $this->pageCount) {
            $page = $this->currentPage+1;
            return $this->renderItemLink ($page, $this->getUrl($page), 'Next »', ['class' => 'next']);
        }
        return '';
    }


    /**
     * Отрисовка кол-ва
     *
     * @return string
     */
    public function renderCounts () {
        $countTo = $this->currentPage*$this->perPage;
        if ($countTo > $this->totalCount) {
            $countTo = $this->totalCount;
        }
        return strtr($this->countTemplates, [
            '{countFrom}'   => ($this->currentPage-1)*$this->perPage+1,
            '{countTo}'     => $countTo,
            '{countAll}'    => $this->totalCount,
        ]);
    }


    /**
     * Отрисовка элементов
     *
     * @return string
     */
    public function renderItems () {
        if ($this->pageCount <= 1) {
            return '';
        }
        $content = [];
        for ($i=1; $i<=$this->pageCount; $i++) {
            $content[] = $this->renderItem($i);
        }
        return implode(' ', $content);
    }


    /**
     * Отрисовка элемента
     *
     * @param array
     * @return string
     */
    public function renderItem ($page) {
        if ($this->currentPage == $page) {
            return strtr($this->itemCurrentTemplate, [
                '{page}' => $page,
            ]);
        }
        else {
            return $this->renderItemLink ($page, $this->getUrl($page), $page, ['class' => 'page']);
        }
    }


    /**
     * Отрисовка ссылки
     *
     * @param $page
     * @param $url
     * @param $title
     * @param array $options
     * @return mixed
     */
    public function renderItemLink ($page, $url, $title, $options = []) {

        // если есть пользовательское переопределение
        if ($this->fnRenderItemLink) {
            return $this->fn ('fnRenderItemLink', [$page, $url, $title, $options]);
        }

        return Html::a ($title, $url, $options);
    }


    /**
     * Получаем ссылку
     *
     * @param int
     * @return string
     */
    protected function getUrl ($page) {
        return $this->url."?page=$page";
    }
}
