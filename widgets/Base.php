<?php


namespace app\widgets;

use app\helpers\Path;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class Base extends \yii\base\Widget
{


    /**
     * Вызов ф-ции из параметров
     *
     * @param $attribute
     * @param $params
     * @return mixed
     */
    public function fn ($attribute, $params) {
        if (!$this->$attribute) {
            return "";
        }
        $fn = $this->$attribute;
        return call_user_func_array ($fn, $params);
    }


}
