<?php

namespace app\widgets;


/**
 * Class ActiveForm
 * @package yii\widgets
 */
class ActiveForm extends \yii\widgets\ActiveForm
{


    /**
     * Begin
     *
     * @param array $params
     * @return static
     */
    public static function begin ($params = []) {
        $base_params = [
            'fieldConfig' => [
                'template' =>  '{label}{input}{hint}',
                'options' => [
                    'tag' => 'p',
                ],
                'class' => ActiveField::className()
            ],
        ];
        $params = array_merge($params, $base_params);
        return parent::begin($params);
    }

}