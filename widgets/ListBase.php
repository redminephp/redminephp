<?php


namespace app\widgets;

use app\helpers\Url;
use app\helpers\Path;


/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class ListBase extends Base
{

    // элементы
    public $items;

    // заголовки
    public $columnList;

    // css класс для ряда
    public $rowClass = "";

    // шаблон шапки
    public $mainTemplate = '<table class="list">
        <thead><tr>
            {header}
        </tr></thead>
        <tbody>
            {body}
        </tbody>
    </table>';

    // шаблон заголовка
    public $titleTemplate   = '<th>{title}</th>';

    // шаблон ряда
    public $rowTemplate    = '<tr class="{class} {oddevenClass}">{content}</tr>';

    // отрисовка элемента
    public $fnRenderRow = false;

    // шаблон элемента
    public $itemTemplate = '<td>{content}</td>';

    // отрисовка элемента
    public $fnRenderItem = false;

    // шаблон для CheckTemplate
    public $toggleCheckTemplate     = '<img alt="Toggle_check" src="{image}" />';


    /**
     * Ends recording a block.
     * This method stops output buffering and saves the rendering result as a named block in the view.
     */
    public function run() {
        echo strtr($this->mainTemplate, [
            '{header}'  => $this->renderHeader(),
            '{body}'    => $this->renderBody(),
        ]);
    }


    /**
     * Отрисовываем header
     *
     * @return string
     */
    protected function renderHeader () {
        $result = "";
        foreach ($this->columnList as $item) {
            $result .= strtr($this->titleTemplate, [
                '{title}' => $item['title'],
            ]);
        }
        return $result;
    }


    /**
     * Отрисовываем body
     *
     * @return string
     */
    protected function renderBody () {
        $result = ""; $index = 0;
        foreach ($this->items as $key => $item) {
            $result .= $this->renderRow($index, $item);
        }
        return $result;
    }


    /**
     * Отрисовка ряда
     *
     * @param $item
     * @return string
     */
    protected function renderRow (&$index, $item) {

        $template = $this->rowTemplate;

        // если есть пользовательская отрисовка элемента
        if ($this->fnRenderRow) {
            $template = $this->fn ('fnRenderRow', [$this, $item, $template]);
        }

        return strtr($template, [
            '{class}'           => $this->rowClass,
            '{oddevenClass}'    => (++$index%2) ? 'odd' : 'even',
            '{content}'         => $this->renderItem ($item),
        ]);
    }


    /**
     * Отрисовка элемента
     *
     * @param unknown
     * @return string
     */
    protected function renderItem ($item) {

        // если есть пользовательская отрисовка элемента
        if ($this->fnRenderItem) {
            return $this->fn ('fnRenderItem', [$this, $item, $this->itemTemplate]);
        }

        // генерируем контент
        $content = '';
        foreach ($this->columnList as $columnInfo) {
            switch ($columnInfo['fieldType']) {
                case 'value':
                    $content .= strtr($this->itemTemplate, [
                        '{content}' => $item[$columnInfo['fieldId']],
                    ]);
                    break;
            }
        }

        return $item;
    }


    /**
     * Отрисовываем ToggleCheck
     *
     * @param bool
     * @return string
     */
    public function renderToggleCheck ($check) {
        if (!$check) {
            return '';
        }
        return strtr($this->toggleCheckTemplate, [
            '{image}' => Path::base('/images/toggle_check.png'),
        ]);
    }

}
