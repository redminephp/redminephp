<?php


namespace app\widgets;

use app\helpers\Url;
use app\helpers\Path;


/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class ListButtons extends ListBase
{
    public $dataRemote = false;

    // кнопки
    public $buttonsTemplate = '{buttonCopy}
            {buttonDelete}';


    // шаблоны кнопок
    public $buttonCopyTemplate      = '<a href="{urlCopy}" {dataRemote} class="icon icon-copy">Copy</a>';
    public $buttonDeleteTemplate    = '<a href="{urlView}" {dataRemote} class="icon icon-del" data-remote="true" data-confirm="Are you sure?" data-method="delete" rel="nofollow">Delete</a>';

    // кнопки архивирования/разархивирования
    public $buttonArchiveTemplate   = '<a class="icon icon-lock" rel="nofollow" {dataRemote} data-method="post" data-confirm="Are you sure?" href="{urlArchive}">Archive</a>';
    public $buttonUnArchiveTemplate = '<a class="icon icon-unlock" rel="nofollow" {dataRemote} data-method="post" href="{urlUnArchive}">Unarchive</a>';

    // ф-ция проверки архивности элемента
    public $fnIsArchive = false;


    /**
     * @return bool
     */
    protected function isPermanent () {
        return false;
    }


    /**
     * Проверка флага архивности элемента
     *
     * @param array
     * @return bool
     */
    public function isArchive ($item) {
        if ($this->fnIsArchive) {
            return $this->fn('fnIsArchive', [$item]);
        }
        if (!isset($item['is_archive']) ) {
            return false;
        }
        return $item['is_archive'];
    }


    /**
     * Отрисовка кнопок
     *
     * @param int
     * @return string
     */
    public function renderButtons ($item) {
        return strtr($this->buttonsTemplate, [
            '{buttonCopy}'          => $this->renderButtonCopy($item),
            '{buttonDelete}'        => $this->renderButtonDelete($item),
            '{buttonSwitchArchive}' => $this->renderButtonSwitchArchive($item),
        ]);
    }


    /**
     * Отрисовка кнопки
     *
     * @param string
     * @return string
     */
    public function renderButton ($item) {
        return strtr($item, [
            '{dataRemote}' => $this->dataRemote ? 'data-remote="true"' : '',
        ]);
    }


    /**
     * Отрисовка кнопки копирования
     *
     * @param $itemId
     * @return string
     */
    protected function renderButtonCopy ($item) {
        $result = strtr($this->buttonCopyTemplate, [
            '{urlCopy}' => $this->fn ('urlCopy', [$item]),
        ]);
        return $this->renderButton($result);
    }


    /**
     * Отрисовка кнопки удаления
     *
     * @param $itemId
     * @return string
     */
    protected function renderButtonDelete ($item) {
        if ($this->isPermanent($item['id']) ) return "";

        $result = strtr($this->buttonDeleteTemplate, [
            '{urlView}' => $this->fn ('urlView', [$item]),
        ]);
        return $this->renderButton($result);
    }


    /**
     * Отрисовка кнопки Archive/UnArchive
     *
     * @param array
     * @return string
     */
    protected function renderButtonSwitchArchive ($item) {
        $template = $this->buttonArchiveTemplate;
        if ($this->isArchive($item) ) {
            $template = $this->buttonUnArchiveTemplate;
        }
        $result = strtr($template, [
           '{urlArchive}'   => '',
           '{urlUnArchive}' => '',
        ]);
        return $this->renderButton($result);
    }

}
