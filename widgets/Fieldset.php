<?php


namespace app\widgets;

use yii\helpers\Html;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class Fieldset extends Base
{

    // заголовок
    public $title;

    // контент
    public $content = "";

    // опции
    public $options = [];

    // шаблон контента
    public $contentTemplate = '<legend>{title}</legend>
        {content}';


    /**
     * Ends recording a block.
     * This method stops output buffering and saves the rendering result as a named block in the view.
     */
    public function run() {
        echo Html::tag ('fieldset', strtr($this->contentTemplate, [
            '{title}'   => $this->title,
            '{content}' => $this->content,
        ]), $this->options);
    }

}
