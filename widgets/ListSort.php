<?php


namespace app\widgets;

use app\helpers\Path;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class ListSort extends ListButtons
{

    // ф-ции генерации url
    public $urlEdit;
    public $urlView;
    public $urlCopy;

    // тип
    public $type;

    //постоянные элементы (без сортировки и удаления)
    public $permanentItems = [];

    // шаблон элемента
    public $itemTemplate = '<td class="name"><span><a href="{url}">{name}</a></span></td>
        <td class="reorder">{sortItems}</td>
        <td class="buttons">{buttons}</td>';

    // шаблон элемента
    public $permanentItemTemplate = false;

    // элементы сортировки
    public $sortItemsTemplate = '{linkTop}{linkUp}{linkDown}{linkBottom}';

    // элемент сортировки
    public $sortItemTemplate = '<a href="{url}?{type}[move_to]={moveTo}" data-method="put" rel="nofollow" title="{title}"><img alt="{title}" src="{image}"></a>';



    /**
     * Ends recording a block.
     * This method stops output buffering and saves the rendering result as a named block in the view.
     */
    public function run() {
        if (!$this->permanentItemTemplate) {
            $this->permanentItemTemplate = $this->itemTemplate;
        }
        echo parent::run();
    }


    /**
     * Проверка элемента на постоянный
     *
     * @param int
     * @return bool
     */
    protected function isPermanent ($itemId) {
        return in_array($itemId, $this->permanentItems);
    }


    /**
     * Отрисовка элемента
     *
     * @param unknown
     * @return string
     */
    protected function renderItem ($item) {
        $template   = !$this->isPermanent($item['id']) ? $this->itemTemplate : $this->permanentItemTemplate;
        $url        = $this->fn ('urlEdit', [$item]);

        // если есть пользовательская отрисовка элемента
        if ($this->fnRenderItem) {
            $template = $this->fn ('fnRenderItem', [$this, $item, $template]);
        }

        // дефолтные элементы
        else {
            $template = strtr($template, [
                '{url}'         => $url,
                '{name}'        => $item['name'],
            ]);
        }

        return strtr($template, [
            '{sortItems}'   => $this->renderSortItems ($item),
            '{buttons}'     => $this->renderButtons ($item),
        ]);
    }



    /**
     * Отрисовка элементов сортировки
     *
     * @param $itemId
     * @return string
     */
    protected function renderSortItems ($item) {
        if ($this->isPermanent($item['id']) ) return "";

        $url = $this->fn ('urlView', [$item]);
        return strtr ($this->sortItemsTemplate, [
            '{linkTop}'     => $this->renderSortItem($url, 'highest', 'Move to top', Path::base('/images/2uparrow.png')),
            '{linkUp}'      => $this->renderSortItem($url, 'higher', 'Move up', Path::base('/images/1uparrow.png')),
            '{linkDown}'    => $this->renderSortItem($url, 'lower', 'Move down', Path::base('/images/1downarrow.png')),
            '{linkBottom}'  => $this->renderSortItem($url, 'lowest', 'Move to bottom', Path::base('/images/2downarrow.png')),
        ]);
    }


    /**
     * Отрисовка элемента сортировки
     *
     * @param $url
     * @param $moveTo
     * @param $title
     * @param $image
     * @return string
     */
    protected function renderSortItem ($url, $moveTo, $title, $image) {
        return strtr ($this->sortItemTemplate, [
            '{url}'     => $url,
            '{type}'    => $this->type,
            '{moveTo}'  => $moveTo,
            '{title}'   => $title,
            '{image}'   => $image,
        ]);
    }

}
