<?php


namespace app\widgets;


/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class Tab extends Base
{

    // элементы
    public $items;

    // активный ключ
    public $activeKey = "";

    // параметры формирования url
    public $urlParams = false;

    // объект app/helpers/Content
    public $contentTabs = false;

    // шаблон шапки
    public $mainTemplate = '<div class="tabs"><ul>{tabs}</ul><div class="tabs-buttons" style="display:none;">
        <button class="tab-left" onclick="moveTabLeft(this);"></button>
        <button class="tab-right" onclick="moveTabRight(this);"></button>
    </div></div>{script}{allcontent}';

    // шаблон заголовка
    public $labelTemplate = '<li><a id="tab-{key}" onclick="showTab(\'{key}\', this.href); this.blur(); return false;" href="{url}">{label}</a></li>';

    // шаблон активного заголовка
    public $labelTemplateActive = '<li><a id="tab-{key}" class="selected" onclick="showTab(\'{key}\', this.href); this.blur(); return false;" href="{url}">{label}</a></li>';

    // шаблон для конента
    public $contentTemplate = '<div id="tab-content-{key}" class="tab-content" style="display: none;">{content}</div>';

    // шаблон для активного конента
    public $contentTemplateActive = '<div id="tab-content-{key}" class="tab-content">{content}</div>';

    // скрипт
    public $scriptSource = '<script>
        $(document).ready(displayTabsButtons);
        $(window).resize(displayTabsButtons);
    </script>';

    /**
     * Ends recording a block.
     * This method stops output buffering and saves the rendering result as a named block in the view.
     */
    public function run() {
        echo $this->renderAll($this->items);
    }


    /**
     * Отрисовывам все
     *
     * @param array
     * @return string
     */
    public function renderAll ($items) {

        // отрисовываем
        $tabsHtml = ""; $contentHtml = "";
        foreach ($items as $item) {
            $tabsHtml       .= $this->renderItemLabel($item);
            $contentHtml    .= $this->renderItemContent($item);
        }

        return strtr($this->mainTemplate, [
            '{tabs}'        => $tabsHtml,
            '{script}'      => $this->scriptSource,
            '{allcontent}'  => $contentHtml,
        ]);
    }


    /**
     * Отрисовывам
     *
     * @param array
     * @return string
     */
    public function renderItemLabel ($item) {
        $isActive = $this->isActiveItem ($item);

        $labelTemplate      = $isActive ? $this->labelTemplateActive : $this->labelTemplate;

        // url
        $url = "";
        if ($this->urlParams) {
            list($fn, $params) = $this->urlParams;
            foreach ($params as $key => $value) {
                $params[$key] = str_replace('{key}', $item['key'], $value);
            }
            $url = call_user_func_array ("\\app\\helpers\\Url::$fn", $params);
        }

        // заголовок
        return strtr($labelTemplate, [
            '{url}'     => $url,
            '{key}'     => $item['key'],
            '{label}'   => $item['label'],
        ]);
    }


    /**
     * Отрисовывам
     *
     * @param array
     * @return string
     */
    public function renderItemContent ($item) {
        $isActive = $this->isActiveItem ($item);

        $contentTemplate    = $isActive ? $this->contentTemplateActive : $this->contentTemplate;

        // контент
        $content = '';
        if ($this->contentTabs) {
            $content = $this->contentTabs->get ($item['key']);
        }
        else if (isset($item['content']) ) {
            $content = $item['content'];
        }

        // контент
        return strtr($contentTemplate, [
            '{key}'     => $item['key'],
            '{content}' => $content,
        ]);
    }


    /**
     * Проверяем пункт на активность
     *
     * @param array
     * @return bool
     */
    public function isActiveItem ($item) {
        return ($item["key"] == $this->activeKey);
    }

}
