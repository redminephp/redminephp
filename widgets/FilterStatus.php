<?php


namespace app\widgets;

use yii\helpers\Html;


/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class FilterStatus extends Base
{
    // title
    public $title = '';

    // статусы
    public $statusItems = [];

    // активный статус
    public $activeItem = '';

    // ключевое слово
    public $keyword = '';

    // базовый url
    public $baseUrl = '';

    // главный шаблон
    public $mainTemplate = '<label for="status">Status :</label>
        {statusItems}
        <label for="name">{title}:</label>
        <input type="text" size="30" name="name" id="name" value="{keyword}" />
        <input type="submit" value="Apply" class="small" />
        <a class="icon icon-reload" href="{baseUrl}">Clear</a>';

    /**
     * Ends recording a block.
     * This method stops output buffering and saves the rendering result as a named block in the view.
     */
    public function run() {
        echo Html::tag ('form', Fieldset::widget([
            'title'     => 'Filters',
            'content'   => $this->renderAll (),
        ]), [
            'method'            => 'get',
            'action'            => $this->baseUrl,
            'accept-charset'    => 'UTF-8',
        ]);
    }


    /**
     * Отрисовка всего контента
     *
     * @return string
     */
    protected function renderAll () {
        return strtr($this->mainTemplate, [
            '{baseUrl}'     => $this->baseUrl,
            '{title}'       => $this->title,
            '{keyword}'     => $this->keyword,
            '{statusItems}' => $this->renderStatusItems(),
        ]);
    }


    /**
     * Отрисовка статусов
     *
     * @return string
     */
    protected function renderStatusItems () {
        return Html::dropDownList ('status', $this->activeItem, $this->statusItems, [
            'id'        => 'status',
            'class'     => 'small',
            'onchange'  => 'this.form.submit(); return false;',
        ]);
    }
}
