<?php

namespace app\db;

use app\helpers\ArrayHelper;

/**
 * Class ActiveQuery
 * @package yii\db
 */
class ActiveQuery extends \yii\db\ActiveQuery
{
    // Fetch params
    protected static $fetchParams = ['fetchKeyValue', 'fetchValue', 'fetchCascade', 'fetchNewKeys'];

    // Decoded result
    protected $decoded = false;


    /**
     * Конструктор
     *
     * @param array $modelClass
     * @param array $config
     */
    public function __construct($modelClass, $config = [])
    {
        parent::__construct($modelClass, $config);

        if (property_exists($modelClass, 'isArrayable') ) {
            $this->asArray ($modelClass::$isArrayable);
        }
    }


    /**
     * Decode result
     *
     * @param bool
     * @return object ActiveQuery
     */
    public function decoded ($decoded = true) {
        $this->decoded = $decoded;
        return $this;
    }


    /**
     * Get one
     *
     * @return object
     */
    public function one ($db = NULL) {
        $result = parent::one($db);

        // Base get method
        if (!$this->decoded || !$result) {
            return $result;
        }

        return $this->decodeItem($result);
    }


    /**
     * Decode item
     *
     * @param $item
     * @return mixed
     */
    protected function decodeItem ($item) {

        // For object
        if (is_object($item) ) {
            $item->decodeAttributes ();
        }

        // For array
        else {
            $model = $this->modelClass;
            $model::object()->decodeValues4List ($item);
        }

        return $item;
    }


    /**
     * By params
     *
     * @param array
     * @return object app\db\ActiveQuery
     */
    public function byParams ($params) {
        $filter         = ArrayHelper::remove($params, 'filter', false);
        $onlyCount      = ArrayHelper::remove($params, 'onlyCount', false);
        $fields         = ArrayHelper::remove($params, 'fields', '*');
        $where          = ArrayHelper::remove($params, 'where', false);
        $orderBy	    = ArrayHelper::remove($params, 'orderBy', false);
        $page           = ArrayHelper::remove($params, 'page', false);
        $offset         = ArrayHelper::remove($params, 'offset', false);
        $limit          = ArrayHelper::remove($params, 'limit', false);

        // Remove fetch params
        foreach (static::$fetchParams as $fetchParam) {
            ArrayHelper::remove($params, $fetchParam);
        }

        // --- страница
        if ($page < 1) {
            $page = 1;
        }
        $page--;
        if ($page && !$offset) {
            $offset = $page*$limit;
        }
        // ---


        // поля
        $this->select($fields);

        // параметры where
        if ($params) {
            $this->andWhere ($params);
        }

        // условия
        if ($where) {
            foreach ($where as $_where) {
                $this->andWhere ($_where);
            }
        }

        // фильтр
        if ($filter) {
            foreach ($filter as $filterItem) {
                $this->andFilterWhere($filterItem);
            }
        }

        // если нужно получить только кол-во записей
        if ($onlyCount) {
            if ($onlyCount === true) {
                $onlyCount = '*';
            }
        }
        else {

            // если есть лимиты для выборки
            if ($limit) {
                $this->limit ($limit);
            }
            if ($offset) {
                $this->offset ($offset);
            }

            // сортировка
            if ($orderBy) {
                $this->orderBy ($orderBy);
            }
        }

        return $this;
    }


    /**
     * Fetch result
     *
     * @param array
     * @return array
     */
    public function fetch ($params = []) {

        // Get list
        $list = $this->asArray(true)->all();

        // Not fetch list
        if (!$params) {
            return $list;
        }

        // Fetch params
        $extractParams = [];
        foreach (static::$fetchParams as $fetchParam) {
            $extractParams[$fetchParam] = false;
            if (isset($params[$fetchParam]) ) {
                $extractParams[$fetchParam] = $params[$fetchParam];
            }
        }
        extract($extractParams);


        // если нужно изменить ключи массива
        if ($fetchNewKeys) {
            foreach ($list as $key => $item) {
                $result_item = [];
                foreach ($fetchNewKeys as $old_key => $new_key) {
                    $result_item[$new_key] = $item[$old_key];
                }
                $list[$key] = $result_item;
            }
        }

        // обработка массива
        if ($fetchKeyValue) {

            // Overwrite fetchKeyValue param
            if ($fetchKeyValue === true) {
                $modelClass = $this->modelClass;
                $fetchKeyValue = $modelClass::fetchFields ();
            }

            $key    = ArrayHelper::getValue($fetchKeyValue, 'key', false);
            $value  = ArrayHelper::getValue($fetchKeyValue, 'value', false);
            if ($key && $value) {
                $list = ArrayHelper::Array2KeyValueArray ($list, $key, $value);
            }
        }

        // если каскадная обработка
        if ($fetchCascade) {
            $fieldId        = ArrayHelper::getValue($fetchCascade, 'fieldId', false);
            $fieldParentId  = ArrayHelper::getValue($fetchCascade, 'fieldParentId', false);
            $baseParentId   = ArrayHelper::getValue($fetchCascade, 'parentId', false);

            if ($fieldId && $fieldParentId) {
                ArrayHelper::a_fetchSubitems($tmp_result, $list, $fieldId, $fieldParentId, $baseParentId);
                $list = $tmp_result;
            }
        }

        // если нужно получить только одно поле
        if ($fetchValue) {
            $list = ArrayHelper::ArrayValues ($list, $fetchValue);
        }

        return $list;
    }

}