<?php

namespace app\helpers;

use app\models\User;
use app\models\Role;
use app\models\Project;


/**
 * Class Block
 * !!@todo переделать все на виджеты widgets!!!
 * @package app\helpers
 */
class Block extends \yii\helpers\BaseUrl
{


    /**
     * Список проектов
     *
     * @param array
     */
    static function projectslist ($list, $config = []) {
        if (!$config) {
            $config = [
                'class_main'    => 'root',
                'class_inner'   => 'root',
            ];
        }
        $data = array (
            'list'      => $list,
            'config'    => $config,
        );
        return self::_view ('projectslist', $data);
    }


    /**
     * Фильтр по проектам
     */
    static function projectsfilter () {
        return self::_view ('projectsfilter');
    }


    /**
     * Вывод уведомлений
     *
     * @param array
     * @param string
     */
    static function flexnotifications ($list, $type = "list") {
        $require_keys = ['text', 'error'];
        foreach ($require_keys as $key) {
            if (!isset($list[$key]) ) {
                $list[$key] = [];
            }
        }
        $data = array (
            'list' => $list,
            'type' => $type,
        );
        return self::_view ('flexnotifications', $data);
    }

    /**
     * Получаем список для меню админки
     *
     * @return array
     */
    private static function menuadminlist () {
        return [
            "projects"              => ["title" => "Projects",              "url" => Url::admin('projects')],
            "users"                 => ["title" => "Users",                 "url" => Url::users()],
            "groups"                => ["title" => "Groups",                "url" => Url::groups()],
            "roles"                 => ["title" => "Roles and permissions", "url" => Url::roles()],
            "trackers"              => ["title" => "Trackers",              "url" => Url::trackers()],
            "issue_statuses"        => ["title" => "Issue statuses",        "url" => Url::issueStatus()],
            "workflows"             => ["title" => "Workflow",              "url" => "/workflows/edit"],
            "custom_fields"         => ["title" => "Custom fields",         "url" => "/custom_fields"],
            "enumerations"          => ["title" => "Enumerations",          "url" => "/enumerations"],
            "settings"              => ["title" => "Settings",              "url" => Url::settings()],
            "server_authentication" => ["title" => "LDAP authentication",   "url" => "/auth_sources"],
            "plugins"               => ["title" => "Plugins",               "url" => "/admin/plugins"],
            "info"                  => ["title" => "Information",           "url" => "/admin/info"],
        ];
    }


    /**
     * Главное меню админки
     *
     * @param string
     */
    static function menuadminmain ($active_key = false)
    {
        $data = array (
            "list" => self::menuadminlist(),
        );
        return self::_view ('menuadminmain', $data);
    }


    /**
     * Главное меню админки
     *
     * @param string
     */
    static function menuadminright ($active_key = false)
    {
        $data = array (
            "list"          => self::menuadminlist(),
            "active_key"    => \Yii::$app->controller->menuAdminRight,
        );
        return self::_view ('menuadminright', $data);
    }


    /**
     * Меню в header'e
     *
     * @param string
     */
    static function menuheader ($active_key) {
        $data = array (
            "list"          => [
                'overview'     => ['url' => '', 'title' => 'Overview'],
                'activity'     => ['url' => '', 'title' => 'Activity'],
                'issues'       => ['url' => '', 'title' => 'Issues'],
                'new-issue'    => ['url' => '', 'title' => 'New issue', 'accesskey' => 7],
                'gantt'        => ['url' => '', 'title' => 'Gantt'],
                'calendar'     => ['url' => '', 'title' => 'Calendar'],
                'news'         => ['url' => '', 'title' => 'News'],
                'documents'    => ['url' => '', 'title' => 'Documents'],
                'wiki'         => ['url' => '', 'title' => 'Wiki'],
                'files'        => ['url' => '', 'title' => 'Files'],
                'settings'     => ['url' => '', 'title' => 'Settings'],
            ],
            "active_key"    => $active_key
        );
        return self::_view ('menuheader', $data);
    }


    /**
     * Список пользователей группы для редактирования
     *
     * @param object
     */
    static function groupuserlistedit ($model) {
        $data = [
            'model' => $model,
        ];
        return self::_view ('groupuserlistedit', $data);
    }


    /**
     * Group memberships list edit block
     *
     * @param object
     */
    public static function groupmembershipslistedit ($model) {
        $data = [
            'model'     => $model,
            'roles'     => Role::getList(['fetchKeyValue' => true]),
            'projects'  => Project::getList(['fetchCascade' => true]),
        ];
        return self::_view ('groupmembershipslistedit', $data);
    }


    /**
     * Список пользователей для автозаполнения в группах
     *
     * @param object
     * @param string
     * @param int
     */
    static function groupsuserautocompletelist ($model, $q = '', $page = 1) {

        $limit = 100;

        $data = [];

        $data['model'] = $model;

        // запрос
        $query = User::find();
        if ($q) {
            $query->orFilterWhere(['like', User::FirstName, $q]);
            $query->orFilterWhere(['like', User::LastName, $q]);
        }
        $query->andWhere (['not in', User::Id, ArrayHelper::ArrayValues($model->users, 'id')]);

        // общее кол-во
        $data['total']  = User::getList (['onlyCount' => true, 'query' => $query]);

        // пользователи для выбора
        $data['list'] = User::getList([
            'query'     => $query,
            'fields'    => [User::Id, User::FirstName, User::LastName],
            'page'      => $page,
            'limit'     => $limit, // @todo вынести (скорее всего потом будет браться из settings)
        ]);

        $data['page']   = $page;
        $data['limit']  = $limit;

        return self::_view ('groupsuserautocompletelist', $data);
    }



    /**
     * Подключение шаблона
     *
     * @param string
     * @param array
     */
    private static function _view ($view, $data = array()) {
        return \Yii::$app->view->render('/block/'.$view, $data);
    }
}