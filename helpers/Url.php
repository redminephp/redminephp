<?php

namespace app\helpers;

/**
 * Class Url
 * @package app\helpers
 */
class Url extends \yii\helpers\Url
{

    /**
     * Генерируем url по сегментам
     *
     * @param string
     * @param array
     * @return string
     */
    public static function segments ($url, $segments) {
        if ($segments) {
            $segments = implode('/', $segments);
            $url .= '/'.$segments;
        }
        return static::toRoute($url);
    }


    /**
     * Login url
     *
     * @return string
     */
    public static function login ()
    {
        return static::toRoute('/login');
    }


    /**
     * Login url
     *
     * @return string
     */
    public static function logout ()
    {
        return static::toRoute('/logout');
    }




    // --- users


    /**
     * Users url
     *
     * @param array
     * @return string
     */
    public static function users ($segments = []) {
        return static::segments('/users', $segments);
    }


    /**
     * Просмотр пользователя
     *
     * @param int
     * @return string
     */
    public static function usersView ($userId) {
        return static::users ([$userId]);
    }


    /**
     * Создание нового пользователя
     *
     * @return string
     */
    public static function usersNew () {
        return static::users (['new']);
    }




    // --- groups


    /**
     * Группы
     *
     * @param array
     * @return string
     */
    public static function groups ($segments = []) {
        return static::segments('/groups', $segments);
    }


    /**
     * Новый проект
     *
     * @param int
     * @return string
     */
    public static function groupsHandler ($itemId) {
        return static::groups ([$itemId]);
    }


    /**
     * Редактирование
     *
     * @param int
     * @return string
     */
    public static function groupsEdit ($itemId, $tab = '') {
        $url = static::groups([$itemId, 'edit']);
        if ($tab) {
            $url .= "?tab=$tab";
        }
        return $url;
    }

    /**
     * Редактирование
     *
     * @return string
     */
    public static function groupsNew () {
        return static::groups (['new']);
    }


    /**
     * Просмотр группы
     *
     * @param int
     * @return string
     */
    public static function groupsView ($groupId) {
        return static::groups ([$groupId]);
    }


    /**
     * Пользователь группы
     *
     * @param int
     * @param int
     * @return string
     */
    public static function groupsUser ($groupId, $userId = '') {
        return static::groups ([$groupId, 'users', $userId]);
    }


    /**
     * Автозаполнение для пользователей
     *
     * @param $groupId
     * @return string
     */
    public static function groupsAutocompleteUser ($groupId) {
        return static::groups ([$groupId, 'autocomplete_for_user']);
    }


    /**
     * Редактирвание связей с групп с проектами
     *
     * @param int
     * @parma int
     * @return string
     */
    public static function groupsMembershipEdit ($groupId, $itemId = false) {
        $url = static::groups (['edit_membership', $groupId]);
        if ($itemId) {
            $url .= '?membership_id='.$itemId;
        }
        return $url;
    }


    /**
     * Удаление связей с групп с проектами
     *
     * @param int
     * @parma int
     * @return string
     */
    public static function groupsMembershipDestroy ($groupId, $itemId) {
        $url = static::groups (['destroy_membership', $groupId]);
        if ($itemId) {
            $url .= '?membership_id='.$itemId;
        }
        return $url;
    }




    // --- projects

    /**
     * Projects url
     *
     * @param string
     * @return string
     */
    public static function projects ($projectHuu = false)
    {
        $url = '/projects';
        if ($projectHuu) {
            $url .= '/'.$projectHuu;
        }
        return static::toRoute($url);
    }


    /**
     * Новый проект
     *
     * @return string
     */
    public static function projectsNew () {
        return static::projects ('new');
    }


    /**
     * Новый проект
     *
     * @return string
     */
    public static function projectsHandler ($projectHuu) {
        return static::projects ($projectHuu);
    }


    /**
     * Новый проект
     *
     * @return string
     */
    public static function projectsCopy ($projectHuu) {
        return static::projects ($projectHuu, 'copy');
    }


    /**
     * Новый проект
     *
     * @param string
     * @param string
     * @return string
     */
    public static function projectsSettings ($projectHuu, $tab = '') {
        $url = static::projects ($projectHuu)."/settings";
        if ($tab) {
            $url = $url .'/'.$tab;
        }
        return $url;
    }




    // --- roles


    /**
     * Роли
     *
     * @param array
     * @return string
     */
    public static function roles ($segments = []) {
        return static::segments('/roles', $segments);
    }


    /**
     * Просмотр роли
     *
     * @param int
     * @return string
     */
    public static function rolesHandler ($itemId) {
        return static::roles([$itemId]);
    }


    /**
     * Новая роль
     *
     * @return string
     */
    public static function rolesNew () {
        return static::roles(['new']);
    }


    /**
     * Корпирование
     *
     * @param int
     * @return string
     */
    public static function rolesCopy ($itemId) {
        return static::rolesNew().'?copy='.$itemId;
    }


    /**
     * Редактирование роли
     *
     * @param int
     * @return string
     */
    public static function rolesEdit ($itemId) {
        return static::roles([$itemId, 'edit']);
    }





    // --- trackers


    /**
     * Трекеры
     *
     * @param array
     * @return string
     */
    public static function trackers ($segments = []) {
        return static::segments('/trackers', $segments);
    }


    /**
     * Просмотр роли
     *
     * @param int
     * @return string
     */
    public static function trackersView ($itemId) {
        return static::trackers([$itemId]);
    }


    /**
     * Новая роль
     *
     * @return string
     */
    public static function trackersNew () {
        return static::trackers(['new']);
    }


    /**
     * Редактирование трекера
     *
     * @param int
     * @return string
     */
    public static function trackersEdit ($itemId) {
        return static::trackers([$itemId, 'edit']);
    }




    // --- issue status

    /**
     * Статусы задач
     *
     * @param array
     * @return string
     */
    public static function issueStatus ($segments = []) {
        return static::segments('/issue_statuses', $segments);
    }

    /**
     * Новый статус
     *
     * @return string
     */
    public static function issueStatusNew () {
        return static::issueStatus(['new']);
    }

    /**
     * Редактирование
     *
     * @param int
     * @return string
     */
    public static function issueStatusEdit ($itemId) {
        return static::issueStatus([$itemId, 'edit']);
    }

    /**
     * Handler
     *
     * @param int
     * @return string
     */
    public static function issueStatusHandler ($itemId) {
        return static::issueStatus([$itemId]);
    }



    // --- admin

    /**
     * Admin panel
     *
     * @param string
     * @return string
     */
    public static function admin ($part = "")
    {
        $url = '/admin';
        if ($part) {
            $url .= "/$part";
        }
        return static::toRoute($url);
    }


    /**
     * Настройки
     *
     * @param strings
     * @param string
     */
    public static function settings ($part = "", $tab = "") {
        $url = "/settings";
        if ($part) {
            $url .= "/$part";
        }
        if ($tab) {
            $url .= "?tab=$tab";
        }
        return static::toRoute($url);
    }


}
