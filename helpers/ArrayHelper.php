<?php

namespace app\helpers;


/**
 * Class ArrayHelper
 * @package app\helpers
 */
class ArrayHelper extends \yii\helpers\ArrayHelper {


    /**
     * Получаем значения по ключу из асс. массива
     *
     * @param $array
     * @param $value
     * @return array
     */
    static function ArrayValues ($array, $value)
    {
        if (!is_array($array)) return;

        $res_array = [];
        foreach ($array as $item) {
            if (is_object($item) ) {
                $res_array[] = $item->$value;
            }
            else {
                $res_array[] = $item[$value];
            }
        }

        return $res_array;

    }


    /**
     * Получаем из массива асс. массива ключ => значение
     *
     * @param $array
     * @param $key
     * @param $value
     * @param bool $distinct
     * @return array
     */
    static function Array2KeyValueArray ($array, $key, $value, $distinct = true)
    {
        if (!is_array($array)) return;

        $res_array = [];
        foreach ($array as $item) {

            if (is_object($item) ) {
                $_key = $item->$key;
                $_value = $item->$value;
            }
            else {
                $_key = &$item[$key];
                $_value = &$item[$value];
            }

            if ($distinct) {
                $res_array[$_key] = $_value;
            }
            else {
                $res_array[$_key][] = $_value;
            }
        }

        return $res_array;

    }


    /**
     * Получаем из асс. массива ключ => элемент
     *
     * @param $array
     * @param $field
     * @param bool $distinct
     * @return array
     */
    static function Array2KeyRowArray ($array, $field, $distinct = true)
    {
        if (!is_array($array)) return;

        $res_array = [];
        foreach ($array as $value) {
            if (is_object($value) ) {
                $key = &$value->$field;
            }
            else {
                $key = &$value[$field];
            }
            if ($distinct) {
                $res_array[$key] = $value;
            }
            else {
                $res_array[$key][] = $value;
            }
        }

        return $res_array;

    }


    /**
     * Array to row list array
     *
     * @param $array
     * @param $field
     * @param $fieldsOwner
     * @param string $childKey
     * @param bool $distinct
     * @return array
     */
    public static function Array2RowListArray ($array, $fieldId, $fieldsOwnerPrefix, $keyChild = 'items', $distinct = true) {
        $allOwners = self::Array2KeyRowArray($array, $fieldId, true);
        $allChilds = self::Array2KeyRowArray($array, $fieldId, false);


        // Fetch list
        $result = [];
        foreach ($allChilds as $ownerId => $listChilds) {

            // Init new item
            $item = [$keyChild => []];

            // Set owner fields
            foreach ($allOwners[$ownerId] as $field => $value) {
                if (static::checkFieldByPrefix($field, $fieldsOwnerPrefix) ) {
                    $item[$field] = $allOwners[$ownerId][$field];
                }
            }

            // Set childs for owner
            foreach ($listChilds as $childInfo) {
                foreach ($childInfo as $field => $value) {
                    if (static::checkFieldByPrefix($field, $fieldsOwnerPrefix) ) {
                        unset ($childInfo[$field]);
                    }
                }
                if ($distinct) {
                    $item[$keyChild] = $childInfo;
                }
                else {
                    $item[$keyChild][] = $childInfo;
                }
            }

            $result[] = $item;
        }

        return $result;
    }


    /**
     * Check field by prefix
     *
     * @param $field
     * @param $fieldsOwnerPrefix
     * @return bool
     */
    private static function checkFieldByPrefix ($field, $fieldsOwnerPrefix) {
        if (!is_array($fieldsOwnerPrefix) ) {
            $fieldsOwnerPrefix = [$fieldsOwnerPrefix];
        }

        foreach ($fieldsOwnerPrefix as $prefix) {
            if (strpos($field, $prefix) === 0) {
                return true;
            }
        }
        return false;
    }


    /**
     * Получаем массив вида [bit => value], где bit = 2^key
     *
     * @param array
     * @return array
     */
    static function getBitValueArray ($array) {
        $result = [];
        foreach ($array as $key => $value) {
            $result[pow(2, $key)] = $value;
        }
        return $result;
    }


    /**
     * Формирует каскадный ассоциативный массив
     *
     * @param link
     * @param array
     * @param string
     * @param string
     * @param int
     */
    static function a_fetchSubitems (&$items, $all_items, $field_id = "id", $field_parent_id = "parent_id", $parent_id = -1)
    {
        $items = [];
        foreach ($all_items as $key => $value) {
            if ($value[$field_parent_id] == $parent_id) {
                $items[$value[$field_id]] = $value;
            }
        }

        $all_items = self::Array2KeyRowArray($all_items, $field_parent_id, false);

        // формируем массив subitems
        self::a_setSubitems ($items, $all_items, $field_id, "subItems");
    }


    /**
     * Каскадная ф-ция для формарования ассоциативного массива
     *
     * @param link
     * @param array
     * @param string
     * @param string
     */
    static function a_setSubitems (&$items, $subitems, $field_id = "id", $name = "subItems")
    {
        if ($items) {
            foreach ($items as $key => $value) {
                if (isset($subitems[$key]) ) {
                    foreach ($subitems[$key] as $skey => $svalue) {
                        $items[$key][$name][$svalue[$field_id]] = $svalue;
                    }
                    self::a_setSubitems ($items[$key][$name], $subitems, $field_id, $name);
                }
            }
        }
    }


    /**
     * Получение всех id подгрупп
     *
     * @param array
     * @param int
     * @param string
     * @param string
     * @param int
     */
    static function a_getChildGroupIds ($groups, $id, $field_id = "id", $field_parent_id = "parent_id", $cascade_level = false)
    {
        $groups = self::Array2KeyRowArray ($groups, $field_parent_id, false);

        if (!$groups) return;

        $group_ids  = []; // id всех подгрупп
        $parent_ids = [$id];
        $level = 1;
        while ($parent_ids) {

            $new_parent_ids = array ();
            foreach ($parent_ids as $parent_id) {

                // $groups[$parent_id] - все подгруппы $parent_id
                if (isset($groups[$parent_id]) ) {
                    foreach ($groups[$parent_id] as $group_info) {
                        $group_ids[] = $group_info[$field_id];
                        $new_parent_ids[] = $group_info[$field_id];
                    }
                }

            }

            // проверка на уровень каскадности
            if ($cascade_level !== false && $level >= $cascade_level) {
                break;
            }
            $level++;

            $parent_ids = $new_parent_ids;
        }

        return $group_ids;

    }



    /**
     * получение всех id родительских групп
     *
     * @param array
     * @param int
     * @param string
     * @param string
     * @param bool
     */
    static function a_getParentGroupIds ($groups, $parent_id, $field_id = "id", $field_parent_id = "parent_id", $fetch = true)
    {
        if ($fetch) {
            $groups = self::Array2KeyValueArray ($groups, $field_id, $field_parent_id);
        }
        if (!$groups) return;

        $parent_ids = [$parent_id];
        while ($parent_id > 0) {
            if (!isset($groups[$parent_id]) ) {
                break;
            }
            $parent_id		= $groups[$parent_id];
            if ($parent_id <= 0) {
                break;
            }
            $parent_ids[]	= $parent_id;
        }

        return array_reverse($parent_ids);

    }


    /**
     * Получаем уровни по parent_id
     *
     * @param array
     * @return array
     */
    static function a_getCascadeLevels (&$items, $all_items, $field_id, $field_parent_id, $parent_id = -1, $level = 1)
    {
        if ($level == 1) {
            $items[-1] = 0;
            $all_items = self::Array2KeyRowArray ($all_items, $field_parent_id, false);
        }

        if (!isset($all_items[$parent_id]) ) {
            return [];
        }

        foreach ($all_items[$parent_id] as $info) {
            $items[$info[$field_id]] = $level;
            self::a_getCascadeLevels ($items, $all_items, $field_id, $field_parent_id, $info[$field_id], $level+1);
        }
    }

    static function a_filter ($array) {
        return array_filter ($array, "\\app\\helpers\\a_empty");
    }

}


/**
 * Array map & link
 *
 * @param link
 * @param string|array
 */
function array_mapself (&$art, $func) {
    $arr = array_map ($func, $arr);
}


/**
 * Проверка на пустой элемент
 *
 * @param unknown
 */
function a_empty ($value) {
    if (trim($value) != "")
        return $value;
}


/**
 * Удаление пустых элементов в массиве
 *
 * @param array
 * @return array
 */
function a_filter ($array) {
    return ArrayHelper::a_filter($array);
}