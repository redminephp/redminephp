<?php

namespace app\helpers;

class Notify {

    // ключ для сессии уведомлений
    static $notifySessionKey = "_ntfy";


    /**
     * Устанавливаем уведомление
     *
     * @param string
     * @param string|array
     */
    static function set ($type, $notify) {
        $notifications = static::getList (false);

        if (!is_array($notify) ) {
            $notifications[$type][] = $notify;
        }
        else {
            $notifications[$type] = $notify;
        }

        \Yii::$app->session->set(static::$notifySessionKey, $notifications);
    }


    /**
     * Получаем список уведомлений
     *
     * @param bool
     * @return array
     */
    static public function getList ($clear = true) {
        $notifications = \Yii::$app->session->get(static::$notifySessionKey);
        if (!$notifications) {
            $notifications = ['error' => [], 'text' => []];
        }
        if ($clear) {
            static::clearAll ();
        }
        return $notifications;
    }


    /**
     * Очищаем список
     */
    static function clearAll () {
        \Yii::$app->session->set(static::$notifySessionKey, '');
    }

}