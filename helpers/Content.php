<?php

namespace app\helpers;

class Content {

    public $content = [];

    /**
     * Создание объекта
     * @return Content
     */
    static function create () {
        return new Content ();
    }


    /**
     * Инициализируем запись контента в буфер
     */
    public function begin () {
        ob_start();
    }


    /**
     * Получаем контент из буфера
     *
     * @param string
     */
    public function end ($key = false) {
        $content = ob_get_contents(); ob_end_clean();
        if (!$key) {
            return $content;
        }
        $this->content[$key] = $content;
    }


    /**
     * Получаем контент по ключу
     *
     * @param string
     * @return text
     */
    public function get ($key) {
        return isset($this->content[$key]) ? $this->content[$key] : "";
    }



}