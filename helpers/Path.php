<?php

namespace app\helpers;

use Yii;

/**
 * Class Url
 * @package app\helpers
 */
class Path extends \yii\helpers\Url
{

    /**
     * Login url
     *
     * @return string
     * @return string
     */
    public static function base ($source)
    {
        return Yii::$app->request->baseUrl.$source;
    }



}
