<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
	public $basePath = '@webroot';
	public $baseUrl = '@web';
	public $css = [
		'css/application.css',
	];
	public $js = [
		//'js/jquery/jquery-1.8.3-ui-1.9.2-ujs-2.0.3.js',
        //'js/jquery/jquery.js',
		'js/application.js',
		'js/flex/tvmsort.js',
	];
	public $depends = [
		'yii\web\YiiAsset',
		//'yii\bootstrap\BootstrapAsset',
	];
}