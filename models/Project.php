<?php

namespace app\models;


use app\models\Project\Module;
use app\models\Project\Tracker as ProjectTracker;

use yii\db\ActiveQuery;

use app\helpers\ArrayHelper;


class Project extends Base
{
    const Table			= "projects";

    const Id			    = "project_id";
    const Name              = "project_name";
    const Description       = "project_description";
    const Homepage          = "project_homepage";
    const IsPublic          = "project_is_public";
    const ParentId          = "project_parent_id";
    const CreatedOn         = "project_created_on";
    const UpdatedOn         = "project_updated_on";
    const Identifier        = "project_identifier";
    const Status            = "project_status";
    const Left              = "project_lft";
    const Right             = "project_rgt";
    const InheritMembers    = "project_inherit_members";

    static $fieldPrefix = "project_";

    const StatusActive      = 1;
    const StatusClosed      = 5;
    const StatusArchived    = 9;

    static $statuses = [
        ''                      => 'all',
        self::StatusActive      => 'active',
        self::StatusClosed      => 'closed',
        self::StatusArchived    => 'archived',
    ];

    public $id;
    public $name;
    public $description;
    public $homepage;
    public $is_public;
    public $parent_id;
    public $created_on;
    public $updated_on;
    public $identifier;
    public $status;
    public $lft;
    public $rgt;
    public $inherit_members;
    public $enabled_module_names;
    public $tracker_ids;

    public $tab = 'info';

    static $tabs = [
        "info"          => ["title" => "Information"],
        "modules"       => ["title" => "Modules"],
        "members"       => ["title" => "Members"],
        "versions"      => ["title" => "Versions"],
        "categories"    => ["title" => "Issue categories"],
        "wiki"          => ["title" => "Wiki"],
        "repositories"  => ["title" => "Repositories"],
        "boards"        => ["title" => "Forums"],
        "activities"    => ["title" => "Activities (time tracking)"],
    ];


    /**
     * Инициализация
     */
    function init () {

        // списковые значения
        static::$listValues['enabled_module_names']     = Settings::object()->getListValues('default_projects_modules');
        static::$listValues['tracker_ids']              = Settings::object()->getListValues('default_projects_tracker_ids');

        // id проектов
        static::$listValues['parent_id']                = static::getList(['fetchKeyValue' => true]);
        static::$listValues['parent_id'][""] = "";
        sort(static::$listValues['parent_id']);

        // список статусов
        static::$listValues['status']                   = static::$statuses;

        // инициализация данным по settings
        $this->enabled_module_names = Settings::object()->getItem('default_projects_modules');
        $this->tracker_ids          = Settings::object()->getItem('default_projects_tracker_ids');
    }


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['name', 'string'],
            ['description', 'string'],
            ['identifier', 'checkIdentifier'],
            ['homepage', 'string'],
            ['is_public', 'boolean'],
            ['parent_id', 'in', 'range' => $this->getListValues('parent_id', true)],
            ['status', 'in', 'range' => $this->getListValues('status', true)],
            ['inherit_members', 'boolean'],
            ['enabled_module_names', 'in', 'range' => $this->getListValues('enabled_module_names', true), 'multivalue' => true],
            ['tracker_ids', 'in', 'range' => $this->getListValues('tracker_ids', true), 'multivalue' => true],
            [['name', 'identifier'], 'required'],
        ];
    }


    /**
     * Получаем label
     *
     * @return array
     */
    public function attributeLabels ()
    {
        return [
            "name"                  => "Name",
            "description"           => "Description",
            "identifier"            => "Identifier",
            "homepage"              => "Homepage",
            "is_public"             => "Public",
            "parent_id"             => "Subproject of",
            "inherit_members"       => "Inherit members",
            "enabled_module_names"  => "Modules",
            "tracker_ids"           => "Trackers",
        ];
    }


    /**
     * Получаем список
     *
     * @param array
     * @param array
     * @return array
     */
    public static function getList ($params = []) {
        $fetchCascade = ArrayHelper::getValue($params, 'fetchCascade');

        // если нужна обработка списка
        if ($fetchCascade === true) {
            $params['fetchCascade'] = [
                'fieldId'       => self::Id,
                'fieldParentId' => self::ParentId,
                'parentId'      => 0,
            ];
        }

        return parent::getList ($params);
    }


    /**
     * Получаем проект по идентификатору
     *
     * @param string
     * @param int
     * @return array
     */
    public static function getByIdentifier ($identifier, $project_id = false) {
        $query = static::find();

        $query->where ([self::Identifier => $identifier]);

        if ($project_id) {
            $query->where([self::Id .'!= '.$project_id]);
        }
        return $query->oneDecode();
    }


    /**
     * Сохранение
     *
     * @return bool|void
     */
    public function save ($runValidation = true, $attributeNames = NULL) {

        $this->updated_on = date("Y-m-d H:i:s");

        // добавление
        if (!$this->id) {
            $this->created_on   = date("Y-m-d H:i:s");
            $this->status       = self::StatusActive;
            $this->selfinsert(false);

            // добавление modules
            foreach ($this->enabled_module_names as $module_name) {
                Module::addItem ($this->id, $module_name);
            }

            // добавление tracker_ids
            foreach ($this->tracker_ids as $tracker_id) {
                ProjectTracker::addItem ($this->id, $tracker_id);
            }
        }

        // редактирование
        else {
            $this->selfupdate(false);
        }
    }


    /**
     * Проверка индентификатора
     *
     * @param string
     * @param array
     */
    public function checkIdentifier ($attribute, $params) {
        if (!preg_match("#^[a-z]{1,}[a-z0-9\-\_]{0,}$#", $this->$attribute) ) {
            $this->addError($attribute, "Identifier is invalid");
        }
        if (!$this->$attribute) {
            $this->addError($attribute, "Name can't be blank");
        }
        if ($this->$attribute == 'new' || $this->getByIdentifier($this->$attribute) ) {
            $this->addError($attribute, "Identifier has already been taken");
        }
    }



}
