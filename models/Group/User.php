<?php

namespace app\models\Group;

use yii\db\ActiveQuery;
use app\helpers\ArrayHelper;

use app\models\User as UserBase;
use app\models\Group as GroupBase;


/**
 * Класс для работы со статусами задач
 */
class User extends \app\models\Base
{
    const Table = "groups_users";

    const GroupId				= "groups_users_group_id";
    const UserId				= "groups_users_user_id";


    /**
     * Получение списка
     *
     * @param array $params
     * @return array|void
     */
    public static function getList ($params = []) {
        $groupId = ArrayHelper::remove($params, 'groupId');

        $query = static::find();

        // получаем пользователей по группе
        if ($groupId) {
            $query->innerJoin(UserBase::tableName(), UserBase::Id .' = '. static::UserId);
            $params[static::GroupId] = $groupId;
        }

        $params['query'] = $query;

        return parent::getList($params);
    }


    /**
     * Добавляем запись
     *
     * @param int
     * @param int
     */
    public static function addItem ($groupId, $userId) {
        $query = new static;
        $query->setAttributes ([
            self::GroupId	=> $groupId,
            self::UserId	=> $userId,
        ], false);
        return $query->insert (false);
    }


    /**
     * Удаляем запись
     *
     * @param int
     * @param int
     */
    public static function deleteItem ($groupId, $userId) {
        static::deleteAll([
            self::GroupId	=> $groupId,
            self::UserId	=> $userId,
        ]);
    }

}

?>