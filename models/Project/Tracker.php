<?php

namespace app\models\Project;

use yii\db\ActiveQuery;

use app\helpers\ArrayHelper;


class Tracker extends \app\models\Base
{
    const Table			= "projects_trackers";

    const ProjectId     = "projects_trackers_project_id";
    const TrackerId     = "projects_trackers_tracker_id";


    /**
     * Добавляем запись
     *
     * @param int
     * @param int
     */
    public static function addItem ($project_id, $tracker_id) {
        $query = new Tracker;
        $query->setAttributes ([
            self::ProjectId	=> $project_id,
            self::TrackerId	=> $tracker_id,
        ], false);
        return $query->insert (false);
    }


    /**
     * Изменение элементов
     *
     * @param array
     * @param array
     */
    public static function changeItems ($tracker_id, $old_items, $new_items) {

        // массивы новых и старых элементов
        $del_items = array_diff ($old_items, $new_items);
        $add_items = array_diff ($new_items, $old_items);

        // удаляем
        static::deleteAll([
            self::TrackerId => $tracker_id,
            self::ProjectId => $del_items
        ]);

        // добавляем
        foreach ($add_items as $itemId) {
            static::addItem ($itemId, $tracker_id);
        }
    }

}

