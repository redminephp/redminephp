<?php

namespace app\models\Project;

use yii\db\ActiveQuery;

use app\helpers\ArrayHelper;


class Module extends \app\models\Base
{
    const Table			= "enabled_modules";

    const Id            = "enabled_modules_id";
    const ProjectId     = "enabled_modules_project_id";
    const Name          = "enabled_modules_name";


    /**
     * Добавляем запись
     *
     * @param int
     * @param int
     */
    public function addItem ($project_id, $name) {
        $query = new Module;
        $query->setAttributes ([
            self::ProjectId	=> $project_id,
            self::Name	    => $name,
        ], false);
        return $query->insert (false);
    }


}

