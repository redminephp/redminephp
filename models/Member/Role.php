<?php

namespace app\models\Member;

use yii\db\ActiveQuery;
use app\helpers\ArrayHelper;


/**
 * Класс для работы с ролями members
 */
class Role extends \app\models\BaseArrayable
{
    const Table = "member_roles";

    const Id			= "member_role_id";
    const MemberId		= "member_role_member_id";
    const RoleId		= "member_role_role_id";
    const IngeritedForm = "member_role_inherited_from";



    /**
     * Get list
     *
     * @param array $params
     * @return array|void
     */
    public static function getList ($params = []) {
        $memberId = ArrayHelper::remove ($params, 'memberId');

        $query = static::find();

        // Get roles
        if ($memberId) {
            $query->innerJoin(\app\models\Role::tableName(), Role::Id .' = '. static::RoleId);
            $query->addWhere ([self::MemberId, $memberId]);
        }

        $params['query'] = $query;

        return parent::getList($params);
    }


    /**
     * Добавляем запись
     *
     * @param int
     * @param int
     */
    public static function addItem ($memberId, $roleId) {
        /*if (!static::getItem($memberId, $roleId) ) {
            return false;
        }*/

        $query = new static;
        $query->setAttributes ([
            self::MemberId	=> $memberId,
            self::RoleId	=> $roleId,
        ], false);
        return $query->insert (false);
    }


    /**
     * Get item
     *
     * @param int
     * @param int
     * @return object
     */
    public static function getItem ($memberId, $roleId) {
        return static::find()
            ->addWhere ([self::MemberId, $memberId])
            ->addWhere ([self::RoleId, $roleId]);
    }

}