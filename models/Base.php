<?php

namespace app\models;

use yii\base\Exception;
use yii\db\ActiveQuery;

use app\helpers\ArrayHelper;

/*

where not in => ['where' => [
    ['not in', $field, $array]
]]

 */


/**
 * Class Base
 * @package app\models
 */
class Base extends \yii\db\ActiveRecord
{

    // перед populate model: ->load(..)
    const EVENT_BEFORE_POPULATE = 'beforePopulate';

    // события перед и после сохранения
    const EVENT_BEFORE_ITEM_SAVE    = 'beforeItemSave';
    const EVENT_AFTER_ITEM_SAVE     = 'afterItemSave';

    // после инициализации элемента (получение из БД)
    const EVENT_AFTER_ITEM_INIT     = 'afterItemInit';

    // префикс к полям в таблице
    static $fieldPrefix = '';

    // если уже декодированы
    private $_isDecoded = false;

    // результатом получения одной записи будет объект соответствующей модели
    static $isArrayable = false;

    // список stored-атрибутов
    protected $storedAttributes = [];

    // список списковых значений
    static $listValues = [];


    /**
     * Init
     */
    public function init () {

        // Set event before ->load(..) - save original attribute values
        $this->on(self::EVENT_BEFORE_POPULATE, function ($event) {
            $this->setStoredAttributes();
        });

    }


    /**
     * Поля при распеделении массива как ключ => значение
     *
     * @return array
     */
    public static function fetchFields () {
        return ['key' => static::Id, 'value' => static::Name];
    }


    // базовые ф-ции запросов


    /**
     * Получаем проект по идентификатору
     *
     * @param string
     * @param int
     * @return array
     */
    public static function getById ($itemId) {

        // Init query
        $query = static::find()
            ->where ([static::Id => $itemId]);

        // Get one
        $item = $query->one();

        // триггер перед сохранения записи
        $item->trigger(self::EVENT_AFTER_ITEM_INIT);

        return $item;
    }


    /**
     * Удаляем по id
     *
     * @param int
     */
    public static function deleteById ($itemId) {
        static::deleteAll ([static::Id => $itemId]);
    }


    /**
     * Change list items
     *
     * @param string
     * @param string
     * @param array
     */
    public function saveMany2Many ($modelClass, $key) {

        // New items
        $newItems = $this->$key;

        // Unlink all old items
        $this->unlinkAll ($key);

        // Link new items
        if ($newItems) {
            $newItems = $modelClass::find()->where([$modelClass::primaryKey() => $newItems])->all();
            foreach ($newItems as $newItem) {
                $this->link($key, $newItem);
            }
        }
    }


    /**
     * Получаем список
     *
     * @param array
     * @param array
     * @return array
     */
    public static function getList ($params = []) {

        // Query
        $query = ArrayHelper::remove($params, 'query', static::find());

        // Only count
        $onlyCount = ArrayHelper::getValue($params, 'onlyCount', false);

        // Find by params
        $query->byParams($params);

        // Get only count
        if ($onlyCount) {
            return $query->count($onlyCount);
        }

        // Get fetch list
        return $query->fetch ($params);
    }


    /**
     * Alias'ы для атрибутов [alias => field]
     *
     * @return array
     */
    public static function attributeAliasList () {
        return [];
    }


    /**
     * Получаем alias поля
     *
     * @param $name
     * @return mixed
     */
    public function getAttribute4Column ($name) {

        // получаем поля
        $schema     = $this->getTableSchema();
        $columns    = array_keys ($schema->columns);

        // если атрибут идет как поле
        if (in_array($name, $columns) ) {

            // учет списка alias-атрибутов
            $alias_list = $this->attributeAliasList ();
            if (isset($alias_list[$name]) ) {
                return $alias_list[$name];
            }

            // если есть префикс - заменяем
            else if (static::$fieldPrefix) {
                $name = str_replace(static::$fieldPrefix, '', $name);
            }
        }

        return $name;
    }


    /**
     * Получаем список атрибутов по полям
     *
     * @return array
     */
    public function getColumnValueList () {

        // поля
        $columns = array_keys(static::getTableSchema()->columns);

        // атрибуты
        $attributes = [];
        foreach ($columns as $column) {
            if ($attribute = $this->getAttribute4Column ($column) ) {
                $attributes[$column] = $this->$attribute;
            }
        }

        return $attributes;
    }


    /**
     * Получаем атрибуты по полям (внутренние данные)
     *
     * @return array
     */
    public function getAttributesInner () {

        // поля
        $columns = array_keys(static::getTableSchema()->columns);

        // атрибуты
        $attributes = [];
        foreach ($columns as $column) {
            if ($attribute = $this->getAttribute4Column ($column) ) {
                $attributes[] = $attribute;
            }
        }

        return $attributes;
    }


    /**
     * Инициализацию safe attributes
     *
     * @param array
     */
    public function setStoredAttributes ($attributes = false) {
        if (!$attributes) {
            $this->storedAttributes = $this->getAttributes();
            return;
        }
        foreach ($attributes as $attribute) {
            $this->storedAttributes[$attribute] = $this->$attribute;
        }
    }


    /**
     * Получаем safe attributes
     *
     * @return array
     */
    public function getStoredAttributes () {
        return $this->storedAttributes;
    }


    /**
     * Get stored attribute
     *
     * @param $attribute
     * @return bool
     */
    public function getStoredAttribute ($attribute, $defaultValue = false) {
        return isset($this->storedAttributes[$attribute]) ? $this->storedAttributes[$attribute] : $defaultValue;
    }


    /**
     * Восстановление safe attributes
     *
     * @param array
     */
    public function restoreAttributes ($attributes = []) {
        if (!$attributes) {
            $attributes = $this->getAttributesInner();
        }
        foreach ($attributes as $attribute) {
            if (isset($this->storedAttributes[$attribute]) ) {
                $this->$attribute = $this->storedAttributes[$attribute];
            }
        }
    }


    /**
     * Восстановление safe attributes по ошибкам
     */
    public function restoreAttributes4Errors () {
        $attributes = array_keys($this->getErrors());
        $this->restoreAttributes ($attributes);
    }


    /**
     * Декодируем значения для списка (если нужно)
     *
     * @param link
     */
    public function decodeValues4List (&$attributes) {
        foreach ($attributes as $attribute => $value) {
            $this->decodeAttributeValue ($attributes[$attribute], $attribute);
        }
    }


    /**
     * Декодируем атрибуты
     *
     * @param bool
     */
    public function decodeAttributes ($through = false) {
        if (!$through && $this->_isDecoded) {
            return;
        }
        $attributes = $this->getAttributesInner();
        foreach ($attributes as $attribute) {
            $this->decodeAttributeValue ($this->$attribute, $attribute);
        }
        $this->_isDecoded = true;
    }


    /**
     * Декодируем значение атрибута если нужно
     *
     * @param string
     * @return unknown
     */
    public function decodeAttributeValue (&$value, $attribute) {
        $validators = $this->getActiveValidators($attribute);

        // проходимся по всем валидаторам, если встречается ф-цию decode - декодируем
        foreach ($validators as $validator) {
            if (method_exists($validator, "decode") ) {
                $value = $validator->decode($value);
            }
        }
    }


    /**
     * Return table name
     *
     * @return string
     */
    public static function tableName ($alias = '') {
        if (!defined('static::Table') ) {
            throw new \Exception ("Constant Table required in class ".static::className());
        }
        $table = static::Table;
        if ($alias) {
            $table = $table . ' ' . $alias;
        }
        return $table;
    }


    /**
     * Получаем объект
     *
     * @return object
     */
    public static function object () {
        static $object = false;
        if ($object === false) {
            $object = new static;
        }
        return $object;
    }


    /**
     * Получаем чистые список ошибок
     *
     * @return array
     */
    public function getClearErrorList () {
        $errors = $this->getErrors(); $result = [];
        foreach ($errors as $key => $value) {
            $result = array_merge($result, $value);
        }
        return $result;
    }


    /**
     * Получаем список опций из общего списка
     *
     * @param string
     * @param bool
     * @return array
     */
    public function getListValues ($key, $only_keys = false) {
        if (!isset(static::$listValues[$key]) ) {
            return [];
        }
        if ($only_keys) {
            return array_keys (static::$listValues[$key]);
        }
        return static::$listValues[$key];
    }


    /**
     * Get list values for add
     *
     * @param $key
     * @param string
     * @return array
     */
    public function getListValuesDiff2Saved ($key, $fieldId = 'id') {
        $list = ArrayHelper::ArrayValues($this->$key, $fieldId);
        $list = array_diff($this->getListValues($key, true), $list);
        return array_values ($list);
    }



    // --- active records functions

    /**
     * Insert
     *
     * @param bool $runValidation
     * @param null $attributes
     * @return bool|void
     */
    public function selfinsert ($runValidation = true, $attributes = null) {

        // устанавливаем атрибуты
        $this->setAttributes ($this->getColumnValueList(), false);

        // вызываем parent-метод insert'a
        parent::insert($runValidation, $attributes);

        // устанавливаем id
        $this->id = static::getDb()->getLastInsertID();
    }


    /**
     * Update
     *
     * @param bool $runValidation
     * @param null $attributeNames
     * @return bool|int|void
     */
    public function selfupdate ($runValidation = true, $attributeNames = null) {

        // устанавливаем id
        $this->setOldAttribute (static::Id, $this->id);

        // устанавливаем атрибуты
        $this->setAttributes ($this->getColumnValueList(), false);

        // вызываем parent-метод insert'a
        parent::update($runValidation, $attributeNames);
    }






    // ---------- OVERWRITE METHODS


    /**
     * Сохранение
     *
     * @return bool|void
     */
    public function save ($runValidation = true, $attributeNames = NULL) {

        // триггер перед сохранения записи
        $this->trigger(self::EVENT_BEFORE_ITEM_SAVE);

        // добавление
        if (!$this->id) {
            $this->selfinsert(false);
        }

        // редактирование
        else {
            $this->selfupdate(false);
        }

        // триггер после сохранения записи
        $this->trigger(self::EVENT_AFTER_ITEM_SAVE);
    }


    /**
     * Overwrite find function
     *
     * @return object \app\db\ActiveQuery
     */
    public static function find()
    {
        return new \app\db\ActiveQuery(static::className());
    }


    /**
     * Загрузка данных в модель
     *
     * @param array
     * @param null
     * @return
     */
    public function load($data, $formName = null) {

        // триггер перед populate model
        $this->trigger(self::EVENT_BEFORE_POPULATE);

        return parent::load($data, $formName);
    }


    /**
     * Инициализируем атрибуты объекта
     *
     * @param object
     * @param $row
     */
    static function populateRecord ($record, $row) {
        foreach ($row as $column => $value) {
            $attribute          = $record->getAttribute4Column ($column);
            $record->$attribute = $value;
        }
        parent::populateRecord($record, $row);
    }


    /**
     * Where для сортировки по позиции
     *
     * @return bool
     */
    public static function positionListWhere () {
        return false;
    }
}