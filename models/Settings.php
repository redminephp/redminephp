<?php

namespace app\models;

use yii\db\ActiveQuery;
use app\helpers\ArrayHelper;

use app\models\User;
use app\models\user\UserAuth;


/**
 * Class Settings
 * @package app\models
 */
class Settings extends BaseArrayable {

    const Table			= "settings";

    const Id			= "setting_id";
    const Name			= "setting_name";
    const Value			= "setting_value";
    const DateUpdate	= "setting_updated_on";

    static $fieldPrefix = "setting_";

    // атрибуты
    public $id;
    public $name;
    public $value;
    public $updated_on;


    // tab
    public $tab;

    // properties
    public $app_title                   = "";
    public $welcome_text                = "Welcome text";
    public $attachment_max_size         = -1;
    public $per_page_options            = "";
    public $activity_days_default       = 0;
    public $host_name                   = "";
    public $protocol                    = "http";
    public $text_formatting             = "textile";
    public $cache_formatted_text        = "none";
    public $wiki_compression            = "none";
    public $feeds_limit                 = 15;
    public $file_max_size_displayed     = 512;
    public $diff_max_lines_displayed    = 1500;
    public $repositories_encodings;
    public $ui_theme;
    public $default_language;
    public $force_default_language_for_anonymous;
    public $force_default_language_for_loggedin;
    public $start_of_week;
    public $date_format;
    public $time_format;
    public $user_format;
    public $gravatar_enabled;
    public $gravatar_default;
    public $thumbnails_enabled;
    public $thumbnails_size;
    public $login_required;
    public $autologin;
    public $self_registration;
    public $unsubscribe;
    public $password_min_length;
    public $lost_password;
    public $openid;
    public $rest_api_enabled;
    public $jsonp_enabled;
    public $session_lifetime;
    public $session_timeout;
    public $default_projects_public;
    public $default_projects_modules;
    public $default_projects_tracker_ids;
    public $sequential_project_identifiers;
    public $new_project_user_role_id;
    public $cross_project_issue_relations;
    public $cross_project_subtasks;
    public $issue_group_assignment;
    public $default_issue_start_date_to_creation_date;
    public $display_subprojects_issues;
    public $issue_done_ratio;
    public $non_working_week_days;
    public $issues_export_limit;
    public $gantt_items_limit;
    public $issue_list_default_columns;
    public $mail_from;
    public $bcc_recipients;
    public $plain_text_mail;
    public $default_notification_option;
    public $notified_events;
    public $emails_header;
    public $emails_footer;
    public $enabled_scm;
    public $autofetch_changesets;
    public $sys_api_key;
    public $repository_log_display_limit;
    public $commit_ref_keywords;
    public $commit_cross_project_ref;
    public $commit_logtime_enabled;
    public $commit_logtime_activity_id;
    public $commit_update_keywords;


    // ключи
    static $allKeys	= [

        /* general */
        "general" => ["app_title", "welcome_text", "attachment_max_size", "per_page_options", "activity_days_default", "host_name", "protocol", "text_formatting", "cache_formatted_text", "wiki_compression", "feeds_limit", "file_max_size_displayed", "diff_max_lines_displayed", "repositories_encodings"],

        /* display */
        "display" => ["ui_theme", "default_language", "force_default_language_for_anonymous", "force_default_language_for_loggedin", "start_of_week", "date_format", "time_format", "user_format", "gravatar_enabled", "gravatar_default", "thumbnails_enabled", "thumbnails_size"],

        /* authentication */
        "authentication" => ["login_required", "autologin", "self_registration", "unsubscribe", "password_min_length", "lost_password", "openid", "rest_api_enabled", "jsonp_enabled", "session_lifetime", "session_timeout"],

        /* projects */
        "projects" => ["default_projects_public", "default_projects_modules", "default_projects_tracker_ids", "sequential_project_identifiers", "new_project_user_role_id"],

        /* issues */
        "issues" => ["cross_project_issue_relations", "cross_project_subtasks", "issue_group_assignment", "default_issue_start_date_to_creation_date", "display_subprojects_issues", "non_working_week_days", "issues_export_limit", "gantt_items_limit", "issue_list_default_columns"],

        /* notifications */
        "notifications" => ["mail_from", "bcc_recipients", "plain_text_mail", "default_notification_option", "notified_events", "emails_header", "emails_footer"],

        /* repositories */
        "repositories" => ["enabled_scm", "autofetch_changesets", "sys_api_key", "repository_log_display_limit", "commit_ref_keywords", "commit_cross_project_ref", "commit_logtime_enabled", "commit_logtime_activity_id", "commit_update_keywords"],
    ];


    static $tabs = [
        "general"           => ["title" => "General"],
        "display"           => ["title" => "Display"],
        "authentication"    => ["title" => "Authentication"],
        "projects"          => ["title" => "Projects"],
        "issues"            => ["title" => "Issue tracking"],
        "notifications"     => ["title" => "Email notifications"],
        "mail_handler"      => ["title" => "Incoming emails"],
        "repositories"      => ["title" => "Repositories"],
    ];


    // типы
    static $tabKeys = [];

    static $listValues = [
        "protocol" => [
            "http"  => "HTTP",
            "https" => "HTTPS",
        ],
        "text_formatting" => [
            ""          => "none",
            "textile"   => "Textile",
        ],
        "wiki_compression" => [
            ""      => "none",
            "gzip"  => "GZip",
        ],
        "ui_theme" => [
            ""          => "Default",
            //"alternate" => "Alternate",
            //"classic"   => "Classic",
        ],
        "default_language" => [
            "en" => "English",
        ],
        "start_of_week" => [
            ""  => "Based on user's language",
            "1" => "Monday",
            "6" => "Saturday",
            "7" => "Sunday",
        ],
        "date_format" => [
            ""          => "Based on user's language",
            "%Y-%m-%d"  => "Y-m-d",
            "%d/%m/%Y"  => "d/m/Y",
            "%d.%m.%Y"  => "d.m.Y",
            "%d-%m-%Y"  => "d-m-Y",
            "%m/%d/%Y"  => "m/d/Y",
            "%d %b %Y"  => "d M Y",
            "%d %B %Y"  => "d F Y",
            "%b %d, %Y" => "M d, Y",
            "%B %d, %Y" => "F d, Y",
        ],
        "time_format" => [
            ""          => "Based on user's language",
            "%H:%M"     => "H:i",
            "%I:%M %p"  => "h:i a",
        ],
        "user_format" => [
            "firstname_lastname"        => "Redmine Admin",
            "firstname_lastinitial"     => "Redmine A.",
            "firstname"                 => "Redmine",
            "lastname_firstname"        => "Admin Redmine",
            "lastname_coma_firstname"   => "Admin, Redmine",
            "lastname"                  => "Admin",
            "username"                  => "%user%",
        ],
        "gravatar_default" => [
            ""              => "none",
            "wavatar"       => "Wavatars",
            "identicon"     => "Identicons",
            "monsterid"     => "Monster ids",
            "retro"         => "Retro",
            "mm"            => "Mystery man",
        ],
        "autologin" => [
            "0"     => "disabled",
            "1"     => "1 day",
            "7"     => "7 days",
            "30"    => "30 days",
            "365"   => "365 days",
        ],
        "self_registration" => [
            "0" => "disabled",
            "1" => "account activation by email",
            "2" => "manual account activation",
            "3" => "automatic account activation",
        ],
        "session_lifetime" => [
            "0"         => "disabled",
            "1440"      => "1 day",
            "10080"     => "7 days",
            "43200"     => "30 days",
            "86400"     => "60 days",
            "525600"    => "365 days",
        ],
        "session_timeout" => [
            "0"     => "disabled",
            "60"    => "1 hour",
            "120"   => "2 hours",
            "240"   => "4 hours",
            "480"   => "8 hours",
            "720"   => "12 hours",
            "1440"  => "24 hours",
            "2880"  => "48 hours",
        ],
        "default_projects_modules" => [
            "issue_tracking"    => "Issue tracking",
            "time_tracking"     => "Time tracking",
            "news"              => "News",
            "documents"         => "Documents",
            "files"             => "Files",
            "wiki"              => "Wiki",
            "repository"        => "Repository",
            "boards"            => "Forums",
            "calendar"          => "Calendar",
            "gantt"             => "Gantt",
        ],
        "default_projects_tracker_ids" => [],
        "new_project_user_role_id" => [],
        "issue_list_default_columns" => [
            "project"           => "Project",
            "parent"            => "Parent task",
            "author"            => "Author",
            "status"            => "Status",
            "priority"          => "Priority",
            "subject"           => "Subject",
            "author"            => "Author",
            "assigned_to"       => "Assignee",
            "updated_on"        => "Updated",
            "category"          => "Category",
            "fixed_version"     => "Target version",
            "start_date"        => "Start date",
            "due_date"          => "Due date",
            "estimated_hours"   => "Estimated time",
            "spent_hours"       => "Spent time",
            "done_ratio"        => "% Done",
            "created_on"        => "Created",
            "closed_on"         => "Closed",
            "relations"         => "Related issues",
            "is_private"        => "Private",
        ],
        "non_working_week_days" => [
            "1" => "Monday",
            "2" => "Tuesday",
            "3" => "Wednesday",
            "4" => "Thursday",
            "5" => "Friday",
            "6" => "Saturday",
            "7" => "Sunday",
        ],
        "cross_project_subtasks" => [
            ""              => "disabled",
            "system"        => "With all projects",
            "tree"          => "With project tree",
            "hierarchy"     => "With project hierarchy",
            "descendants"   => "With subprojects",
        ],
        "issue_done_ratio" => [
            "issue_field"   => "Use the issue field",
            "issue_status"  => "Use the issue status",
        ],
        "default_notification_option" => [
            "all"               => "For any event on all my projects",
            "only_my_events"    => "Only for things I watch or I'm involved in",
            "only_assigned"     => "Only for things I am assigned to",
            "only_owner"        => "Only for things I am the owner of",
            "none"              => "No events",
        ],
        "notified_events" => [
            "issue_added"               => "Issue added",
            "issue_updated"             => "Issue updated",
            "issue_note_added"          => "Note added",
            "issue_status_updated"      => "Status updated",
            "issue_priority_updated"    => "Priority updated",
            "news_added"                => "News added",
            "news_comment_added"        => "Comment added to a news",
            "document_added"            => "Document added",
            "file_added"                => "File added",
            "message_posted"            => "Message added",
            "wiki_content_added"        => "Wiki page added",
            "wiki_content_updated"      => "Wiki page updated",
        ],
        "notified_events_parent"    => [
            "issue_note_added",
            "issue_status_updated",
            "issue_priority_updated",
        ],
        "enabled_scm"    => [
            "Subversion"	=> [
                "command_title" => "svn", "command_enable" => true, "version" => "1.8.4", "disabled" => false,
            ],
            "Git"			=> [
                "command_title" => "git", "command_enable" => true, "version" => "1.8.3", "disabled" => false,
            ],
            "Darcs"			=> [
                "command_title" => "darcs", "command_enable" => false, "version" => "", "disabled" => true,
            ],
            "Mercurial"		=> [
                "command_title" => "hg", "command_enable" => false, "version" => "", "disabled" => true,
            ],
            "Cvs"			=> [
                "command_title" => "cvs", "command_enable" => false, "version" => "1.12.13", "disabled" => true,
            ],
            "Bazaar"		=> [
                "command_title" => "bzr", "command_enable" => false, "version" => "", "disabled" => true,
            ],
        ],
        "commit_logtime_activity_id"    => [
            "Default"		=> 0,
            "Design"		=> 8,
            "Development"	=> 9,
        ],
        "scuk_done_ratio"    => [
            "0 %"	=> 0,
            "10 %"	=> 10,
            "20 %"	=> 20,
            "30 %"	=> 30,
            "40 %"	=> 40,
            "50 %"	=> 50,
            "60 %"	=> 60,
            "70 %"	=> 70,
            "80 %"	=> 80,
            "90 %"	=> 90,
            "100 %"	=> 100,
        ],
        "scuk_if_tracker_id"    => [],
        "scuk_status_id"        => [],
    ];


    /**
     * Инициализация
     *
     * @param array
     */
    public function init ()
    {
        // получаем типы
        self::$tabKeys = array_keys (self::$tabs);
        self::$tabKeys[0] = ""; // general = ''

        // установка default'ных значений
        foreach (self::$allKeys as $tab => $fields) {
            $this->setStoredAttributes($fields);
        }

        // преобразование для даты и времени
        $date_format_fields = ["date_format", "time_format"];
        foreach ($date_format_fields as $field) {
            foreach (self::$listValues[$field] as $key => $value) {
                if ($key) {
                    self::$listValues[$field][$key] = date($value);
                }
            }
        }

        // заменяем имя пользователя
        foreach (self::$listValues["user_format"] as $key => $value) {
            self::$listValues["user_format"][$key] = str_replace ("%user%", UserAuth::info('login'), $value);
        }

        // данные по трекерам @todo естественно оптимизация получения списков!
        self::$listValues['default_projects_tracker_ids']  = Tracker::getList (["fetchKeyValue" => true]);
        self::$listValues['scuk_if_tracker_id']			= Tracker::getList (["fetchKeyValue" => true]);

        self::$listValues['scuk_status_id'] = Issue\Status::getList (["fetchKeyValue" => true]);

        // данные по ролям (@todo пока получаем все роли, далее определимся с алгоритмом)
        self::$listValues['new_project_user_role_id'][""] = '--- Please select ---';
        self::$listValues['new_project_user_role_id'] = array_merge(
            self::$listValues['new_project_user_role_id'],
            Role::getList ([
                Role::Assignable => 1,
                "fetchKeyValue" => ['key' => Role::Id, 'value' => Role::Name],
            ])
        );
    }


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [

            // settings types
            ["tab", "in", "range" => self::$tabKeys],

            /* general */
            [["app_title", "welcome_text", "host_name"], "string"],
            ["attachment_max_size", "number", "min" => -1],
            ["per_page_options", "sep_string"],
            ["protocol", "in", "range" => $this->getListValues('protocol', true)],
            ["text_formatting", "in", "range" => $this->getListValues('text_formatting', true)],
            ["cache_formatted_text", "boolean"], // check
            ["wiki_compression", "in", "range" => $this->getListValues('wiki_compression', true)],
            [["feeds_limit", "file_max_size_displayed", "diff_max_lines_displayed"], "number", "min" => 1],
            [["feeds_limit", "file_max_size_displayed", "diff_max_lines_displayed"], "required"],
            ["repositories_encodings", "sep_string", "fn" => "strval"],  // db_string ?

            /* display */
            ["ui_theme", "in", "range" => $this->getListValues('ui_theme', true)],
            ["default_language", "in", "range" => $this->getListValues('default_language', true)],
            ["force_default_language_for_anonymous", "boolean"], // check
            ["force_default_language_for_loggedin", "boolean"], // check
            ["start_of_week", "in", "range" => $this->getListValues('start_of_week', true)],
            ["date_format", "in", "range" => $this->getListValues('date_format', true)],
            ["time_format", "in", "range" => $this->getListValues('time_format', true)],
            ["user_format", "in", "range" => $this->getListValues('user_format', true)],
            ["gravatar_enabled", "boolean"], // check
            ["gravatar_default", "in", "range" => $this->getListValues('gravatar_default', true)],
            ["thumbnails_enabled", "boolean"], // check
            ["thumbnails_size", "number", "min" => -1],

            /* authentification */
            ["login_required", "boolean"], // check
            ["autologin", "in", "range" => $this->getListValues('autologin', true)],
            ["self_registration", "in", "range" => $this->getListValues('self_registration', true)],
            ["unsubscribe", "boolean"], // check
            ["password_min_length", "number", "min" => 4],
            ["lost_password", "boolean"], // check
            ["openid", "boolean"], // check
            ["rest_api_enabled", "boolean"], // check
            ["jsonp_enabled", "boolean"], // check
            ["session_lifetime", "in", "range" => $this->getListValues('session_lifetime', true)],
            ["session_timeout", "in", "range" => $this->getListValues('session_timeout', true)],

            /* projects */
            ["default_projects_public", "boolean"], // check
            ["default_projects_modules", "mv_string", "range" => $this->getListValues('default_projects_modules', true)],
            ["default_projects_tracker_ids", "mv_string", "range" => $this->getListValues('default_projects_tracker_ids', true)],
            ["sequential_project_identifiers", "boolean"], // check
            ["new_project_user_role_id", "in", "range" => $this->getListValues('new_project_user_role_id', true)],

            /* issues */
            ["cross_project_issue_relations", "boolean"], // check
            ["cross_project_subtasks", "in", "range" => $this->getListValues('cross_project_subtasks', true)],
            ["issue_group_assignment", "boolean"], // check
            ["default_issue_start_date_to_creation_date", "boolean"], // check
            ["display_subprojects_issues", "boolean"], // check
            ["issue_done_ratio", "in", "range" => $this->getListValues('issue_done_ratio', true)],
            ["non_working_week_days", "mv_string", "range" => $this->getListValues('non_working_week_days', true)],
            ["issues_export_limit", "number", "min" => -1],
            ["gantt_items_limit", "number", "min" => -1],
            ["issue_list_default_columns", "mv_string", "range" => $this->getListValues('issue_list_default_columns', true)],

            /* notifications */
            ["mail_from", "string"],
            ["bcc_recipients", "boolean"], // check
            ["plain_text_mail", "boolean"], // check
            ["default_notification_option", "in", "range" => $this->getListValues('default_notification_option', true)],
            ["notified_events", "mv_string", "range" => $this->getListValues('notified_events', true)],
            ["emails_header", "string"],
            ["emails_footer", "string"],
        ];
    }
    

    /**
     * Получаем label
     *
     * @return array
     */
    public function attributeLabels ()
    {
        return [

            /* general */
            "app_title"                             => "Application title",
            "welcome_text"                          => "Welcome text",
            "attachment_max_size"                   => "Maximum attachment size",
            "per_page_options"                      => "Objects per page options",
            "activity_days_default"                 => "Days displayed on project activity",
            "host_name"                             => "Host name and path",
            "protocol"                              => "Protocol",
            "text_formatting"                       => "Text formatting",
            "wiki_compression"                      => "Wiki history compression",
            "feeds_limit"                           => "Maximum number of items in Atom feeds",
            "file_max_size_displayed"               => "Maximum size of text files displayed inline",
            "diff_max_lines_displayed"              => "Maximum number of diff lines displayed",
            "repositories_encodings"                => "Attachments and repositories encodings",

            /* display */
            "ui_theme"                              => "Theme",
            "default_language"                      => "Default language",
            "force_default_language_for_anonymous"  => "Force default language for anonymous users",
            "force_default_language_for_loggedin"   => "Force default language for logged-in users",
            "start_of_week"                         => "Start calendars on",
            "date_format"                           => "Date format",
            "time_format"                           => "Time format",
            "user_format"                           => "Users display format",
            "gravatar_enabled"                      => "Use Gravatar user icons",
            "gravatar_default"                      => "Default Gravatar image",
            "thumbnails_enabled"                    => "Display attachment thumbnails",
            "thumbnails_size"                       => "Thumbnails size (in pixels)",

            /* authentification */
            "login_required"                        => "Authentication required",
            "autologin"                             => "Autologin",
            "self_registration"                     => "Self-registration",
            "unsubscribe"                           => "Allow users to delete their own account",
            "password_min_length"                   => "Minimum password length",
            "lost_password"                         => "Lost password",
            "openid"                                => "Allow OpenID login and registration",
            "rest_api_enabled"                      => "Enable REST web service",
            "jsonp_enabled"                         => "Enable JSONP support",
            "session_lifetime"                      => "Session maximum lifetime",
            "session_timeout"                       => "Session inactivity timeout",

            /* projects */
            "default_projects_public"               => "New projects are public by default",
            "default_projects_modules"              => "Default enabled modules for new projects",
            "default_projects_tracker_ids"          => "Default trackers for new projects",
            "sequential_project_identifiers"        => "Generate sequential project identifiers",
            "new_project_user_role_id"              => "Role given to a non-admin user who creates a project",

            /* issues */
            "cross_project_issue_relations"             => "Allow cross-project issue relations",
            "cross_project_subtasks"                    => "Allow cross-project subtasks",
            "issue_group_assignment"                    => "Allow issue assignment to groups",
            "default_issue_start_date_to_creation_date" => "Use current date as start date for new issues",
            "display_subprojects_issues"                => "Display subprojects issues on main projects by default",
            "issue_done_ratio"                          => "Calculate the issue done ratio with",
            "non_working_week_days"                     => "Non-working days",
            "issues_export_limit"                       => "Issues export limit",
            "gantt_items_limit"                         => "Maximum number of items displayed on the gantt chart",
            "issue_list_default_columns"                => "Default columns displayed on the issue list",

            /* notifications */
            "mail_from"                             => "Emission email address",
            "bcc_recipients"                        => "Blind carbon copy recipients (bcc)",
            "plain_text_mail"                       => "Plain text mail (no HTML)",
            "default_notification_option"           => "Default notification option",
            "notified_events"                       => "Select actions for which email notifications should be sent.",
            "emails_header"                         => "Email header",
            "emails_footer"                         => "Email footer",
        ];

    }


    /**
     * Инициализируем атрибуты
     */
    public function initAttributes () {

        // переопределяем значения атрибутов
        $list = $this->getList ();
        foreach ($list as $attribute => $value) {
            $this->$attribute = $value;
        }

        // переопределяем default'ные поля
        $this->setStoredAttributes (array_keys($list));
    }


    /**
     * Добавление
     *
     * @param string
     * @param string
     */
    function addItem ($field, $value)
    {
        $query = new Settings;
        $query->name    = $field;
        $query->value   = $value;
        $query->selfinsert (false);

        return $query->id;
    }


    /**
     * Обновление
     *
     * @param string
     * @param string
     */
    function updateItem ($field, $value)
    {
        static::updateAll([self::Value => $value], [self::Name => $field]);
    }


    /**
     * Обновление
     *
     * @param string
     * @param string
     */
    function saveItem ($field, $value)
    {
        if ($this->getItem ($field) === false) {
            $this->addItem ($field, $value);
        }
        else {
            $this->updateItem ($field, $value);
        }
    }


    /**
     * Получение настройки
     *
     * @param string
     * @return unknown
     */
    function getItem ($key)
    {
        $list = $this->getList (false);
        return isset($list[$key]) ? $list[$key] : false;
    }


    /**
     * Получение списка настроек
     *
     * @param bool
     * @return array
     */
    public static function getList ($rewrite_by_keys = true, $refresh = false)
    {
        static $cache_list = false;

        $cache_key = intval($rewrite_by_keys);
        if (!isset($cache_list[$cache_key]) || $refresh) {

            // получаем список
            $list = parent::getList(['fetchKeyValue' => [
                'key' => static::Name, 'value' => static::Value
            ]]);

            // если нужно перезаписать по ключам
            if ($rewrite_by_keys) {

                // проверяем наличие всех ключей
                foreach (self::$allKeys as $main_key => $keys) {
                    foreach ($keys as $key) {
                        if (!isset($list[$key]) ) {
                            $list[$key] = "";
                        }
                    }
                }

                // спец. обработка для сборного поля
                $list["commit_update_keywords"] = unserialize ($list["commit_update_keywords"]);
            }

            // удаляем из списка те данные которых нет в св-вах класса
            foreach ($list as $key => $value) {
                if (!property_exists(static::object(), $key) ) {
                    unset ($list[$key]);
                }
            }

            // декодируем начальные значения
            static::object()->decodeValues4List ($list);

            $cache_list[$cache_key] = $list;
        }

        return $cache_list[$cache_key];
    }


}
