<?php

namespace app\models;

use yii\db\ActiveQuery;
use app\helpers\ArrayHelper;

/**
 * Класс для работы с ролями
 */
class Role extends Base
{

	const Table			    = "roles";
	
	const Id				= "role_id";
	const Name				= "role_name";
	const Position			= "role_position";
	const Assignable		= "role_assignable";
	const Builtin			= "role_builtin";
	const Permissions		= "role_permissions";
	const IssuesVisibility	= "role_issues_visibility";

    static $fieldPrefix     = "role_";

    public $id;
    public $name;
    public $position;
    public $assignable;
    public $builtin = 0;
    public $permissions;
    public $issues_visibility;


    // ключи для полей доступа
    static $permissionKeys	= [
        'projects'      => ['add_project', 'edit_project', 'close_project', 'select_project_modules', 'manage_members', 'manage_versions', 'add_subprojects'],
        'forums'        => ['manage_boards', 'add_messages', 'edit_messages', 'edit_own_messages', 'delete_messages', 'delete_own_messages'],
        'calendar'      => ['view_calendar'],
        'documents'     => ['add_documents', 'edit_documents', 'delete_documents', 'view_documents'],
        'files'         => ['manage_files', 'view_files'],
        'gantt'         => ['view_gantt'],
        'issuetracking' => ['manage_categories', 'view_issues', 'add_issues', 'edit_issues', 'manage_issue_relations', 'manage_subtasks', 'set_issues_private', 'set_own_issues_private', 'add_issue_notes', 'edit_issue_notes', 'edit_own_issue_notes', 'view_private_notes', 'set_notes_private', 'move_issues', 'delete_issues', 'manage_public_queries', 'save_queries', 'view_issue_watchers', 'add_issue_watchers', 'delete_issue_watchers'],
        'news'          => ['manage_news', 'comment_news'],
        'repository'    => ['manage_repository', 'browse_repository', 'view_changesets', 'commit_access', 'manage_related_issues'],
        'timetracking'  => ['log_time', 'view_time_entries', 'edit_time_entries', 'edit_own_time_entries', 'manage_project_activities'],
        'wiki'          => ['manage_wiki', 'rename_wiki_pages', 'delete_wiki_pages', 'view_wiki_pages', 'export_wiki_pages', 'view_wiki_edits', 'edit_wiki_pages', 'delete_wiki_pages_attachments', 'protect_wiki_pages'],
    ];

    // заголовки ключей блоков прав
    static $permissionTitles = [
        'projects'      => 'Project',
        'forums'        => 'Forums',
        'calendar'      => 'Calendar',
        'documents'     => 'Documents',
        'files'         => 'Files',
        'gantt'         => 'Gantt',
        'issuetracking' => 'Issue tracking',
        'news'          => 'News',
        'repository'    => 'Repository',
        'timetracking'  => 'Time tracking',
        'wiki'          => 'Wiki',
    ];

    static $listValues = [
        'permissions' => [

            // project
            'add_project'               => 'Create project',
            'edit_project'              => 'Edit project',
            'close_project'             => 'Close / reopen the project',
            'select_project_modules'    => 'Select project modules',
            'manage_members'            => 'Manage members',
            'manage_versions'           => 'Manage versions',
            'add_subprojects'           => 'Create subprojects',

            // forums
            'manage_boards'             => 'Manage forums',
            'add_messages'              => 'Post messages',
            'edit_messages'             => 'Edit messages',
            'edit_own_messages'         => 'Edit own messages',
            'delete_messages'           => 'Delete messages',
            'delete_own_messages'       => 'Delete own messages',

            // calendar
            'view_calendar'             => 'View calendar',

            // documents
            'add_documents'             => 'Add documents',
            'edit_documents'            => 'Edit documents',
            'delete_documents'          => 'Delete documents',
            'view_documents'            => 'View documents',

            // files
            'manage_files'              => 'Manage files',
            'view_files'                => 'View files',

            // gantt
            'view_gantt'                => 'View gantt chart',

            // issue tracking
            'manage_categories'         => 'Manage issue categories',
            'view_issues'               => 'View Issues',
            'add_issues'                => 'Add issues',
            'edit_issues'               => 'Edit issues',
            'manage_issue_relations'    => 'Manage issue relations',
            'manage_subtasks'           => 'Manage subtasks',
            'set_issues_private'        => 'Set issues public or private',
            'set_own_issues_private'    => 'Set own issues public or private',
            'add_issue_notes'           => 'Add notes',
            'edit_issue_notes'          => 'Edit notes',
            'edit_own_issue_notes'      => 'Edit own notes',
            'view_private_notes'        => 'View private notes',
            'set_notes_private'         => 'Set notes as private',
            'move_issues'               => 'Move issues',
            'delete_issues'             => 'Delete issues',
            'manage_public_queries'     => 'Manage public queries',
            'save_queries'              => 'Save queries',
            'view_issue_watchers'       => 'View watchers list',
            'add_issue_watchers'        => 'Add watchers',
            'delete_issue_watchers'     => 'Delete watchers',

            // news
            'manage_news'               => 'Manage news',
            'comment_news'              => 'Comment news',

            // repository
            'manage_repository'         => 'Manage repository',
            'browse_repository'         => 'Browse repository',
            'view_changesets'           => 'View changesets',
            'commit_access'             => 'Commit access',
            'manage_related_issues'     => 'Manage related issues',

            // time tracking
            'log_time'                  => 'Log spent time',
            'view_time_entries'         => 'View spent time',
            'edit_time_entries'         => 'Edit time logs',
            'edit_own_time_entries'     => 'Edit own time logs',
            'manage_project_activities' => 'Manage project activities',

            // wiki
            'manage_wiki'                   => 'Manage wiki',
            'rename_wiki_pages'             => 'Rename wiki pages',
            'delete_wiki_pages'             => 'Delete wiki pages',
            'view_wiki_pages'               => 'View wiki',
            'export_wiki_pages'             => 'Export wiki pages',
            'view_wiki_edits'               => 'View wiki history',
            'edit_wiki_pages'               => 'Edit wiki pages',
            'delete_wiki_pages_attachments' => 'Delete attachments',
            'protect_wiki_pages'            => 'Protect wiki pages',
        ],
        'issues_visibility' => [
            'all'       => 'All issues',
            'default'   => 'All non private issues',
            'own'       => 'Issues created by or assigned to the user',
        ],
    ];


    /**
     * Правила
     *
     * @return array
     */
    public function rules () {
        return [
            ["name", "string"],
            ["assignable", "boolean"],
            ["issues_visibility", "in", "range" => $this->getListValues('issues_visibility', true)],
            ["permissions", "mv_string", "range" => $this->getListValues('permissions', true), 'itemPrefix' => ':'],
            [['name'], 'required'],
        ];
    }


    /**
     * Получаем label
     *
     * @return array
     */
    public function attributeLabels ()
    {
        return [
            "name"                  => "Name",
            "assignable"            => "Issues can be assigned to this role",
            "issues_visibility"     => "Issues visibility",
            "permissions"           => "Permissions",
        ];
    }


    /**
     * Members relations
     *
     * @return static
     */
    public function getMembers () {
        return $this->hasMany(Member::className(), [Member::Id => MemberRole::MemberId])
            ->viaTable(MemberRole::Table, [MemberRole::RoleId => Role::Id]);
    }


    /**
     * Получаем ключи по группе
     */
    public function getPermissionsByGroup ($group) {
        if (!isset(static::$permissionKeys[$group]) ) {
            return [];
        }
        $list = static::$permissionKeys[$group];

        $listValues = $this->getListValues('permissions');

        $result = [];
        foreach ($list as $key) {
            $result[$key] = $listValues[$key];
        }
        return $result;
    }

	
	/**
	 * Получаем список
	 *
	 * @param array
	 * @param array
	 * @return array
	 */
    public static function getList ($params = []) {
        $params['orderBy'] = ArrayHelper::remove($params, 'orderBy', self::Builtin .",".self::Position);
        return parent::getList ($params);
	}


    /**
     * Получаем список встроенных типов
     *
     * @param array
     * @return array
     */
    public static function getBuiltinList ($list) {
        return static::getList ([
            'where'         => [Role::Builtin .' != 0'],
            'fetchValue'    => Role::Id
        ]);
    }


    /**
     * Where для сортировки
     *
     * @return string
     */
    public static function positionListWhere () {
        return self::Builtin .' = 0';
    }
	
}

?>