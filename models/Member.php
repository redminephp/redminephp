<?php

namespace app\models;

use yii\db\ActiveQuery;

use app\helpers\ArrayHelper;

use app\models\Project;
use app\models\Member\Role as MemberRole;

/**
 * Класс для работы с members
 */
class Member extends \app\models\Base
{
    const Table			    = "members";

    const Id				= "member_id";
    const UserId			= "member_user_id";
    const ProjectId			= "member_project_id";
    const CreatedOn			= "member_created_on";
    const MailNotification	= "member_mail_notification";

    static $fieldPrefix = "member_";

    // Fields
    public $id;
    public $user_id;
    public $project_id;
    public $created_on;
    public $mail_notification = 0;

    // Project
    public $project;

    // Roles
    public $roles = [];

    static $listValues = [
        'project_id'    => [],
        'roles'         => [],
    ];


    /**
     * Init
     */
    public function init () {

        parent::init ();

        // после инициализации элемента
        $this->on(self::EVENT_AFTER_ITEM_INIT, function ($event) {
            if ($this->id) {

                // Init projects
                $this->project = Project::getById($this->project_id);

                // Init roles
                /*$this->roles = MemberRole::getList([
                    'memberId' => $this->id,
                    'fetchNewKeys' => [
                        Role::Id    => 'id',
                        Role::Name  => 'name',
                    ]
                ]);*/

            }
            $this->off (static::EVENT_AFTER_ITEM_INIT);
        });

        // All project list
        if (!static::$listValues['project_id']) {
            static::$listValues['project_id'] = Project::getList(['fetchKeyValue' => true]);
        }

        // All roles list
        if (!static::$listValues['roles']) {
            static::$listValues['roles'] = Role::getList(['fetchKeyValue' => true]);
        }
    }



    /**
     * Rules
     *
     * @return array the validation rules.
     */
    public function rules()
    {
        $this->trigger(self::EVENT_AFTER_ITEM_INIT);

        return [
            ['project_id',  'in', 'range' => $this->getListValues('project_id', true)],
            ['roles',       'in', 'range' => $this->getListValuesDiff2Saved('roles'), 'multivalue' => true],
        ];
    }


    /**
     * Roles relations
     */
    public function getRoles () {
        return $this->hasMany(Role::className(), [Role::Id => MemberRole::RoleId])
            ->viaTable(MemberRole::Table, [MemberRole::MemberId => Member::Id]);
    }


    /**
     * Project relation
     */
    public function getProject() {
        return $this->hasOne(Project::className(), [Project::Id => Member::ProjectId]);
    }



    /**
     * Получение списка
     *
     * @param array $params
     * @return array|void
     */
    public static function getList ($params = []) {

        // Base query with roles & project
        $params['query'] = static::find()
            ->with('roles')
            ->with('project');

        return parent::getList($params);
    }


    /**
     * Добавляем запись
     *
     * @param int
     * @param int
     */
    public static function addItem ($userId, $projectId) {
        /*if (static::getItem($userId, $projectId) ) {
            return false;
        }*/

        $query = new static;
        $query->setAttributes ([
            self::UserId	=> $userId,
            self::ProjectId	=> $projectId,
            self::CreatedOn => date('Y-m-d H:i:s'),
        ], false);
        return $query->insert (false);
    }


    /**
     * Get item
     *
     * @param $userId
     * @param $projectId
     */
    public static function getItem ($userId, $projectId) {
        return static::find()
            ->addWhere ([static::UserId, $userId])
            ->addWhere ([static::ProjectId, $projectId]);
    }


    /**
     * Удаляем запись
     *
     * @param int
     * @param int
     */
    public static function deleteItem ($userId, $projectId) {
        static::deleteAll([
            self::UserId	=> $userId,
            self::ProjectId	=> $projectId,
        ]);
    }

}