<?php

namespace app\models;

use yii\db\ActiveQuery;

/**
 * Класс для работы с задачами
 */
class Issue extends \app\models\Base
{

    const Table			= "issues";

    const Id				= "issue_id";
    const TrackerId			= "issue_tracker_id";
    const ProjectId			= "issue_project_id";
    const Subject			= "issue_subject";
    const Description		= "issue_description";
    const DueDate			= "issue_due_date";
    const CategoryId		= "issue_category_id";
    const StatusId			= "issue_status_id";
    const AssignedToId		= "issue_assigned_to_id";
    const PriorityId		= "issue_priority_id";
    const FixedVersionId	= "issue_fixed_version_id";
    const AuthorId			= "issue_author_id";
    const LockVersion		= "issue_lock_version";
    const CreatedOn			= "issue_created_on";
    const UpdatedOn			= "issue_updated_on";
    const StartDate			= "issue_start_date";
    const DoneRatio			= "issue_done_ratio";
    const EstimatedHours	= "issue_estimated_hours";
    const ParentId			= "issue_parent_id";
    const RootId			= "issue_root_id";
    const Left				= "issue_lft";
    const Right				= "issue_rgt";
    const IsPrivate			= "issue_is_private";
    const DeliverableId		= "issue_deliverable_id";
    const ClosedOn			= "issue_closed_on";


}


?>