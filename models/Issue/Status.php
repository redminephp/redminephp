<?php

namespace app\models\Issue;

use yii\db\ActiveQuery;
use app\helpers\ArrayHelper;


/**
 * Класс для работы со статусами задач
 */
class Status extends \app\models\Base
{
    const Table = "issue_statuses";

    const Id				= "issue_status_id";
    const Name				= "issue_status_name";
    const IsClosed			= "issue_status_is_closed";
    const IsDefault			= "issue_status_is_default";
    const Position			= "issue_status_position";
    const DefaultDoneRatio	= "issue_status_default_done_ratio";

    static $fieldPrefix     = "issue_status_";

    public $id;
    public $name;
    public $is_closed;
    public $is_default;
    public $position;
    public $default_done_ratio;


    /**
     * Инициализация
     */
    public function init () {

        // устанавливаем обработчик события сохранения элемента
        $this->on(self::EVENT_BEFORE_ITEM_SAVE, function ($event) {

            // если элемент становится default'ным - сбрасываем все флаги is_default для остальных записей
            if ($this->is_default) {
                static::updateAll([self::IsDefault => 0]);
            }
        });

    }


    /**
     * Правила
     *
     * @return array
     */
    public function rules () {
        return [
            ["name", "string"],
            ["is_closed", "boolean"],
            ["is_default", "boolean"],
            [['name'], 'required'],
        ];
    }


    /**
     * Получаем label
     *
     * @return array
     */
    public function attributeLabels ()
    {
        return [
            "name"          => "Name",
            "is_closed"     => "Issue closed",
            "is_default"    => "Default value",
        ];
    }


    /**
     * Получаем список
     *
     * @param array
     * @param array
     * @return array
     */
    public static function getList ($params = []) {
        $params['orderBy'] = ArrayHelper::remove($params, 'orderBy', self::Position);
        return parent::getList ($params);
    }

}

?>