<?php

namespace app\models;

use yii\db\ActiveQuery;
use app\helpers\ArrayHelper;

use app\models\Project\Tracker as ProjectTracker;


/**
 * Класс для работы с трекерами
 */
class Tracker extends Base
{

    const Table = "trackers";

    const Id			= "tracker_id";
    const Name			= "tracker_name";
    const IsInChLog		= "tracker_is_in_chlog";
    const Position		= "tracker_position";
    const IsOnRoadmap	= "tracker_is_in_roadmap";
    const FieldsBits	= "tracker_fields_bits";

    static $fieldPrefix = "tracker_";

    public $id;
    public $name;
    public $is_in_chlog     = 0;
    public $position;
    public $is_in_roadmap   = 0;
    public $fields_bits     = 0;
    public $projects        = [];


    // списковые значения
    static $listValues = [
        'fields_bits' => [
            'assigned_to_id'    => 'Assignee',
            'category_id'       => 'Category',
            'fixed_version_id'  => 'Target version',
            'parent_issue_id'   => 'Parent task',
            'start_date'        => 'Start date',
            'due_date'          => 'Due date',
            'estimated_hours'   => 'Estimated time',
            'done_ratio'        => '% Done',
        ],
        'projectsCascade' => [],
    ];



    /**
     * Ф-ция инициализации
     */
    public function init () {
        parent::init ();

        $projects = Project::getList([
            'fields' => [Project::Id, Project::Name, Project::ParentId],
        ]);
        static::$listValues['projects'] = ArrayHelper::Array2KeyValueArray($projects, Project::Id, Project::Name);

        // получаем проекты каскадно
        ArrayHelper::a_fetchSubitems(static::$listValues['projectsCascade'], $projects, Project::Id, Project::ParentId, 0);

        // после инициализации элемента
        $this->on(self::EVENT_AFTER_ITEM_INIT, function ($event) {
            if ($this->id) {
                $this->projects = ProjectTracker::getList([ProjectTracker::TrackerId => $this->id, 'fetchValue' => ProjectTracker::ProjectId]);
            }
        });

        // устанавливаем обработчик события после сохранения элемента
        $this->on(self::EVENT_AFTER_ITEM_SAVE, function ($event) {

            // старые проекты
            $old_projects = $this->getStoredAttributes(['projects']);
            $old_projects = $old_projects['projects'];

            // обновляем записи
            ProjectTracker::changeItems ($this->id, $old_projects, $this->projects);
        });
    }



    /**
     * Правила
     *
     * @return array
     */
    public function rules () {
        return [
            ["name", "string"],
            ["is_in_roadmap", "boolean"],
            ["fields_bits", "bits", "isInvert" => true, "range" => ArrayHelper::getBitValueArray(
                $this->getListValues('fields_bits', true)
            )],
            ["projects", 'in', 'range' => $this->getListValues('projects', true), 'multivalue' => true],
            [['name'], 'required'],
        ];
    }


    /**
     * Получаем label
     *
     * @return array
     */
    public function attributeLabels ()
    {
        return [
            "name"                  => "Name",
            "is_in_roadmap"         => "Issues displayed in roadmap",
            "issues_visibility"     => "Issues visibility",
            "fields_bits"           => "Standard fields",
            "projects"              => "Projects",
        ];
    }


    /**
     * Получаем список
     *
     * @param array
     * @param array
     * @return array
     */
    public static function getList ($params = []) {
        $params['orderBy'] = ArrayHelper::remove($params, 'orderBy', self::Position);
        return parent::getList ($params);
    }

}