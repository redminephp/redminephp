<?php

namespace app\models\user;

use app\models\User;

/**
 * Class for user tokens
 */
class UserToken extends \app\models\BaseArrayable
{
	const Table = "tokens";
	
	const Id			= "token_id";
	const UserId		= "token_user_id";
	const Action		= "token_action";
	const Value			= "token_value";
	const DateCreated	= "token_created_on";
		
	const ActionAutologin	= "autologin";
	const ActionApi			= "api";
	const ActionFeeds		= "feeds";

	
	
	/**
	 * Add
	 *
	 * @param string
	 * @param array
	 * @return string
	 */
	public static function addItem ($action, $user)
	{
		$token = self::generateToken ($user);
		
		$data = array (
			self::Action 		=> $action,
			self::Value			=> $token,
			self::UserId		=> $user->get('id'),
			self::DateCreated	=> date ("Y-m-d H:i:s"),
		);
		
		// add new record
		$query = new UserToken;
		$query->setAttributes ($data, false);
		$query->insert (true);

		return $token;
	}
	
	
	/**
	 * Get
	 *
	 * @param string
	 * @param string
	 * @param bool
	 */
    public static function getLastItem ($action, $user_id)
	{
		$query = UserToken::find();
		
		$query->where ([
			self::Action	=> $action,
			self::UserId	=> $user_id,
		]);
		
		$query->orderBy ([self::Id => SORT_DESC]);
		
		$result = $query->one();
		return $result;
	}
	
	
	/**
	 * Generate token
	 *
	 * @param array
	 * @return string
	 */
    public static function generateToken ($user)
	{
		return sha1(uniqid($user->get('salt')+time()));
	}
}