<?php

namespace app\models\user;

use app\models\User;

class UserAuth extends \yii\base\Object implements \yii\web\IdentityInterface
{
	
	// user info
	private $_user;

    public $token = '';
	
	
	/**
	 * Constructor
	 *
	 * @param array
	 */
	public function __construct ($user = false, $token = false)
	{
		parent::__construct ();
		
		$this->_user    = $user;
        $this->token    = $token;
	}
	
	
	/**
	 * Get user property by key
	 *
	 * @param string
	 * @return unknown
	 */
	public function get ($key)
	{
		return isset($this->_user->$key) ? $this->_user->$key : null;
	}


    /**
     * Set user proterty by key-value
     *
     * @param string
     * @param unknown
     */
    public function set ($key, $value)
    {
        $this->_user->$key = $value;
    }

	
	/**
	 * Find identity by user_id
	 *
	 * @param int
	 * @return unknown
	 */
	public static function findIdentity ($user_id)
	{
		// get user info
        $user = User::getById ($user_id);
		if (!$user) {
			return null;
		}
		
		// get last token
		$token_info = UserToken::getLastItem (UserToken::ActionAutologin, $user_id);
		if (!$token_info) {
			return null;
		}

		return new static($user, $token_info[UserToken::Value]);
	}
	
	
	
	/**
	 * Find user by access token (disabled)
     *
     * @param string
     * @return bool
	 */
	public static function findIdentityByAccessToken($token)
	{
		return false;
	}


    /**
     * Get user values by Identity
     *
     * @param string
     * @return string
     */
    public static function info ($key)
    {
        return \Yii::$app->user->getIdentity()->get($key);
    }

	
	/**
	 * Get user id
	 *
	 * @return int
	 */
	public function getId()
	{
		return $this->get('id');
	}

	
	/**
	 * Get auth key
	 *
	 * @return string
	 */
	public function getAuthKey()
	{
		$salt	= $this->get ('salt');
		return sha1(json_encode($this->get('id')).$this->token.$salt);
	}


	/**
	 * Validate auth key
	 *
	 * @param string
	 * @return bool
	 */
	public function validateAuthKey($authKey)
	{
		return ($this->getAuthKey() === $authKey);
	}


	/**
	 * Validate password
	 *
	 * @param string
	 * @return bool
	 */
	public function validatePassword($password)
    {
        $password_info = User::getPassword ($password, $this->get('salt'));
		return $this->get('password') === $password_info["password"];
	}
	
	
	/**
	 * Is login
	 *
	 * @return bool
 	 */
    public function isLogin () {
		return $this->getId();
	}
	
	
}