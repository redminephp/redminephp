<?php

namespace app\models;

use yii\db\ActiveQuery;

use app\models\user\UserAuth;
use app\models\user\UserToken;


class User extends Base
{
	const Table			= "users";
	
	const Id			= "user_id";
	const Login			= "user_login";
	const Password		= "user_hashed_password";
	const FirstName		= "user_firstname";
	const LastName		= "user_lastname";
	const Email			= "user_mail";
	const IsAdmin		= "user_admin";
	const Status		= "user_status";
	const LastLogon		= "user_last_login_on";
	const Language		= "user_language";
	const AuthSourceId	= "user_auth_source_id";	
	const DateCreated	= "user_created_on";
	const DateUpdated	= "user_updated_on";	
	const Type			= "user_type";	
	const IdentifyUrl	= "user_identity_url";	
	const EmailNotify	= "user_mail_notification";	
	const Salt			= "user_salt";

    static $fieldPrefix = "user_";

    public $id;
    public $username    = '';
    public $password    = '';
    public $firstname   = '';
    public $lastname    = '';
    public $email       = '';
    public $is_admin    = 0;
    public $status      = 1;
    public $lastlogon;
    public $language    = '';
    public $auth_source_id;
    public $created_on;
    public $updated_on;
    public $type;
    public $identity_url;
    public $email_notify = '';
    public $salt;

    private $_user = false;

    // значение поля type
    static $typeValue = 'User';


    /**
     * Поля при распеделении массива как ключ => значение
     *
     * @return array
     */
    public static function fetchFields () {
        return ['key' => static::Id, 'value' => static::Login];
    }


    /**
     * Инициализация
     */
    function init () {
        $this->type         = static::$typeValue;
        $this->created_on   = date ('Y-m-d H:i:s');
        $this->updated_on   = date ('Y-m-d H:i:s');
    }


    /**
     * Алиасы атрибутов для связи с полями
     *
     * @return array
     */
    public static function attributeAliasList () {
        return [
            static::Login       => 'username',
            static::Password    => 'password',
            static::Email       => 'email',
            static::IsAdmin     => 'is_admin',
            static::LastLogon   => 'lastlogon',
            static::EmailNotify => 'email_notify',
        ];
    }


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['username', 'password'], 'required'],

            // password is validated by validatePassword()
            ['password', 'validatePassword'],
        ];
    }



    /**
     * Получаем label
     *
     * @return array
     */
    public function attributeLabels ()
    {
        return [
            'username'      => 'Login:',
            'password'      => 'Password:',
        ];
    }


    /**
     * Get user full name from array
     *
     * @param array
     * @return string
     */
    public static function fullName ($item) {
        return $item[static::FirstName] .' '. $item[static::LastName];
    }


    /**
     * Получение списка
     *
     * @param array
     * @param array
     * @return array|void
     */
    public static function getList ($params = []) {
        $params[static::Type] = static::$typeValue;
        return parent::getList ($params);
    }
	
	
	/**
	 * Get by login
	 *
	 * @param string
	 * @return array
	 */
    public static function getByLogin ($login) {
        $query = static::find();

        $query->where ([static::Login => $login]);

        return $query->oneDecode();
	}

	
	
	/**
	 * Prepare password
	 *
	 * @param string
	 * @param string
	 */
    public static function getPassword ($password, $salt = false)
	{
		// init salt if not exists
		if (!$salt) {
			$salt = sha1(uniqid(time()));
		}
		
		// generate password
		$password = sha1($salt . sha1($password));
		
		return array("password" => $password, "salt" => $salt);
	}



    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     */
    public function validatePassword()
    {
        $user = $this->_getUser();
        if (!$user || !$user->validatePassword($this->password)) {
            $this->addError('password', 'Incorrect username or password.');
        }
    }


    /**
     * Logs in a user using the provided username and password.
     * @return boolean whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate(['username', 'password'])) {

            $user = $this->_getUser();

            // generate session key and attach token to user info
            $token = UserToken::addItem (UserToken::ActionAutologin, $user);
            $user->token = $token;

            return \Yii::$app->user->login($this->_getUser(), 3600*24*30);
        } else {
            return false;
        }
    }


    /**
     * Finds user by [[login]]
     *
     * @return User|null
     */
    private function _getUser()
    {
        if (!$this->username) {
            return false;
        }
        if ($this->_user === false) {
            $user           = User::getByLogin ($this->username);
            $this->_user    = new UserAuth ($user);
        }
        return $this->_user;
    }
	

}
