<?php

namespace app\models;

use yii\db\ActiveQuery;
use app\helpers\ArrayHelper;

use app\models\Group\User as GroupUser;


/**
 * Класс для работы с трекерами
 */
class Group extends User
{

    // значение поля type
    static $typeValue = 'Group';

    // tab
    public $tab = 'general';

    // название
    public $name;

    // Users
    public $users = [];

    // Members, array like [project_id1 => [role_id1, role_id2, role_id3, ..], project_id2 => [role_id1, role_id2, ..]]
    public $members = [];

    // tabs
    static $tabs = [
        "general"       => ["title" => "General"],
        "users"         => ["title" => "Users"],
        "memberships"   => ["title" => "Projects"],
    ];


    // списковые значения для валидации
    static $listValues = [
        "users" => [],
    ];


    /**
     * Инициализация
     */
    public function init () {

        parent::init ();

        // после инициализации элемента
        $this->on(self::EVENT_AFTER_ITEM_INIT, function ($event) {
            if ($this->id) {

                // Init users
                $this->users = GroupUser::getList([
                    'groupId' => $this->id,
                    'fetchNewKeys' => [
                        User::Id            => 'id',
                        User::FirstName     => 'firstname',
                        User::LastName      => 'lastname',
                        GroupUser::GroupId  => 'group_id',
                    ]
                ]);
                foreach ($this->users as $key => $value) {
                    $this->users[$key]['name'] = $value['firstname'] .' '. $value['lastname'];
                }


                // Init members
                $this->members = Member::getList([
                    Member::UserId => $this->id,
                ]);

                /*$list = Member::find()
                    ->select('*')
                    ->with('roles')
                    ->with('project')
                    ->andWhere([Member::UserId => $this->id])
                    ->all();

                print_v($list); die ();*/
            }
            $this->off (static::EVENT_AFTER_ITEM_INIT);
        });

        // весь список пользователей
        if (!static::$listValues['users']) {
            static::$listValues['users'] = User::getList(['fetchKeyValue' => true]);
        }
    }


    /**
     * Алиасы атрибутов для связи с полями
     *
     * @return array
     */
    public static function attributeAliasList () {
        return array_merge(parent::attributeAliasList(), [
            static::LastName => 'name',
        ]);
    }


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        $this->trigger(self::EVENT_AFTER_ITEM_INIT);

        return [
            ['name', 'required'],
            ['name', 'string'],
            ['name', 'unique', 'targetAttribute' => static::LastName, 'message' => 'Name has already been taken'],
            ['users', 'in', 'range' => $this->getListValuesDiff2Saved('users'), 'multivalue' => true, 'on' => 'add'],
            ['users', 'in', 'range' => ArrayHelper::ArrayValues($this->users, 'id'), 'multivalue' => true, 'on' => 'delete'],
        ];
    }



    // Name has already been taken

    /**
     * Получаем label
     *
     * @return array
     */
    public function attributeLabels ()
    {
        return [
            'name'      => 'Name',
        ];
    }

}