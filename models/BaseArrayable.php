<?php

namespace app\models;

use yii\base\Exception;
use yii\db\ActiveQuery;

use app\helpers\ArrayHelper;

class BaseArrayable extends Base
{

    // результатом получения одной записи будет массив
    static $isArrayable = true;

}