// JavaScript Document
var Core = {
	CheckRegexp : function (regexp, value) {
		var re = new RegExp(regexp);
		return value && re.test(value);
	}
}

function extend (Child, Parent) {
	var F = function() { }
	F.prototype = Parent.prototype
	Child.prototype = new F()
	Child.prototype.constructor = Child
	Child.superclass = Parent.prototype
}


function setCookie (name, value, time_in_hours, path, domain, secure) 
{
	expires = false;
	if (time_in_hours != -1)
	{
		if (!time_in_hours)	time_in_hours = 5;
		
		var d = new Date();
		d.setHours(d.getHours() + time_in_hours);
		expires = d.toGMTString()
	}
	
	if (!path)	path = "/";
	
	
	document.cookie = name + "=" + escape(value) +
		((expires) ? "; expires=" + expires : "") +
		((path) ? "; path=" + path : "") +
		((domain) ? "; domain=" + domain : "") +
		((secure) ? "; secure" : "");
}


function getCookie(name) 
{
	var cookie = " " + document.cookie;
	var search = " " + name + "=";
	var setStr = null;
	var offset = 0;
	var end = 0;
	
	if (cookie.length > 0) 
	{
		offset = cookie.indexOf(search);
		if (offset != -1) 
		{
			offset += search.length;
			end = cookie.indexOf(";", offset)
			if (end == -1) 
			{
				end = cookie.length;
			}
			setStr = unescape(cookie.substring(offset, end));
		}
	}
	
	return(setStr);
}

function stopEvent (e)
{
	if (!e && window.event) {
		e = event;
	}
	if (!e) {
		return;
	}
	if (e.stopPropagation) {
		e.stopPropagation();
	}
	else {
		e.cancelBubble = true;
	}
	if(e.preventDefault) {
		e.preventDefault();
	}
	else {
		e.returnValue = false;
	}
}


/**
 * Инициализируем placeholder
 */
function InitPlaceholder (submit_callback)
{
	$('input[title], textarea[title]').blur(function(){ 
		if ($(this).val() == '') {
			$(this).val($(this).attr('title'));
			$(this).addClass('m-placeholder');
		}
	
	}).focus(function(){
		$(this).removeClass('m-placeholder');
		if ($(this).val() == $(this).attr('title') ) {
			$(this).val('');
		}
		
	}).each(function(){
		
		if ($(this).val() == '' || $(this).val() == $(this).attr('title') ) {
			$(this).val( $(this).attr('title') );
			$(this).addClass('m-placeholder');
		}
	
	});
}