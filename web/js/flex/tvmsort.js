// JavaScript Document
var tvmSort = {
	
	Sort : function (p, type) {
		
		var root = $(p).closest ("table");
		var item = $(p).closest (".sort-item");
		
		var success = false;
		var content = item.html();
		switch (type) {
			case "movetotop":
				//root.find(".sort-item").first()
				break;
			case "movetobottom":
				break;
			case "moveup":
				if (item.prev().hasClass("sort-item") ) {
					item.prev().before("<tr class=\"sort-item\">"+content+"</tr>");
					success = true;
				}
				break;
			case "movedown":
				if (item.next().hasClass("sort-item") ) {
					item.next().after("<tr class=\"sort-item\">"+content+"</tr>");
					success = true;
				}
				break;
		}
		
		if (success) {
			item.remove ();
			
			var index = 0;
			root.find(".sort-item").each(function(){
				$(this).removeClass("odd").removeClass("even");
				if (index % 2 == 0) {
					$(this).addClass ("odd");
				}
				else {
					$(this).addClass ("even");
				}
				index++;
			});
		}
	}
	
}