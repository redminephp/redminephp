<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace yii\validators;

use Yii;

/**
 * StringValidator validates that the attribute value is of certain length.
 *
 * Note, this validator should only be used with string-typed attributes.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class MultivalueStringValidator extends SeparatedStringValidator
{
    /**
     * @var integer|array specifies the length limit of the value to be validated.
     * This can be specified in one of the following forms:
     *
     * - an integer: the exact length that the value should be of;
     * - an array of one element: the minimum length that the value should be of. For example, `[8]`.
     *   This will overwrite [[min]].
     * - an array of two elements: the minimum and maximum lengths that the value should be of.
     *   For example, `[8, 128]`. This will overwrite both [[min]] and [[max]].
     */
    public $length;

    /**
     * @var string user-defined error message used when the value is not a string
     */
    public $message;
    /**
     * @var string user-defined error message used when the length of the value is not equal to [[length]].
     */
    public $notEqual;
    /**
     * @var string the encoding of the string value to be validated (e.g. 'UTF-8').
     * If this property is not set, [[\yii\base\Application::charset]] will be used.
     */
    public $encoding;

    /**
     * @var array - элементы которые могут входить в separated string
     */
    public $range;

    /**
     * @var string - разделитель
     */
    public $separator = "\n- ";

    /**
     * @var string - ф-ция для преобразования элементов separated string
     */
    public $fn = "strval";


    /**
     * @var string - префикс: первый элемент
     */
    public $prefix  = '---';


    /**
     * @var string - постфикс
     */
    public $postfix = "\n";


    /**
     * Декодируем (пока так)
     *
     * @param string
     */
    public function decode ($value) {
        $value = explode ($this->separator, $value);
        array_shift($value);
        $value[count($value)-1] = str_replace($this->postfix, "", end($value));
        str_replace (array("\"", "'"), array("", ""), $value);

        // удаляем кавычки у значений (т.к. для числовых данных используются кавычки)
        $value = array_map ([$this, 'removeQuotes'], $value);

        // если есть префикс у элементов - убираем его
        $this->removeItemPrefix($value);

        return $value;
    }


    /**
     * Удаление кавычек
     *
     * @param string
     * @return string
     */
    public function removeQuotes ($value) {
        return strtr($value, [
            "\""    => "",
            "'"     => "",
        ]);
    }


    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        if ($this->encoding === null) {
            $this->encoding = Yii::$app->charset;
        }
        if ($this->message === null) {
            $this->message = Yii::t('yii', '{attribute} must be a string.');
        }
    }

    /**
     * @inheritdoc
     */
    public function validateAttribute($object, $attribute)
    {
        $value = $object->$attribute;

        // проверяем список на валидность
        $value = $this->validateList ($value);

        // добавляем префик в начало
        array_unshift($value, $this->prefix);

        // пребразуем обратно в строку
        $value = implode ($this->separator, $value);

        // добавляем постфикс
        $value .= $this->postfix;

        // переопределяем атрибут
        $object->$attribute = $value;
    }

    /**
     * @inheritdoc
     */
    protected function validateValue($value)
    {
        // тут пока нет никаких проверок
        return null;
    }

    /**
     * @inheritdoc
     */
    public function clientValidateAttribute($object, $attribute, $view)
    {

    }
}
