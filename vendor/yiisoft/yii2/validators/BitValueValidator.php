<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace yii\validators;

use Yii;
use app\helpers\ArrayHelper;

/**
 * StringValidator validates that the attribute value is of certain length.
 *
 * Note, this validator should only be used with string-typed attributes.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class BitValueValidator extends RangeValidator
{


    // если сразу несколько значений
    public $multivalue  = true;

    // инвертируемое значение бита
    public $isInvert    = false;

    public $skipOnEmpty = false;


    /**
     * Декодируем
     *
     * @param string
     */
    public function decode ($value) {

        // получаем значения по битовому значению
        $result = [];
        foreach ($this->range as $bit => $name) {
            $has_bit = ($value & $bit);
            if ($has_bit && !$this->isInvert || !$has_bit && $this->isInvert) {
                $result[] = $name;
            }
        }

        return $result;
    }



    /**
     * @inheritdoc
     */
    public function validateAttribute ($object, $attribute)
    {
        // обрабатываем список
        $result = [];
        foreach ($object->$attribute as $key => $value) {
            if ($this->validateItem ($value) ) {
                $result[] = $value;
            }
        }
        $object->$attribute = $result;

        // биты
        $bits = 0;
        foreach ($object->$attribute as $key => $name) {
            $bits += array_search($name, $this->range);
        }

        // если инвертация
        if ($this->isInvert) {
            $count = 0;
            for ($i=0; $i<count($this->range); $i++) {
                $count += pow(2, $i);
            }
            $bits = $count-$bits;
        }

        $object->$attribute = $bits;
    }


    /**
     * @inheritdoc
     */
    protected function validateValue($value)
    {
        // тут пока нет никаких проверок
        return null;
    }

    /**
     * @inheritdoc
     */
    public function clientValidateAttribute($object, $attribute, $view)
    {

    }
}
