<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace yii\validators;

use Yii;
use app\helpers\ArrayHelper;

/**
 * StringValidator validates that the attribute value is of certain length.
 *
 * Note, this validator should only be used with string-typed attributes.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class SeparatedStringValidator extends Validator
{
    /**
     * @var integer|array specifies the length limit of the value to be validated.
     * This can be specified in one of the following forms:
     *
     * - an integer: the exact length that the value should be of;
     * - an array of one element: the minimum length that the value should be of. For example, `[8]`.
     *   This will overwrite [[min]].
     * - an array of two elements: the minimum and maximum lengths that the value should be of.
     *   For example, `[8, 128]`. This will overwrite both [[min]] and [[max]].
     */
    public $length;

    /**
     * @var string user-defined error message used when the value is not a string
     */
    public $message;
    /**
     * @var string user-defined error message used when the length of the value is not equal to [[length]].
     */
    public $notEqual;
    /**
     * @var string the encoding of the string value to be validated (e.g. 'UTF-8').
     * If this property is not set, [[\yii\base\Application::charset]] will be used.
     */
    public $encoding;

    /**
     * @var array - элементы которые могут входить в separated string
     */
    public $range;

    /**
     * @var string - разделитель
     */
    public $separator = ",";

    /**
     * @var string - ф-ция для преобразования элементов separated string
     */
    public $fn = "intval";


    // префикс для элемента
    public $itemPrefix = "";


    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        if ($this->encoding === null) {
            $this->encoding = Yii::$app->charset;
        }
        if ($this->message === null) {
            $this->message = Yii::t('yii', '{attribute} must be a string.');
        }
    }


    /**
     * Декодируем (пока так)
     *
     * @param string
     */
    public function decode ($value) {
        if (!$this->itemPrefix) {
            return $value;
        }

        // преобразуем в массив
        $value = explode ($this->separator, $value);

        // если есть префикс у элементов - убираем его
        $this->removeItemPrefix($value);

        // возвращаем в исходном виде
        return implode($this->separator, $value);
    }


    /**
     * Удаляем префикс у элементов
     *
     * @param link
     */
    public function removeItemPrefix (&$list) {
        if (!$this->itemPrefix) {
            return;
        }

        // если есть префикс у элементов - убираем его
        foreach ($list as $key => $val) {
            if (stripos($val, $this->itemPrefix) !== false) {
                $list[$key] = substr($val, strlen($this->itemPrefix));
            }
        }
    }


    /**
     * Добавление префикса к элементам
     *
     * @param link
     */
    public function addItemPrefix (&$list) {
        if (!$this->itemPrefix) {
            return;
        }
        foreach ($list as $key => $val) {
            $list[$key] = $this->itemPrefix.$val;
        }
    }


    /**
     * @inheritdoc
     */
    public function validateAttribute($object, $attribute)
    {
        $value = $object->$attribute;

        // преобразуем в массив
        $value = explode ($this->separator, $value);

        // проверяем список на валидность
        $value = $this->validateList ($value);

        // возвращаем в прежний формат
        $value = implode($this->separator, $value);

        // переопределяем атрибут
        $object->$attribute = $value;
    }


    /**
     * Проверяем список на валидность
     *
     * @param array
     * @return array
     */
    public function validateList ($list) {

        // преобразуем
        $list = array_map($this->fn, $list);
        $list = array_unique(ArrayHelper::a_filter ($list));

        // если есть массив с данными - проверяем на валидность
        if ($this->range !== null && is_array($this->range) ) {
            foreach ($list as $key => $val) {
                if (!in_array($val, $this->range) ) {
                    unset ($list[$key]);
                }
            }
        }

        // добавляем префикс к элементам
        $this->addItemPrefix ($list);

        return $list;
    }

    /**
     * @inheritdoc
     */
    protected function validateValue($value)
    {
        // тут пока нет никаких проверок
        return null;
    }

    /**
     * @inheritdoc
     */
    public function clientValidateAttribute($object, $attribute, $view)
    {

    }
}
