<?php

/**
 * Replacer for print_r() function.
 *
 * Actually was created due to error
 * that print_r raises in error_handlers
 *
 * @package CORE
 * @subpackage Debug
 */
class PrintVClass
{
    // indenting
    const INDENT = '   ';
    const ARRAY_INDENT = '   ';
    const OBJECT_INDENT = '   ';

    // colors for parts
    const COL_TYPE = '#0725FC';
    const COL_VALUE = '#C03E00';
    const COL_KEY = '#023E8E';
    const COL_MODS = '#8C5C52';
    const COL_LEN = '#FC5C01';
    const COL_CLASS = '#0100B0';
    const COL_PROP = '#023E8E';
    const COL_DYNAMIC_PROP = '#F2617D';

    // name for possibly unique index / property
    const DUMB_NAME = 'dumb_dumb___0123_dumb_index_unique';

    // max deep level for printing
    public static $MAX_LEVEL = 5;

    // storage for recursion check
    public static $rc_refs = array();

    // set max depth level
    public static function setMaxLevel($level)
    {
        self::$MAX_LEVEL = $level;
    }

    // prints any variable
    public static function v_mixed(&$var, $return, $indent = 0)
    {
        switch (gettype($var)):
            // NULL
            case 'NULL':
                return self::v_null($var, $return);
                break;

            // boolean
            case 'boolean':
                return self::v_boolean($var, $return);
                break;

            // integer
            case 'integer':
                return self::v_integer($var, $return);
                break;

            // double
            case 'double':
                return self::v_double($var, $return);
                break;

            // string
            case 'string':
                return self::v_string($var, $return);
                break;

            // array
            case 'array':
                return self::v_array($var, $return, $indent);
                break;

            // object
            case 'object':
                return self::v_object($var, $return, $indent);
                break;

            // resource
            case 'resource':
                return self::v_resource($var, $return);
                break;
        endswitch;
    }

    // NULL
    private static function v_null(&$var, $return)
    {
        if ($return === true)
            return "null [NULL]";
        else
            return "<i style=\"color:" . self::COL_TYPE . "\">null</i> [<b style=\"color:" .
            self::COL_VALUE . "\">NULL</b>]";
    }

    // boolean
    private static function v_boolean(&$var, $return)
    {
        if ($return === true)
            return "boolean [" . ($var === true ? 'true' : 'false') . "]";
        else
            return "<i style=\"color:" . self::COL_TYPE . "\">boolean</i> [<b style=\"color:" .
            self::COL_VALUE . "\">" . ($var === true ? 'true' : 'false') . "</b>]";
    }

    // integer
    private static function v_integer(&$var, $return)
    {
        if ($return === true)
            return "integer [$var]";
        else
            return "<i style=\"color:" . self::COL_TYPE . "\">integer</i> [<b style=\"color:" .
            self::COL_VALUE . "\">$var</b>]";
    }

    // double
    private static function v_double(&$var, $return)
    {
        if ($return === true)
            return "double [$var]";
        else
            return "<i style=\"color:" . self::COL_TYPE . "\">double</i> [<b style=\"color:" .
            self::COL_VALUE . "\">$var</b>]";
    }

    // string
    private static function v_string(&$var, $return)
    {
        if ($return === true)
            return "string(" . strlen($var) . ") [" . htmlspecialchars($var) . "]";
        else
            return "<i style=\"color:" . self::COL_TYPE . "\">string(<b style=\"color:" .
            self::COL_LEN . "\">" . strlen($var) . "</b>)</i> [<b style=\"color:" .
            self::COL_VALUE . "\">" . htmlspecialchars($var) . "</b>]";
    }

    // array
    private static function v_array(&$var, $return, $indent)
    {
        // add to links (for checking recursion)
        self::$rc_refs[] = &$var;

        // indent
        $p = str_repeat(self::INDENT, $indent);

        if ($return === true)
        {
            // full info
            if ($indent < self::$MAX_LEVEL)
            {
                $st = "array\n";
                $st .= $p . "(\n";

                foreach ($var as $key => $val)
                {
                    $st .= $p . self::ARRAY_INDENT . "[" . (is_string($key) ? "'$key'" : $key) . "] => ";
                    if (self::check_recursion($var[$key]))
                        $st .= "** RECURSION **\n";
                    else
                        $st .= self::v_mixed($var[$key], true, $indent + 1) . "\n";
                }

                $st .= $p . ')';
            }
            // only type and size
            else
                $st = "array (" . count($var) . ")";
        }
        else
        {
            // full info
            if ($indent < self::$MAX_LEVEL)
            {
                $st = "<i style=\"color:" . self::COL_TYPE . "\">array</i>\n";
                $st .= $p . "(\n";

                foreach ($var as $key => $val)
                {
                    $st .= $p . self::ARRAY_INDENT . "[<span style=\"color:" . self::COL_KEY .
                        "\">" . (is_string($key) ? "'$key'" : $key) . "</span>] => ";
                    if (self::check_recursion($var[$key]))
                        $st .= "<b><u>** RECURSION **</u></b>\n";
                    else
                        $st .= self::v_mixed($var[$key], false, $indent + 1) . "\n";
                }

                $st .= $p . ')';
            }
            // only type and size
            else
                $st = "<i style=\"color:" . self::COL_TYPE . "\">array (<b style=\"color:" .
                    self::COL_LEN . "\">" . count($var) . "</b>)</i>";
        }

        return $st;
    }

    // object
    private static function v_object(&$var, $return, $indent)
    {
        // add to links (for checking recursion)
        self::$rc_refs[] = &$var;

        // indent
        $p = str_repeat(self::INDENT, $indent);

        if ($return === true)
        {
            // full info
            if ($indent < self::$MAX_LEVEL)
            {
                $st = "object (" . get_class($var) . ")\n";

                // there are special cases
                switch (true):
                    /* DOMDocument */
                    case ($indent == 0) && (get_class($var) == 'DOMDocument'):
                        $var->formatOutput = true;
                        $st .= $var->saveXML();
                        break;

                    /* DOMNodeList */
                    case (get_class($var) == 'DOMNodeList'):
                        $st = "object (" . get_class($var) . ")(" . $var->length . ")\n";
                        $st .= $p . "{\n";

                        $i = 0;
                        foreach ($var as $val)
                        {
                            $st .= $p . self::OBJECT_INDENT . "[" . $i++ . "] => ";
                            $st .= self::v_mixed($val, true, $indent + 1) . "\n";
                        }

                        $st .= $p . '}';
                        break;

                    /* DOMElement */
                    case (get_class($var) == 'DOMElement'):
                        $st = "object (" . get_class($var) . ")<" . $var->tagName . ">";
                        break;

                    /* mysqli_result */
                    case (get_class($var) == 'mysqli_result'):
                        $st = "object (" . get_class($var) . ")[rows: " . $var->num_rows . ", fields: " . $var->field_count . ']';
                        break;

                    /* standard way */
                    default:
                        $st .= $p . "{\n";

                        $info = self::reflect_object($var);
                        foreach ($info as $item)
                        {
                            switch ($item['type']):
                                // const
                                case 'const':
                                    $st .= $p . self::OBJECT_INDENT . "[const : " . $item['name'] . "] => " .
                                        self::v_mixed($item['value'], true, $indent + 1) . "\n";
                                    break;

                                case 'property':
                                    if (!$item['default']) $item['modifiers'] .= ' run-time';
                                    $st .= $p . self::OBJECT_INDENT . "[" . $item['modifiers'] . " : " . $item['name'] . "] => ";
                                    if (array_key_exists('value', $item))
                                    {
                                        if (self::check_recursion($item['value']))
                                            $st .= "** RECURSION **\n";
                                        else
                                            $st .= self::v_mixed($item['value'], true, $indent + 1) . "\n";
                                    }
                                    else
                                        $st .= "** NO ACCESS **\n";
                                    break;
                            endswitch;
                        }

                        $st .= $p . "}";
                        break;
                endswitch;
            }
            // only type
            else
            {
                // there are special cases
                switch (true):
                    /* DBProperty */
                    case (get_class($var) == 'DBProperty'):
                        $st = "object (" . get_class($var) . ")";
                        $st .= " [";

                        $tmp_obj = (array) $var;
                        $st .= $tmp_obj["\0*\0type"] . ", ";
                        $st .= self::v_mixed($tmp_obj["\0*\0value"], true, 0);

                        $st .= ']';
                        break;

                    default:
                        $st = "object (" . get_class($var) . ")";
                endswitch;
            }
        }
        else
        {
            // full info
            if ($indent < self::$MAX_LEVEL)
            {
                $st = "<i style=\"color:" . self::COL_TYPE . "\">object (<span style=\"color:"
                    . self::COL_CLASS . "\">" . get_class($var) . "</span>)</i>\n";

                // there are special cases
                switch (true):
                    /* DOMDocument */
                    case ($indent == 0) && (get_class($var) == 'DOMDocument'):
                        $var->formatOutput = true;
                        $st .= htmlspecialchars($var->saveXML());
                        break;

                    /* DOMNodeList */
                    case (get_class($var) == 'DOMNodeList'):
                        $st = "<i style=\"color:" . self::COL_TYPE . "\">object (<span style=\"color:"
                            . self::COL_CLASS . "\">" . get_class($var) . "</span>)(<span style=\"" . self::COL_LEN
                            . "\">" .  $var->length . "</span>)</i>\n";

                        $st .= $p . "{\n";
                        $i = 0;
                        foreach ($var as $val)
                        {
                            $st .= $p . self::OBJECT_INDENT . "[<span style=\"color:" . self::COL_PROP .
                                "\">" . $i++ . "</span>] => ";
                            $st .= self::v_mixed($val, false, $indent + 1) . "\n";
                        }

                        $st .= $p . '}';
                        break;

                    /* DOMElement */
                    case (get_class($var) == 'DOMElement'):
                        $st = "<i style=\"color:" . self::COL_TYPE . "\">object (<span style=\"color:"
                            . self::COL_CLASS . "\">" . get_class($var) . "</span>)&lt;<span style=\"" . self::COL_LEN
                            . "\">" .  $var->tagName . "</span>&gt;</i>";
                        break;

                    /* mysqli_result */
                    case (get_class($var) == 'mysqli_result'):
                        $st = "<i style=\"color:" . self::COL_TYPE . "\">object (<span style=\"color:"
                            . self::COL_CLASS . "\">" . get_class($var) . "</span>)[<span style=\"" . self::COL_LEN
                            . "\">rows: " .  $var->num_rows . ', fields: ' . $var->field_count . "</span>]</i>";
                        break;

                    /* standard way */
                    default:
                        $st .= $p . "{\n";

                        $info = self::reflect_object($var);
                        foreach ($info as $item)
                        {
                            switch ($item['type']):
                                // const
                                case 'const':
                                    $st .= $p . self::OBJECT_INDENT . "[<span style=\"color:" . self::COL_MODS . "\">const</span> : <span style=\"color:" .
                                        self::COL_PROP . "\">" . $item['name'] . "</span>] => " .
                                        self::v_mixed($item['value'], false, $indent + 1) . "\n";
                                    break;

                                case 'property':
                                    if (!$item['default']) $item['modifiers'] .= ' run-time';
                                    $st .= $p . self::OBJECT_INDENT . "[<span style=\"color:" . self::COL_MODS . "\">" .
                                        $item['modifiers'] . "</span> : <span style=\"color:" .	($item['default'] ? self::COL_PROP : self::COL_DYNAMIC_PROP) . "\">" . $item['name'] . "</span>] => ";
                                    if (array_key_exists('value', $item))
                                    {
                                        if (self::check_recursion($item['value']))
                                            $st .= "<b><u style=\"color:black\">** RECURSION **</u></b>\n";
                                        else
                                            $st .= self::v_mixed($item['value'], false, $indent + 1) . "\n";
                                    }
                                    else
                                        $st .= "<b><u style=\"color:black\">** NO ACCESS **</u></b>\n";
                                    break;
                            endswitch;
                        }

                        $st .= $p . "}";
                        break;
                endswitch;
            }
            // only type
            else
            {
                switch (true):
                    /* DBProperty */
                    case (get_class($var) == 'DBProperty'):
                        $st = "<i style=\"color:" . self::COL_TYPE . "\">object (<span style=\"color:"
                            . self::COL_CLASS . "\">" . get_class($var) . "</span>)</i>";

                        $st .= " [";

                        $tmp_obj = (array) $var;
                        $st .= "<span style=\"color:" . self::COL_VALUE . "\">" . $tmp_obj["\0*\0type"] . "</span>, ";
                        $st .= self::v_mixed($tmp_obj["\0*\0value"], false, 0);

                        $st .= "]";
                        break;

                    default:
                        $st = "<i style=\"color:" . self::COL_TYPE . "\">object (<span style=\"color:"
                            . self::COL_CLASS . "\">" . get_class($var) . "<span>)</i>";
                endswitch;
            }
        }

        return $st;
    }

    // resource
    private static function v_resource(&$var, $return)
    {
        if ($return === true)
            return "resource (" . get_resource_type($var) . ")";
        else
            return "<i style=\"color:" . self::COL_TYPE . "\">resource (<span style=\"color:"
            . self::COL_CLASS . "\">" . get_resource_type($var) . "</span>)</i>";
    }

    // check recursive dependencies in array & objects
    private static function check_recursion(&$var)
    {
        // array case
        if (is_array($var))
        {
            // setting dumb index
            $dindex = self::DUMB_NAME;
            $var[$dindex] = 1;

            $RECURSION = false;

            // checking if any array attribute of self::$rc_refs
            // has such dumb property
            foreach (self::$rc_refs as $key => $val)
            {
                // check only arrays
                if (!is_array(self::$rc_refs[$key])) continue;

                if (isset(self::$rc_refs[$key][$dindex]))
                {
                    $RECURSION = true;
                    break;
                }
            }

            // unsetting index
            unset($var[$dindex]);

            return $RECURSION;
        }
        // object case
        elseif (is_object($var))
        {

            // setting dumb property
            $dprop = self::DUMB_NAME;

            // спец. обработка для установки property (правда теряется класс объекта), установим хотя бы внешний класс объекта
            $class_name = get_class ($var);
            $var = json_encode($var);
            $var = json_decode($var);
            if (!$var) {
                $var = new stdClass;
            }
            $var->__class_name = $class_name;
            $var->$dprop = 1;

            $RECURSION = false;

            // checking if any object attribute of self::$rc_refs
            // has such dumb property
            foreach (self::$rc_refs as $key => $val)
            {
                // check only objects
                if (!is_object(self::$rc_refs[$key])) continue;

                if (isset(self::$rc_refs[$key]->$dprop))
                {
                    $RECURSION = true;
                    break;
                }
            }

            // unsetting property
            unset($var->$dprop);

            return $RECURSION;
        }
        // all else
        else
            return false;
    }

    // function reflects object (uses Reflection extension)
    private static function &reflect_object(&$var)
    {
        $rf = array();

        $rf_mapping = array();

        $robj = new ReflectionObject($var);

        // constants
        $constants = $robj->getConstants();
        foreach ($constants as $key => $val)
        {
            $rf[] = array(
                'type' => 'const',
                'name' => $key,
                'value' => &$constants[$key]
            );
        }

        // properties
        $properties = $robj->getProperties();
        foreach ($properties as $item)
        {
            $prop = array(
                'type' => 'property',
                'name' => $item->getName(),
                'modifiers' => implode(' ', Reflection::getModifierNames($item->getModifiers())),
                'default' => $item->isDefault()
            );

            // accessing values through hacks
            switch (true):
                // static public
                case $item->isStatic() && $item->isPublic():
                    $prop['value'] = $robj->getStaticPropertyValue($prop['name']);
                    break;

                // static protected
                case $item->isStatic() && $item->isProtected():
                    break;

                // static private
                case $item->isStatic() && $item->isPrivate():
                    break;

                // public
                case $item->isPublic():
                    $prop['value'] = &$var->$prop['name'];
                    break;

                // protected
                case $item->isProtected():
                    $tmp_obj = (array) $var;
                    $prop['value'] = &$tmp_obj["\0*\0" . $prop['name']];
                    break;

                // private
                case $item->isPrivate():
                    $tmp_obj = (array) $var;
                    $prop['value'] = &$tmp_obj["\0" . $robj->getName() . "\0" . $prop['name']];
                    break;
            endswitch;

            $rf[] = $prop;
            $rf_mapping[$prop['name']] = count($rf) - 1;
        }

        // as for bug in implementation of ReflectionObject
        // dynamically-set properties fetched manually
        $dout = ReflectionObject::export($var, true);
        if (preg_match("/Dynamic properties \[(?:\d+)\] \{(.+?)\}/is", $dout, $matches))
        {
            $txt_props = explode("\n", $matches[1]);
            foreach ($txt_props as $line)
            {
                if (preg_match("/Property \[ (?:[^\$]*)[\$](.+) \]/i", $line, $matches))
                {
                    $prop_name = $matches[1];
                    if (!array_key_exists($prop_name, $rf_mapping))
                    {
                        $prop = array(
                            'type' => 'property',
                            'name' => $matches[1],
                            'modifiers' => 'public',
                            'default' => false,
                            'value' => &$var->$prop_name
                        );
                        $rf[] = $prop;
                        $rf_mapping[$prop['name']] = count($rf) - 1;
                    }
                    else
                        $rf[$rf_mapping[$prop_name]]['default'] = false;
                }
            }
        }

        return $rf;
    }


    /**
     * Get class Parents
     *
     * @param string $class Class Name
     * @return array
     */
    public static function getClassParents($class)
    {
        $classes = array($class);
        while ($class = get_parent_class($class))
        {
            $classes[] = $class;
        }

        return $classes;
    }


    // simple value output
    public static function simple_val($var)
    {
        switch (gettype($var)):
            // NULL
            case 'NULL':
                return "<span style=\"color:" . self::COL_VALUE . "\">NULL</span>";
                break;

            // boolean
            case 'boolean':
                return "<span style=\"color:" . self::COL_VALUE . "\">" . ($var === true ? 'true' : 'false') . "</span>";
                break;

            // integer
            case 'integer':
            case 'double':
                return "<span style=\"color:" . self::COL_VALUE . "\">$var</span>";
                break;

            // string
            case 'string':
                return "\"<span style=\"color:" . self::COL_VALUE . "\">" .
                htmlspecialchars(str_replace(array("\n", "\r"), array('\n', '\r'), $var)) . "</span>\"";
                break;

            // array
            case 'array':
                // check if array is numeric or hash
                $numeric = array_reduce(array_keys($var), create_function('$r, $v', 'return $r && (gettype($v) == "integer");'), true);
                $vals = array();
                if ($numeric) // numeric array
                {
                    foreach ($var as $value) $vals[] = self::simple_val($value);
                }
                else // hash
                {
                    foreach ($var as $key => $value) $vals[] = self::simple_val($key) . ' =&gt; ' . self::simple_val($value);
                }
                return "<i style=\"color:" . self::COL_TYPE . "\">array</i><i>(</i>" . implode(', ', $vals) . "<i>)</i>";
                break;

            // object
            case 'object':
                return "<i style=\"color:" . self::COL_TYPE . "\">object (<span style=\"color:"
                . self::COL_CLASS . "\">" . get_class($var) . "</span>)</i>";
                break;

            default: return self::v_mixed($var);
        endswitch;
    }
}

/**
 * Wrapper for {@link PrintVClass} - extended {@link print_r()} version
 *
 * Displays types & other useful information
 *
 * @param mixed $var Variable to dump
 * @param bool $return Return result in text version or display in browser
 * @return void|string
 */
function print_v($var, $return = false)
{
    PrintVClass::$rc_refs = array();

    // switching type of element
    if ($return === true)
        return PrintVClass::v_mixed($var, true);
    else
    {
        echo "<pre style=\"background:#FDFFF3; border:1px solid black; padding:5px; margin:5px; text-align:left; color:black; font-family:monospace; font-size:12px;\">\n" .
            PrintVClass::v_mixed($var, false) . "</pre>\n";
    }
}

/**
 * Wrapper for {@link PrintVClass} - extended {@link print_r()} version for multiple arguments
 *
 * Displays types & other useful information
 */
function print_vm()
{
    $args = func_get_args();
    foreach ($args as $arg)
    {
        PrintVClass::$rc_refs = array();

        echo "<pre style=\"background:#FDFFF3; border:1px solid black; padding:5px; margin:5px; text-align:left; color:black; font-size:11px\">\n" .
            PrintVClass::v_mixed($arg, false) . "</pre>\n";
    }
}

/**
 * Set Max Depth Level
 *
 * @param int $level Max Depth
 */
function print_v_depth($level)
{
    PrintVClass::setMaxLevel($level);
}


/**
 * Print Simple Class Inheritance
 *
 * @param string|object $class
 */
function print_vc($class)
{
    $class = is_object($class) ? get_class($class) : $class;
    $classes = PrintVClass::getClassParents($class);

    $line = '';
    foreach ($classes as $idx => $item)
    {
        if ($idx > 0) $line .= ' <span style="font-size:smaller">extends</span> ';
        $color = $idx > 0 ? PrintVClass::COL_CLASS : 'green';
        $line .= '<i style="color:' . $color . '">' . $item . '</i>';
    }
    echo "<pre style=\"background:#FDFFF3; border:1px solid black; padding:5px; margin:5px; text-align:left; color:black; font-family:monospace; font-size:12px;\">\n" .
        $line . "</pre>\n";
}


/**
 * Print Full Class Inheritance
 *
 * @param string|object $class
 */
function print_vcf($class)
{
    $class = is_object($class) ? get_class($class) : $class;
    $classes = PrintVClass::getClassParents($class);

    $deps = array();
    foreach ($classes as $item)
    {
        $dep = array();
        $reflection = new ReflectionClass($item);

        $dep['class'] = $item;
        $dep['final'] = $reflection->isFinal();
        $dep['abstract'] = $reflection->isAbstract();
        $dep['internal'] = $reflection->isInternal();

        $interfaces = $reflection->getInterfaces();
        $dep['interfaces'] = array();
        foreach ($interfaces as $interface)
            $dep['interfaces'][] = $interface->getName();

        $deps[] = $dep;
    }

    $line = '';
    foreach ($deps as $idx => $dep)
    {
        $line .= str_repeat('  ', $idx);
        if ($idx > 0) $line .= '<span style="font-size:smaller">extends</span> ';

        // modifiers
        if ($dep['internal']) $line .= '<i style="color:' . PrintVClass::COL_MODS . '">&lt;internal&gt;</i> ';
        if ($dep['final']) $line .= '<i style="color:' . PrintVClass::COL_MODS . '">final</i> ';
        if ($dep['abstract']) $line .= '<i style="color:' . PrintVClass::COL_MODS . '">abstract</i> ';

        // class name
        $color = $idx > 0 ? PrintVClass::COL_CLASS : 'green';
        $line .= '<i style="font-weight:bold; color:' . $color . '">' . $dep['class'] . '</i>';

        // interfaces
        if (count($dep['interfaces']) > 0)
        {
            $line .= ' <span style="font-size:smaller">implements</span> ';
            $line .= implode(', ', array_map(
                create_function('$v', 'return "<i style=\"color:' . PrintVClass::COL_CLASS . '\">$v</i>";'),
                $dep['interfaces']));
        }

        // line end
        if ($idx < count($deps) - 1) $line .= "\n";
    }

    echo "<pre style=\"background:#FDFFF3; border:1px solid black; padding:5px; margin:5px; text-align:left; color:black; font-family:monospace; font-size:12px;\">\n" .
        $line . "</pre>\n";
}


/**
 * Print Full Class Reflection
 *
 * @param string|object $class
 */
function print_c($class)
{
    $class = is_object($class) ? get_class($class) : $class;

    $ref = new ReflectionClass($class);

    $line = '';

    // modifiers
    if ($ref->isInternal()) $line .= '<i style="color:' . PrintVClass::COL_MODS . '">&lt;internal&gt;</i> ';
    if ($ref->isFinal()) $line .= '<i style="color:' . PrintVClass::COL_MODS . '">final</i> ';
    if ($ref->isAbstract() && !$ref->isInterface()) $line .= '<i style="color:' . PrintVClass::COL_MODS . '">abstract</i> ';

    // type
    $line .= '<i style="color:' . PrintVClass::COL_TYPE . '">' . ($ref->isInterface() ? 'interface' : 'class') . '</i> ';

    // name
    $line .= '<i style="font-weight:bold; color:' . PrintVClass::COL_CLASS . '">' . $class . '</i>';

    // extends
    if ($parent = $ref->getParentClass())
    {
        $line .= ' <span style="font-size:smaller">extends</span> ' .
            '<i style="color:' . PrintVClass::COL_CLASS . '">' . $parent->getName() . '</i>';
    }

    // implements
    $interfaces = $ref->getInterfaces();
    if (count($interfaces) > 0)
    {
        $line .= ' <span style="font-size:smaller">implements</span> ';
        $line .= implode(', ', array_map(
            create_function('$v',
                'return "<i style=\"color:' . PrintVClass::COL_CLASS . '\">" . $v->getName() . "</i>";'),
            $interfaces));
    }

    $indent = PrintVClass::OBJECT_INDENT;
    $line .= "\n{\n";

    // constants
    $constants = $ref->getConstants();
    if (count($constants) > 0)
    {
        $line .= $indent . '<span style="color:lightgray">/* CONSTANTS (' . count($constants) . ') */</span>' . "\n";
        foreach ($constants as $name => $value)
        {
            $line .= $indent . '<span style="color:' . PrintVClass::COL_MODS . '">const</span> ';
            $line .= '<b style="color:' . PrintVClass::COL_PROP . '">' . $name . '</b> = ';
            $line .= PrintVClass::simple_val($value) . ";\n";
        }
    }
    $new_line = count($constants) > 0;

    // properties
    $props = $ref->getProperties();
    if (count($props) > 0)
    {
        // sorting out static & non-static properies
        $static = array();
        $nonstatic = array();
        foreach ($props as $prop)
        {
            $prop->isStatic() ? ($static[] = $prop) : ($nonstatic[] = $prop);
        }

        if (count($static) > 0)
        {
            if ($new_line) $line .= "\n";
            $line .= $indent . '<span style="color:lightgray">/* STATIC PROPERTIES (' . count($static) . ') */</span>' . "\n";

            foreach ($static as $prop)
            {
                $line .= $indent;

                if ($prop->isStatic()) $line .= '<span style="color:' . PrintVClass::COL_MODS . '">static</span> ';

                switch (true):
                    case $prop->isPublic(): $visibility = 'public'; break;
                    case $prop->isProtected(): $visibility = 'protected'; break;
                    case $prop->isPrivate(): $visibility = 'private'; break;
                endswitch;
                $line .= '<span style="color:' . PrintVClass::COL_MODS . '">' . $visibility . '</span> ';

                $line .= '$<b style="color:' . PrintVClass::COL_PROP . '">' . $prop->getName() . '</b>';
                if ($prop->isPublic()) $line .= ' = '  . PrintVClass::simple_val($prop->getValue());
                $line .= ';';

                // declared in
                $declared_in = $prop->getDeclaringClass()->getName();
                if ($ref->getName() !== $declared_in)
                {
                    $line .= '   <div style="display:inline; color:gray">// ';
                    $line .= '<span style="font-size:smaller; color:inherit">declared in</span> ';
                    $line .= '<i style="color:' . PrintVClass::COL_CLASS . '">' . $declared_in . '</i>';
                    $line .= '</div>';
                }

                $line .= "\n";
            }

            $new_line = true;
        }

        // non-static
        if (count($nonstatic) > 0)
        {
            if ($new_line) $line .= "\n";
            $line .= $indent . '<span style="color:lightgray">/* PROPERTIES (' . count($nonstatic) . ') */</span>' . "\n";

            foreach ($nonstatic as $prop)
            {
                $line .= $indent;

                if ($prop->isStatic()) $line .= '<span style="color:' . PrintVClass::COL_MODS . '">static</span> ';

                switch (true):
                    case $prop->isPublic(): $visibility = 'public'; break;
                    case $prop->isProtected(): $visibility = 'protected'; break;
                    case $prop->isPrivate(): $visibility = 'private'; break;
                endswitch;
                $line .= '<span style="color:' . PrintVClass::COL_MODS . '">' . $visibility . '</span> ';

                $line .= '$<b style="color:' . PrintVClass::COL_PROP . '">' . $prop->getName() . '</b>;';

                // declared in
                $declared_in = $prop->getDeclaringClass()->getName();
                if ($ref->getName() !== $declared_in)
                {
                    $line .= '   <div style="display:inline; color:gray">// ';
                    $line .= '<span style="font-size:smaller; color:inherit">declared in</span> ';
                    $line .= '<i style="color:' . PrintVClass::COL_CLASS . '">' . $declared_in . '</i>';
                    $line .= '</div>';
                }

                $line .= "\n";
            }

            $new_line = true;
        }
    }

    // methods
    $methods = $ref->getMethods();
    if (count($methods) > 0)
    {
        // sorting out static & non-static methods
        $static = array();
        $nonstatic = array();
        foreach ($methods as $method)
        {
            $method->isStatic() ? ($static[] = $method) : ($nonstatic[] = $method);
        }

        // static
        if (count($static) > 0)
        {
            if ($new_line) $line .= "\n";
            $line .= $indent . '<span style="color:lightgray">/* STATIC METHODS (' . count($static) . ') */</span>' . "\n";

            foreach ($static as $method)
            {
                // parsing export for additional info
                ob_start();
                $method->export($class, $method->getName());
                $export = ob_get_clean();
                preg_match('/Method \[ <(.+?)> .*? \]/is', $export, $matches);
                $export = $matches[1];

                $export = explode(', ', $export);
                $intr = array_shift($export);
                $internal = $intr != 'user';
                $internal_string = $intr;

                $export = array_filter($export, create_function('$f', 'return !in_array($f, array("ctor", "dtor"));'));
                $export = array_map(create_function('$f', 'return explode(" ", $f);'), $export);

                $line .= $indent;

                if ($internal) $line .= '&lt;<span style="color:' . PrintVClass::COL_MODS . '">' . $internal_string . '</span>&gt; ';
                if ($method->isStatic()) $line .= '<span style="color:' . PrintVClass::COL_MODS . '">static</span> ';
                if ($method->isFinal()) $line .= '<span style="color:' . PrintVClass::COL_MODS . '">final</span> ';
                if (!$ref->isInterface() && $method->isAbstract()) $line .= '<span style="color:' . PrintVClass::COL_MODS . '">abstract</span> ';

                switch (true):
                    case $method->isPublic(): $visibility = 'public'; break;
                    case $method->isProtected(): $visibility = 'protected'; break;
                    case $method->isPrivate(): $visibility = 'private'; break;
                endswitch;
                $line .= '<span style="color:' . PrintVClass::COL_MODS . '">' . $visibility . '</span> ';

                if ($method->returnsReference()) $line .= '&amp;';
                if ($method->isConstructor())
                    $color = 'green';
                elseif ($method->isDestructor())
                    $color = 'red';
                else
                    $color = PrintVClass::COL_PROP;
                $line .= '<b style="color:' . $color . '">' . $method->getName() . '</b>';

                // params
                $params = $method->getParameters();
                $param_lines = array();
                foreach($params as $idx => $param)
                {
                    $param_line = '';

                    // type hint
                    $hint = $param->getClass();
                    if (!is_null($hint))
                        $param_line .= '<i style="color:' . PrintVClass::COL_CLASS . '">' . $hint->getName() . '</i> ';

                    $param_line .= ($param->isPassedByReference() ? '&amp;' : '') . '$';
                    $color = $param->isOptional() ? 'gray' : PrintVClass::COL_KEY;
                    $param_line .= '<i style="color:' . $color . '">' . ($param->getName() ? $param->getName() : "param$idx") . '</i>';
                    if ($param->isOptional() && $param->isDefaultValueAvailable())
                    {
                        $def = @$param->getDefaultValue();
                        $param_line .= ' = ' . PrintVClass::simple_val($def);
                    }
                    $param_lines[] = $param_line;
                }
                $line .= '(' . implode(', ', $param_lines) . ');';

                if (count($export) > 0)
                {
                    $line .= '   <div style="display:inline; color:gray">// ' .
                        implode(', ', array_map(
                            create_function('$v',
                                'return "<span style=\"font-size:smaller\">$v[0]</span> <i style=\"color:' . PrintVClass::COL_CLASS . '\">" . $v[1] . "</i>";'),
                            $export)) . '</div>';
                }

                $line .="\n";
            }

            $new_line = true;
        }

        // non-static
        if (count($nonstatic) > 0)
        {
            if ($new_line) $line .= "\n";
            $line .= $indent . '<span style="color:lightgray">/* METHODS (' . count($nonstatic) . ') */</span>' . "\n";

            foreach ($nonstatic as $method)
            {
                // parsing export for additional info
                ob_start();
                $method->export($class, $method->getName());
                $export = ob_get_clean();
                preg_match('/Method \[ <(.+?)> .*? \]/is', $export, $matches);
                $export = $matches[1];

                $export = explode(', ', $export);
                $intr = array_shift($export);
                $internal = $intr != 'user';
                $internal_string = $intr;

                $export = array_filter($export, create_function('$f', 'return !in_array($f, array("ctor", "dtor"));'));
                $export = array_map(create_function('$f', 'return explode(" ", $f);'), $export);

                $line .= $indent;

                if ($internal) $line .= '&lt;<span style="color:' . PrintVClass::COL_MODS . '">' . $internal_string . '</span>&gt; ';
                if ($method->isStatic()) $line .= '<span style="color:' . PrintVClass::COL_MODS . '">static</span> ';
                if ($method->isFinal()) $line .= '<span style="color:' . PrintVClass::COL_MODS . '">final</span> ';
                if (!$ref->isInterface() && $method->isAbstract()) $line .= '<span style="color:' . PrintVClass::COL_MODS . '">abstract</span> ';

                switch (true):
                    case $method->isPublic(): $visibility = 'public'; break;
                    case $method->isProtected(): $visibility = 'protected'; break;
                    case $method->isPrivate(): $visibility = 'private'; break;
                endswitch;
                $line .= '<span style="color:' . PrintVClass::COL_MODS . '">' . $visibility . '</span> ';

                if ($method->returnsReference()) $line .= '&amp;';
                if ($method->isConstructor())
                    $color = 'green';
                elseif ($method->isDestructor())
                    $color = 'red';
                else
                    $color = PrintVClass::COL_PROP;
                $line .= '<b style="color:' . $color . '">' . $method->getName() . '</b>';

                // params
                $params = $method->getParameters();
                $param_lines = array();
                foreach($params as $idx => $param)
                {
                    $param_line = '';

                    // type hint
                    $hint = $param->getClass();
                    if (!is_null($hint))
                        $param_line .= '<i style="color:' . PrintVClass::COL_CLASS . '">' . $hint->getName() . '</i> ';

                    $param_line .= ($param->isPassedByReference() ? '&amp;' : '') . '$';
                    $color = $param->isOptional() ? 'gray' : PrintVClass::COL_KEY;
                    $param_line .= '<i style="color:' . $color . '">' . ($param->getName() ? $param->getName() : "param$idx") . '</i>';
                    if ($param->isOptional() && $param->isDefaultValueAvailable())
                    {
                        $def = @$param->getDefaultValue();
                        $param_line .= ' = ' . PrintVClass::simple_val($def);
                    }
                    $param_lines[] = $param_line;
                }
                $line .= '(' . implode(', ', $param_lines) . ');';

                if (count($export) > 0)
                {
                    $line .= '   <div style="display:inline; color:gray">// ' .
                        implode(', ', array_map(
                            create_function('$v',
                                'return "<span style=\"font-size:smaller\">$v[0]</span> <i style=\"color:' . PrintVClass::COL_CLASS . '\">" . $v[1] . "</i>";'),
                            $export)) . '</div>';
                }

                $line .="\n";
            }

            $new_line = true;
        }
    }

    $line .= "}";

    echo "<pre style=\"background:#FDFFF3; border:1px solid black; padding:5px; margin:5px; text-align:left; color:black; font-family:monospace; font-size:12px;\">\n" .
        $line . "</pre>\n";
}


/**
 * Print Full Extension Version
 *
 * @param string $ext
 */
function print_ve($ext)
{
    $ref = new ReflectionExtension($ext);

    $line = '';

    // name
    $line .= '<i style="color:' . PrintVClass::COL_TYPE . '">extension</i> ';
    $line .= '<i style="font-weight:bold; color:' . PrintVClass::COL_CLASS . '">' . $ref->getName() . '</i>';

    // version
    $version = $ref->getVersion();
    if ($version == '')
    {
        switch ($ref->getName()):
            case 'libxml': $version = LIBXML_DOTTED_VERSION; break;
            case 'iconv': $version = ICONV_VERSION; break;
            case 'gmp': $version = GMP_VERSION; break;
            case 'gd':
                $info = gd_info();
                $version = $info['GD Version'];
                $supports = array(
                    'FreeType' => $info["FreeType Support"],
                    'T1Lib' => $info["T1Lib Support"],
                    'GIF' => $info["GIF Read Support"] && $info["GIF Create Support"],
                    'JPG' => $info["JPG Support"],
                    'PNG' => $info["PNG Support"],
                    'WBMP' => $info["WBMP Support"],
                    'XBM' => $info["XBM Support"]
                );
                $supports = implode(', ', array_keys(array_filter($supports, create_function('$v', 'return $v;'))));
                $version .= ' [' . $supports . ']';
                break;
            case 'curl':
                $info = curl_version();
                $version = $info['version'] . (isset($info['ssl_version']) ? ', ' . trim($info['ssl_version']) : '');
                break;
            case 'openssl': $version = OPENSSL_VERSION_TEXT; break;
        endswitch;
    }
    if ($version != '')
    {
        $line .= ' <i style="color:' . PrintVClass::COL_TYPE . '">version</i> ';
        $line .= '<i style="font-weight:bold; color:' . PrintVClass::COL_VALUE . '">' . $version . '</i>';
    }

    echo "<pre style=\"background:#FDFFF3; border:1px solid black; padding:5px; margin:5px; text-align:left; color:black; font-family:monospace; font-size:12px;\">\n" .
        $line . "</pre>\n";
}


/**
 * Print Full Extension Reflection
 *
 * @param string $ext
 */
function print_ver($ext)
{
    $ref = new ReflectionExtension($ext);

    $line = '';

    // name
    $line .= '<i style="color:' . PrintVClass::COL_TYPE . '">extension</i> ';
    $line .= '<i style="font-weight:bold; color:' . PrintVClass::COL_CLASS . '">' . $ref->getName() . '</i>';

    // version
    $version = $ref->getVersion();
    if ($version == '')
    {
        switch ($ref->getName()):
            case 'libxml': $version = LIBXML_DOTTED_VERSION; break;
            case 'iconv': $version = ICONV_VERSION; break;
            case 'gmp': $version = GMP_VERSION; break;
            case 'gd':
                $info = gd_info();
                $version = $info['GD Version'];
                $supports = array(
                    'FreeType' => $info["FreeType Support"],
                    'T1Lib' => $info["T1Lib Support"],
                    'GIF' => $info["GIF Read Support"] && $info["GIF Create Support"],
                    'JPG' => $info["JPG Support"],
                    'PNG' => $info["PNG Support"],
                    'WBMP' => $info["WBMP Support"],
                    'XBM' => $info["XBM Support"]
                );
                $supports = implode(', ', array_keys(array_filter($supports, create_function('$v', 'return $v;'))));
                $version .= ' [' . $supports . ']';
                break;
            case 'curl':
                $info = curl_version();
                $version = $info['version'] . (isset($info['ssl_version']) ? ', ' . trim($info['ssl_version']) : '');
                break;
            case 'openssl': $version = OPENSSL_VERSION_TEXT; break;
        endswitch;
    }
    if ($version != '')
    {
        $line .= ' <i style="color:' . PrintVClass::COL_TYPE . '">version</i> ';
        $line .= '<i style="font-weight:bold; color:' . PrintVClass::COL_VALUE . '">' . $version . '</i>';
    }

    $indent = PrintVClass::OBJECT_INDENT;
    $line .= "\n{\n";

    // INI Entries
    $inientries = $ref->getINIEntries();
    if (count($inientries) > 0)
    {
        $line .= $indent . '<span style="color:lightgray">/* INI ENTRIES (' . count($inientries) . ') */</span>' . "\n";
        foreach ($inientries as $name => $value)
        {
            $line .= $indent . '<span style="color:' . PrintVClass::COL_MODS . '">ini_entry</span> ';
            $line .= '<b style="color:' . PrintVClass::COL_PROP . '">' . $name . '</b> = ';
            $line .= PrintVClass::simple_val($value) . ";\n";
        }
    }
    $new_line = count($inientries) > 0;

    // constants
    $constants = $ref->getConstants();
    if (count($constants) > 0)
    {
        if ($new_line) $line .= "\n";
        $line .= $indent . '<span style="color:lightgray">/* CONSTANTS (' . count($constants) . ') */</span>' . "\n";
        foreach ($constants as $name => $value)
        {
            $line .= $indent . '<span style="color:' . PrintVClass::COL_MODS . '">const</span> ';
            $line .= '<b style="color:' . PrintVClass::COL_PROP . '">' . $name . '</b> = ';
            $line .= PrintVClass::simple_val($value) . ";\n";
        }

        $new_line = true;
    }

    // classes
    $classes = $ref->getClassNames();
    if (count($classes) > 0)
    {
        if ($new_line) $line .= "\n";
        $line .= $indent . '<span style="color:lightgray">/* CLASSES (' . count($classes) . ') */</span>' . "\n";
        foreach ($classes as $class)
        {
            $line .= $indent . '<span style="color:' . PrintVClass::COL_MODS . '">class</span> ';
            $line .= '<b style="color:' . PrintVClass::COL_PROP . '">' . $class . "</b>;\n";
        }

        $new_line = true;
    }

    // functions
    $funcs = $ref->getFunctions();
    if (count($funcs) > 0)
    {
        if ($new_line) $line .= "\n";
        $line .= $indent . '<span style="color:lightgray">/* FUNCTIONS (' . count($funcs) . ') */</span>' . "\n";
        foreach ($funcs as $func)
        {
            $disabled = $func->isDisabled();
            $line .= $indent;
            if ($disabled) $line .= '&lt;<span style="color:red">disabled</span>&gt; ';

            $line .= '<span style="color:' . PrintVClass::COL_MODS . '">function</span> ';
            if ($func->returnsReference()) $line .= '&amp;';
            $line .= '<b style="color:' . ($disabled ? 'red' : PrintVClass::COL_PROP) . '">' . $func->getName() . "</b>";

            // params
            $params = $func->getParameters();
            $param_lines = array();
            foreach($params as $idx => $param)
            {
                $param_line = ($param->isPassedByReference() ? '&amp;' : '') . '$';
                $color = $param->isOptional() ? 'gray' : PrintVClass::COL_KEY;
                $param_line .= '<i style="color:' . $color . '">' . ($param->getName() ? $param->getName() : "param$idx") . '</i>';
                if ($param->isOptional() && $param->isDefaultValueAvailable())
                {
                    $def = @$param->getDefaultValue();
                    $param_line .= ' = ' . PrintVClass::simple_val($def);
                }
                $param_lines[] = $param_line;
            }
            $line .= '(' . implode(', ', $param_lines) . ");\n";
        }

        $new_line = true;
    }

    $line .= "}";

    echo "<pre style=\"background:#FDFFF3; border:1px solid black; padding:5px; margin:5px; text-align:left; color:black; font-family:monospace; font-size:12px;\">\n" .
        $line . "</pre>\n";
}
?>
