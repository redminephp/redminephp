<?php

// autoload_files.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

$files = array(
    $vendorDir . '/ezyang/htmlpurifier/library/HTMLPurifier.composer.php',
    $vendorDir . '/swiftmailer/swiftmailer/lib/swift_required.php',
);

// if not productions version - attach print_v lib
if (YII_ENV != "prod") {
	$files[] = $vendorDir . '/printv_lib/print_v.php';
}

return $files;
