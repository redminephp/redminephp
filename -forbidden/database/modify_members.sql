ALTER TABLE `members`
  CHANGE COLUMN `id` `member_id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `members`
  CHANGE COLUMN `user_id` `member_user_id` int(11) NOT NULL DEFAULT '0';

ALTER TABLE `members`
  CHANGE COLUMN `project_id` `member_project_id` int(11) NOT NULL DEFAULT '0';

ALTER TABLE `members`
  CHANGE COLUMN `created_on` `member_created_on` datetime NULL DEFAULT NULL;

ALTER TABLE `members`
  CHANGE COLUMN `mail_notification` `member_mail_notification` tinyint(1) NOT NULL DEFAULT '0';