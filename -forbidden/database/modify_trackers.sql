ALTER TABLE `trackers`
  CHANGE COLUMN `id` `tracker_id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `trackers`
  CHANGE COLUMN `name` `tracker_name` varchar(30) NOT NULL DEFAULT '';

ALTER TABLE `trackers`
  CHANGE COLUMN `is_in_chlog` `tracker_is_in_chlog` tinyint(1) NOT NULL DEFAULT '0';

ALTER TABLE `trackers`
  CHANGE COLUMN `position` `tracker_position` int(11) NULL DEFAULT '1';

ALTER TABLE `trackers`
  CHANGE COLUMN `is_in_roadmap` `tracker_is_in_roadmap` tinyint(1) NOT NULL DEFAULT '1';

ALTER TABLE `trackers`
  CHANGE COLUMN `fields_bits` `tracker_fields_bits` int(11) NULL DEFAULT '0';