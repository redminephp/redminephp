ALTER TABLE `projects_trackers`
  CHANGE COLUMN `project_id` `projects_trackers_project_id` int(11) NOT NULL DEFAULT 0;

ALTER TABLE `projects_trackers`
  CHANGE COLUMN `tracker_id` `projects_trackers_tracker_id` int(11) NOT NULL DEFAULT 0;