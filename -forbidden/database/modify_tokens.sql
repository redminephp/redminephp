ALTER TABLE `tokens`
  CHANGE COLUMN `id` `token_id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `tokens`
  CHANGE COLUMN `user_id` `token_user_id` int(11) NOT NULL DEFAULT '0';

ALTER TABLE `tokens`
  CHANGE COLUMN `action` `token_action` varchar(30) NOT NULL DEFAULT '';

ALTER TABLE `tokens`
  CHANGE COLUMN `value` `token_value` varchar(40) NOT NULL DEFAULT '';

ALTER TABLE `tokens`
  CHANGE COLUMN `created_on` `token_created_on` datetime NOT NULL;