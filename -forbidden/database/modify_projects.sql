ALTER TABLE `projects`
  CHANGE COLUMN `id` `project_id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `projects`
  CHANGE COLUMN `name` `project_name` varchar(255) NOT NULL DEFAULT '';

ALTER TABLE `projects`
  CHANGE COLUMN `description` `project_description` text NULL;

ALTER TABLE `projects`
  CHANGE COLUMN `homepage` `project_homepage` varchar(255) NULL DEFAULT '';

ALTER TABLE `projects`
  CHANGE COLUMN `is_public` `project_is_public` tinyint(1) NOT NULL DEFAULT '1';

ALTER TABLE `projects`
  CHANGE COLUMN `parent_id` `project_parent_id` int(11) NULL DEFAULT NULL;

ALTER TABLE `projects`
  CHANGE COLUMN `created_on` `project_created_on` datetime NULL DEFAULT NULL;

ALTER TABLE `projects`
  CHANGE COLUMN `updated_on` `project_updated_on` datetime NULL DEFAULT NULL;

ALTER TABLE `projects`
  CHANGE COLUMN `identifier` `project_identifier` varchar(255) NULL DEFAULT NULL;

ALTER TABLE `projects`
  CHANGE COLUMN `status` `project_status` int(11) NOT NULL DEFAULT '1';

ALTER TABLE `projects`
  CHANGE COLUMN `lft` `project_lft` int(11) NULL DEFAULT NULL;

ALTER TABLE `projects`
  CHANGE COLUMN `rgt` `project_rgt` int(11) NULL DEFAULT NULL;

ALTER TABLE `projects`
  CHANGE COLUMN `inherit_members` `project_inherit_members` tinyint(1) NOT NULL DEFAULT '0';

ALTER TABLE `projects`
  CHANGE COLUMN `project_parent_id` `project_parent_id` int(11) NULL DEFAULT 0;