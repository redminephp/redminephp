ALTER TABLE `enabled_modules`
  CHANGE COLUMN `id` `enabled_modules_id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `enabled_modules`
  CHANGE COLUMN `project_id` `enabled_modules_project_id` int(11) NULL DEFAULT NULL;

ALTER TABLE `enabled_modules`
  CHANGE COLUMN `name` `enabled_modules_name` varchar(255) NOT NULL DEFAULT '';