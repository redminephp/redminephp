ALTER TABLE `users`
  CHANGE COLUMN `id` `user_id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `users`
  CHANGE COLUMN `login` `user_login` varchar(255) NOT NULL DEFAULT '';

ALTER TABLE `users`
  CHANGE COLUMN `hashed_password` `user_hashed_password` varchar(40) NOT NULL DEFAULT '';

ALTER TABLE `users`
  CHANGE COLUMN `firstname` `user_firstname` varchar(30) NOT NULL DEFAULT '';

ALTER TABLE `users`
  CHANGE COLUMN `lastname` `user_lastname` varchar(255) NOT NULL DEFAULT '';

ALTER TABLE `users`
  CHANGE COLUMN `mail` `user_mail` varchar(60) NOT NULL DEFAULT '';

ALTER TABLE `users`
  CHANGE COLUMN `admin` `user_admin` tinyint(1) NOT NULL DEFAULT '0';

ALTER TABLE `users`
  CHANGE COLUMN `status` `user_status` int(11) NOT NULL DEFAULT '1';

ALTER TABLE `users`
  CHANGE COLUMN `last_login_on` `user_last_login_on` datetime NULL DEFAULT NULL;

ALTER TABLE `users`
  CHANGE COLUMN `language` `user_language` varchar(5) NULL DEFAULT '';

ALTER TABLE `users`
  CHANGE COLUMN `auth_source_id` `user_auth_source_id` int(11) NULL DEFAULT NULL;

ALTER TABLE `users`
  CHANGE COLUMN `created_on` `user_created_on` datetime NULL DEFAULT NULL;

ALTER TABLE `users`
  CHANGE COLUMN `updated_on` `user_updated_on` datetime NULL DEFAULT NULL;

ALTER TABLE `users`
  CHANGE COLUMN `type` `user_type` varchar(255) NULL DEFAULT NULL;

ALTER TABLE `users`
  CHANGE COLUMN `identity_url` `user_identity_url` varchar(255) NULL DEFAULT NULL;

ALTER TABLE `users`
  CHANGE COLUMN `mail_notification` `user_mail_notification` varchar(255) NOT NULL DEFAULT '';

ALTER TABLE `users`
  CHANGE COLUMN `salt` `user_salt` varchar(64) NULL DEFAULT NULL;