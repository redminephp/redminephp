ALTER TABLE `roles`
  CHANGE COLUMN `id` `role_id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `roles`
  CHANGE COLUMN `name` `role_name` varchar(30) NOT NULL DEFAULT '';

ALTER TABLE `roles`
  CHANGE COLUMN `position` `role_position` int(11) NULL DEFAULT '1';

ALTER TABLE `roles`
  CHANGE COLUMN `assignable` `role_assignable` tinyint(1) NULL DEFAULT '1';

ALTER TABLE `roles`
  CHANGE COLUMN `builtin` `role_builtin` int(11) NOT NULL DEFAULT '0';

ALTER TABLE `roles`
  CHANGE COLUMN `permissions` `role_permissions` text NULL;

ALTER TABLE `roles`
  CHANGE COLUMN `issues_visibility` `role_issues_visibility` varchar(30) NOT NULL DEFAULT 'default';