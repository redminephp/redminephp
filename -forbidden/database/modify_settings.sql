ALTER TABLE `settings`
  CHANGE COLUMN `id` `setting_id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `settings`
  CHANGE COLUMN `name` `setting_name` varchar(255) NOT NULL DEFAULT '';

ALTER TABLE `settings`
  CHANGE COLUMN `value` `setting_value` text NULL;

ALTER TABLE `settings`
  CHANGE COLUMN `updated_on` `setting_updated_on` datetime NULL DEFAULT NULL;