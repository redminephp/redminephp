ALTER TABLE `member_roles`
  CHANGE COLUMN `id` `member_role_id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `member_roles`
  CHANGE COLUMN `member_id` `member_role_member_id` int(11) NOT NULL;

ALTER TABLE `member_roles`
  CHANGE COLUMN `role_id` `member_role_role_id` int(11) NOT NULL;

ALTER TABLE `member_roles`
  CHANGE COLUMN `inherited_from` `member_role_inherited_from` int(11) NULL DEFAULT NULL;