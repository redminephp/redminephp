ALTER TABLE `issues`
  CHANGE COLUMN `id` `issue_id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `issues`
  CHANGE COLUMN `tracker_id` `issue_tracker_id` int(11) NOT NULL;

ALTER TABLE `issues`
  CHANGE COLUMN `project_id` `issue_project_id` int(11) NOT NULL;

ALTER TABLE `issues`
  CHANGE COLUMN `subject` `issue_subject` varchar(255) NOT NULL DEFAULT '';

ALTER TABLE `issues`
  CHANGE COLUMN `description` `issue_description` text NULL;

ALTER TABLE `issues`
  CHANGE COLUMN `due_date` `issue_due_date` date NULL DEFAULT NULL;

ALTER TABLE `issues`
  CHANGE COLUMN `category_id` `issue_category_id` int(11) NULL DEFAULT NULL;

ALTER TABLE `issues`
  CHANGE COLUMN `status_id` `issue_status_id` int(11) NOT NULL;

ALTER TABLE `issues`
  CHANGE COLUMN `assigned_to_id` `issue_assigned_to_id` int(11) NULL DEFAULT NULL;

ALTER TABLE `issues`
  CHANGE COLUMN `priority_id` `issue_priority_id` int(11) NOT NULL;

ALTER TABLE `issues`
  CHANGE COLUMN `fixed_version_id` `issue_fixed_version_id` int(11) NULL DEFAULT NULL;

ALTER TABLE `issues`
  CHANGE COLUMN `author_id` `issue_author_id` int(11) NOT NULL;

ALTER TABLE `issues`
  CHANGE COLUMN `lock_version` `issue_lock_version` int(11) NOT NULL DEFAULT '0';

ALTER TABLE `issues`
  CHANGE COLUMN `created_on` `issue_created_on` datetime NULL DEFAULT NULL;

ALTER TABLE `issues`
  CHANGE COLUMN `updated_on` `issue_updated_on` datetime NULL DEFAULT NULL;

ALTER TABLE `issues`
  CHANGE COLUMN `start_date` `issue_start_date` date NULL DEFAULT NULL;

ALTER TABLE `issues`
  CHANGE COLUMN `done_ratio` `issue_done_ratio` int(11) NOT NULL DEFAULT '0';

ALTER TABLE `issues`
  CHANGE COLUMN `estimated_hours` `issue_estimated_hours` float NULL DEFAULT NULL;

ALTER TABLE `issues`
  CHANGE COLUMN `parent_id` `issue_parent_id` int(11) NULL DEFAULT NULL;

ALTER TABLE `issues`
  CHANGE COLUMN `root_id` `issue_root_id` int(11) NULL DEFAULT NULL;

ALTER TABLE `issues`
  CHANGE COLUMN `lft` `issue_lft` int(11) NULL DEFAULT NULL;

ALTER TABLE `issues`
  CHANGE COLUMN `rgt` `issue_rgt` int(11) NULL DEFAULT NULL;

ALTER TABLE `issues`
  CHANGE COLUMN `is_private` `issue_is_private` tinyint(1) NOT NULL DEFAULT '0';

ALTER TABLE `issues`
  CHANGE COLUMN `deliverable_id` `issue_deliverable_id` int(11) NULL DEFAULT NULL;

ALTER TABLE `issues`
  CHANGE COLUMN `closed_on` `issue_closed_on` datetime NULL DEFAULT NULL;