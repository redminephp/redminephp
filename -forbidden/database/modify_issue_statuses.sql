ALTER TABLE `issue_statuses`
  CHANGE COLUMN `id` `issue_status_id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `issue_statuses`
  CHANGE COLUMN `name` `issue_status_name` varchar(30) NOT NULL DEFAULT '';

ALTER TABLE `issue_statuses`
  CHANGE COLUMN `is_closed` `issue_status_is_closed` tinyint(1) NOT NULL DEFAULT '0';

ALTER TABLE `issue_statuses`
  CHANGE COLUMN `is_default` `issue_status_is_default` tinyint(1) NOT NULL DEFAULT '0';

ALTER TABLE `issue_statuses`
  CHANGE COLUMN `position` `issue_status_position` int(11) NULL DEFAULT '1';

ALTER TABLE `issue_statuses`
  CHANGE COLUMN `default_done_ratio` `issue_status_default_done_ratio` int(11) NULL DEFAULT NULL;