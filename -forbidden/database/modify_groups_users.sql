ALTER TABLE `groups_users`
  ADD INDEX (`group_id`);

ALTER TABLE `groups_users`
  ADD INDEX (`user_id`);

ALTER TABLE `groups_users`
  CHANGE COLUMN `group_id` `groups_users_group_id` int(11) NOT NULL;

ALTER TABLE `groups_users`
  CHANGE COLUMN `user_id` `groups_users_user_id` int(11) NOT NULL;